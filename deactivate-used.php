<?php
/**
 * check if running
 */
exec('ps aux | grep -v grep | grep -v '.getmypid().' | grep -v "/bin/sh -c" | grep '.basename(__FILE__), $check);
if(count($check)>0) {
    die("\nAlready running: {$check[0]}\n\n");
}

require_once('../shared-resources/_configs/configs.inc');
require_once('../shared-resources/lurdlogger.php');

LL::log(LL::blue, "\n" . date("Y-m-d H:i:s"));

// deact one-off clearance boards
//$query = "set @uids = null; update products set active = 'n' WHERE mto = 0 and stock = 0 and active = 'y' AND ( SELECT @uids := CONCAT_WS(',', id, @uids) ) limit 100; SELECT @uids;";
$prods = $master->rawQuery("select id, count(s.slot_id) as slots from products p inner join products_slots s on s.product_id=p.id WHERE mto = 0 and active = 'y' group by p.id having slots = 0");
$count = count($prods);
if($count > 0) {
    $pids = join(',', array_column($prods, 'id'));
    LL::log(LL::white, "deactivate: $pids");
    $result = $master->rawQuery("update products set active='n' where id in ($pids) limit $count");
    print_r($result);
} else echo ".";

// remove clearance flag for out of stock MTO boards
//$query = "set @uids = null; update products set clearance = 0 WHERE mto = 1 and stock = 0 and clearance = 1 AND ( SELECT @uids := CONCAT_WS(',', id, @uids) ) limit 100; SELECT @uids;";
$prods = $master->rawQuery("select id, count(s.slot_id) as slots from products p inner join products_slots s on s.product_id=p.id WHERE mto = 1 and clearance = 1 group by p.id having slots = 0");
$count = count($prods);
if($count > 0) {
    $pids = join(',', array_column($prods, 'id'));
    LL::log(LL::white, "deactivate: $pids");
    $result = $master->rawQuery("update products set clearance=0 where id in ($pids) limit $count");
    print_r($result);
} else echo ".";

/**
 * remove 0 quantity slots
 */
$master->where('quantity', 0);
$slots = $master->get('products_slots');
if(!empty($slots)) {
    LL::log(LL::cyan, 'slots to delete: '.json_encode($slots));
    $master->where('quantity', 0);
    if ($master->delete('products_slots', count($slots)))
        LL::log(LL::cyan, 'slots deleted');
} else echo ".";