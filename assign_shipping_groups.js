require('colors').enabled = true
const args = require('yargs').argv
const shippingClass = require('./jsClasses/shipping.js')
const dbClass = require('./jsClasses/db.js')
const config = require('./jsConfig.prod.json')

console.log(`${Date()}`.blue.underline)

let db = new dbClass(config)
let query = `
    select 
        i.item_id, 
        i.order_id, 
        p.product_type
    from ordered_items i 
        inner join orders o on o.order_id = i.order_id
        inner join products p on p.id = i.product_id 
    where 
        (i.group_id <= 0 or i.group_id is null)
        and o.status in (2,7);`
db.executeSql(query)
    .catch(err => {
        return 'sql error: '.red + err + '\n'
    })
    .then(async results => {
        let orders = {}
        for (let item of results) {
            if(!orders[item.order_id])
                orders[item.order_id] = []
            orders[item.order_id].push(item)
        }

        let mutationItems = []
        for (let orderId in orders) {
            let order = orders[orderId]
            let shipping = new shippingClass()
            for (let item of order) {
                let gid = shipping.assignGroupByProductType(item.product_type);
                mutationItems.push({
                    id: item.item_id,
                    order: item.order_id,
                    group_id: gid
                })
            }
        }

        if (mutationItems.length === 0) {
            return 'No items to group\n'
        }

        let result = ''
        let variables = { items: mutationItems }
        await db.executeGql(`${__dirname}/gql/order_items_group_id.graphql`, variables)
            .catch(err => {
                result = 'gql error: '.red + err
            })
            .then(async results => {
                let groups = {}
                for (let item of results.order_items) {
                    if(!groups[item.order.id])
                        groups[item.order.id] = []
                    if(!groups[item.order.id][item.group_id])
                        groups[item.order.id][item.group_id] = []
                    groups[item.order.id][item.group_id].push(item)
                }

                let report = ''
                for (let orderId in groups) {
                    //let groupId = groups[orderId][0].group_id; console.log('gid', groupId)
                    report += '\n' + `ORDER ${orderId}`.black.bgGreen + '\n'
                    for(let groupId in groups[orderId]) {
                        let string = ''
                        let longest = groups[orderId][groupId].reduce((a, b) => a.name.length > b.name.length ? a : b).name.length
                        string += `  - `.yellow + `GROUP ${groupId}`.bold + ` ${''.padStart(longest - 6, '-')}\n`.yellow
                        string += groups[orderId][groupId].map(e => {
                            //let color = e.group_id % 2 === 0 ? 'gray' : 'white'
                            return `  | `.yellow + `${e.name}`.white + `${''.padStart(longest - e.name.length, ' ')} |`.yellow
                        }).join('\n')
                        string += `\n  --------${''.padStart(longest - 4, '-')}`.yellow

                        report += `${string}\n`
                    }
                }

                result = report
            })

        return result
    })
    .then(async message => {
        console.log(message)

         /** reset stuff for testing?
        if(args.testing)
            await db.executeSql('update ordered_items set group_id = 0 where order_id in (917239, 917230);') */

        process.exit()
    })