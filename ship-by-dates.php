<?php

/**
 * check if running
 */
exec('ps aux | grep -v grep | grep -v '.getmypid().' | grep -v "/bin/sh -c" | grep '.basename(__FILE__), $check);
if(count($check)>0) {
    die("\n\nAlready running: {$check[0]}\n\n");
}

require_once('../shared-resources/_configs/configs.inc');
require_once('../shared-resources/lurdlogger.php');

$holidays = array('2017-12-24', '2017-12-25', '2017-12-26', '2018-01-01');
$skipdays = array("Saturday", "Sunday");

function addDays($timestamp, $days, $skipdates = array(), $skipdays = array("Saturday", "Sunday")) {
        $i = 1;
    if((in_array(date("l", $timestamp), $skipdays) || in_array(date("Y-m-d", $timestamp), $skipdates))) {
        if($days==0) {
            echo 'reset days to 1...';
            $days = 1;
        }
    }

    while ($days >= $i) {
        $timestamp = strtotime("+1 day", $timestamp);
        if ( (in_array(date("l", $timestamp), $skipdays)) || (in_array(date("Y-m-d", $timestamp), $skipdates)) )
        {
            $days++;
        }
        $i++;
    }

    return date("Y-m-d 00:00:00",$timestamp);
}

function subDays($timestamp, $days, $skipdates = array(), $skipdays = array("Saturday", "Sunday")) {
    $i = 1;
    if($days==0 && (in_array(date("l", $timestamp), $skipdays) || in_array(date("Y-m-d", $timestamp), $skipdates))) {
        echo 'reset days to 1...';
        $days = 1;
    }

    while ($days >= $i) {
        $timestamp = strtotime("-1 day", $timestamp);
        if ( (in_array(date("l", $timestamp), $skipdays)) || (in_array(date("Y-m-d", $timestamp), $skipdates)) )
        {
            $days++;
        }
        $i++;
    }

    return date("Y-m-d 00:00:00",$timestamp);
}

// fathers day (jun 18th) sbd/cutoff mto res (comm should be -1 weekday)
// state => [ sbd, cutoff ]
/*$holiday_sbd_fd = array(
    1 => array('06/16','06/09'), // FL
    2 => array('06/15','06/08'), // AL
    3 => array('06/14','06/07'), // TX
    4 => array('06/13','06/06'), // NY
    5 => array('06/12','06/05') // CA
);*/

// xmas day sbd/cutoff mto res (comm should be -1 weekday)
// state => [ sbd, cutoff ]
$holiday_sbd_xm = array(
    1 => array('12/22', '12/21'), // FL
    2 => array('12/21', '12/20'), // AL
    3 => array('12/20', '12/19'), // TX
    4 => array('12/19', '12/16'), // NY
    5 => array('12/18', '12/15') // CA
);

//$same_day_boards = array(9502488, 9502494, 34328);
$same_day_boards = array(25440);
//$next_day_boards = array(25440);

// all board types
$slave->where('title like "%Boards: %"');
$board_types_array = $slave->get('product_types', null, 'product_type_id');
$board_types = array_merge(array(28, 29), array_column($board_types_array, 'product_type_id'));

$slave->where('title like "%Bags: %"');
$bag_types_array = $slave->get('product_types', null, 'product_type_id');
$bag_types = array_column($bag_types_array, 'product_type_id');

/*$slave->where('title like "%ACC: %"');
$slave->where('product_type_id', array(2), 'NOT IN');
$accs_array = $slave->get('product_types', null, 'product_type_id');
$acc_types = array_column($accs_array, 'product_type_id');*/
$acc_types = array(75,157,88,133,167,165,166,158,163,164,10047,86,272,16,10056,9);

$solid_bag_types = array(3, 18, 74, 78, 79, 82, 10093, 139, 154);
$high_demand_types = array(10043, 10046, 10014, 10055, 73, 10020, 10048, 266, 269); // gd tower, checker, dt board, tt, baggo

$orders = $slave->rawQuery("
    SELECT o.*, m.title, group_concat(i.item_status) as 'item_statuses' FROM orders o
      LEFT JOIN marketplaces m on m.site_id=o.site
      INNER JOIN ordered_items i on i.order_id=o.order_id
    WHERE o.status IN (2,7)
    AND (
        o.ship_by = '0000-00-00 00:00:00'
        OR  o.ship_by IS NULL
        OR  o.ship_by = ''
    )
    AND (o.updated_date > DATE_SUB(CURDATE(), INTERVAL 5 DAY) OR o.date_added > DATE_SUB(CURDATE(), INTERVAL 14 DAY))
    GROUP BY o.order_id;
");
$i=0;

if(count($orders)==0)
    die();

$processed = array();
$errors = array();
$email = false;

LL::log(LL::blue, "\n".date("Y-m-d H:i:s")." ==========================");
LL::log(LL::yellow, count($orders)." to check");

foreach($orders as $order) {

    /*if(!array_intersect(array(0, 13), explode(',', $order['item_statuses']))) {
        LL::log(LL::light_gray, "\n{$order['order_id']} has no outstanding items");
        continue;
    } else {
        echo "\n";
    }*/

    $order_date = $order['date_added'];
    $upd_date = $order['updated_date'];
    // if order entered more than 3 days ago, use updated date in calculations instead (it was probably paid for recently)
    //$eff_date = date('Ymd', strtotime($order_date)) < date('Ymd', strtotime('-5 day')) ? $upd_date : $order_date;
    //LL::log(LL::white, "ed: $eff_date");
    LL::log(LL::white, "\n{$order['order_id']} ({$order['title']}) added {$order['date_added']} updated {$order['updated_date']}");

    $today = date('Y-m-d 00:00:00');
    $day = date('D', strtotime($order_date));

    // if weekend order, move effective order date to next biz
    if(in_array($day, array('Sat', 'Sun')))
        $order_date = addDays(strtotime($order_date), 1, $holidays);

    LL::log(LL::white, "effective order date: $order_date");
    $hour = date('H', strtotime($order_date));

    $state = $order['shipping_state'];
    $shipping_type = $order['shipping_type'];

    // get all items in the order
    $slave->join('products p', 'p.id=i.product_id', 'INNER');
    $slave->where("i.order_id", $order['order_id']);
    $items = $slave->get("ordered_items i", null, 'i.product_type, i.name, p.stock, p.id, i.slot_nums, p.clearance');

    $rush = false;
    $holiday_delivery = false;
    $instock = null;
    $clearance = null;
    $same_day_boards_only = null;
    $next_day_boards_only = null;
    foreach($items as $k => $v) {
        // check stock
        if($instock !== false && $v['slot_nums'] != '')
            $instock = true;
        else $instock = false;

        // clearance
        if($clearance !== false && $v['clearance'] == 1)
            $clearance = true;
        else $clearance = false;

        // check for rush
        if($v['id']==1029203)
            $rush = 24;
        if($v['id']==8343)
            $rush = 48;

        // check for guaranteed holiday delivery
        if($v['id']==2788620)
            $holiday_delivery = true;

        // fix GH product type
        if($v['product_type'] == 268 && stripos($v['name'], 'guy harvey')!==false) {
            // guy harvey wall decal, correct the product type
            $items[$k]['product_type'] = 254;
            $email = true;
        }

        // change board type to instock if necessary
        if($v['product_type'] != 29 && in_array($v['product_type'], $board_types) && $v['slot_nums'] != '')
            $items[$k]['product_type'] = 28;

        // check for same day boards only
        if(in_array($v['product_type'], $board_types)) {
            $same_day_boards_only = (in_array($v['id'], $same_day_boards) && $same_day_boards_only !== false) ? true : false;
        }
    }

    $count = count($items);
    if($rush) {
        $master->rawQuery("update ordered_items set rush_production = 1 where order_id = {$order['order_id']} limit $count");
    }

    // all product types in the order
    $product_types = array_column($items, "product_type"); //print_r($product_types);
    $product_ids = array_column($items, 'id'); //print_r($product_ids);
    $product_names = array_column($items, 'name');

    $details = '';
    //LL::log(LL::white, "state: ".(empty($state) ? '??' : $state).", types: ".implode(',', $product_types));
    LL::log(LL::white, "types: ".implode(',', $product_types));

    $bags_only = count(array_diff($product_types, $solid_bag_types)) > 0 ? false : true;
    if($bags_only) LL::log(LL::yellow, 'bags only');
    $high_demand_only = count(array_diff($product_types, $high_demand_types)) > 0 ? false : true;
    $has_boards = count(array_intersect($product_types, $board_types)) > 0 ? true : false;

    // if the non-board product types in the order are only board addons, it's a pure board order,
    // so we can determine if it's a same-day type board order with no extra stuff in it

    // stuff that can go with a board and shouldn't push the sbd out (accessories, bags, rush line item, etc)
    $board_addon_types = array_merge($acc_types, $bag_types, array(80));
    $non_board_types = array_diff($product_types, $board_types);
    //print_r($non_board_types); print_r($board_addon_types); echo count(array_diff($non_board_types, $board_addon_types));

    $pure_board_order = $has_boards && count(array_diff($non_board_types, $board_addon_types)) == 0 ? true : false;
    //if($pure_board_order) LL::log(LL::yellow, 'pure board order');

    // ship zones
    $ship_zone_days = false;
    if($state) {
        $slave->where("FIND_IN_SET('$state', states) > 0");
        $ship_zone_days = $slave->getOne('ship_zones', null, array('ship_zone_id', 'ship_days'));
    }
    $custom = false;

    // get highest production time based on product type
    $shipping_info = null;

    if($product_types) {
        $slave->where("product_type_id", $product_types, "IN");
        $slave->where("shipping_offset IS NOT NULL");
        $shipping_info = $slave->getOne("product_types", "MAX(shipping_offset) as shipping_offset");
        echo json_encode($shipping_info)."\n";

        /**
         * override for items in type ACC:Addons which are custom
         * it was discovered that this product type is a mixture of custom and non-custom

        $custom_types = array(25,137,138,139,140,141,146,147,85,100,182);
        $custom_addons = array(4684,8478,8479,10315,10316,10317,10318,13694,42180,103098,103099,103100,103101);
        if(in_array('custom', $product_names) || array_intersect($product_types, $custom_types) || array_intersect($product_ids, $custom_addons)) {
            $custom = true;
            LL::log(LL::yellow, 'has custom/stained');
            if($shipping_info['shipping_offset']<5)
                $shipping_info['shipping_offset']=5;
        }*/
    }

    $same_day_board_order = false;
    if($pure_board_order && $same_day_boards_only) {
        LL::log(LL::cyan, 'Promo board');
        $same_day_board_order = true;
        $shipping_info['shipping_offset'] = 1;
    }

    if($instock) {
        LL::log(LL::cyan, 'All in-stock');
        $shipping_info['shipping_offset'] = 0;
    } elseif($rush) {
        echo chr(27)."[1;33mrush: $rush".chr(27)."[0m\n";
        if($shipping_info['shipping_offset'] > 1)
            $shipping_info['shipping_offset'] = $rush==48 ? 2 : 1;
        /*if($hour>=14)
            $shipping_info['shipping_offset']++;*/
    } elseif($clearance) {
        LL::log(LL::cyan, 'Clearance');
        $shipping_info['shipping_offset'] = 1;
    }

    // default to 3 if no data
    if(is_null($shipping_info['shipping_offset']))
        $shipping_info['shipping_offset'] = 3;

    if($order['title']===0 || $order['title']==='0')
        $order['title'] = 'VT';

    $ship_by = false;
    $xmas_sbd = false;
    $offset_sbd = false;

    // calc SBD based on production time
    if($shipping_info) {
        $offset = $shipping_info['shipping_offset'];
        LL::log(LL::white, "hour: $hour");
        if($bags_only && $hour >= 15) { $offset++; LL::log(LL::yellow, 'bags 3pm cutoff'); } // bags cutoff 3pm
        elseif($same_day_board_order && $offset == 0 && $hour >= 12) { $offset++; LL::log(LL::yellow, 'promo board 12pm cutoff'); } // same-day only before noon
        elseif($hour >= 14) { $offset++; LL::log(LL::yellow, 'standard 2pm cutoff'); } // everything else cutoff 2pm

        LL::log(LL::white, "calc'd offset: $offset from $order_date");
        $offset_sbd = addDays(strtotime($order_date), $offset, $holidays);

        // stupid rush thingy
        /*if($rush) {
            LL::log(LL::yellow, "sub 2 days for rush");
            $offset_sbd = subDays(strtotime($offset_sbd), 2, $holidays);
        }*/
    }

    /**
     * holiday override
     */
    $use_holiday_sbd = false;
    /*if(!$bags_only && $ship_zone_days && $holiday_sbd_xm) {
        $this_holiday_sbd = $holiday_sbd_xm[$ship_zone_days['ship_zone_id']]; // 0 => sbd, 1 => cutoff
        $cutoff = date('Y-m-d 17:00:00', strtotime($this_holiday_sbd[1]));
        if(date('Y-m-d H:i:s') < $cutoff) {
            $holiday_ts = strtotime($this_holiday_sbd[0]);
            $use_holiday_sbd = $shipping_type==90 ?
                date('Y-m-d 00:00:00', $holiday_ts) :  // residential
                date('Y-m-d 00:00:00', strtotime('-1 weekday', $holiday_ts));   // commercial
            LL::log(LL::green, "holiday sbd ($shipping_type): $use_holiday_sbd");
        } else {
            LL::log(LL::yellow, 'after cutoff');
            $use_holiday_sbd = $offset_sbd < '2017-12-28 00:00:00' ? '2017-12-28 00:00:00' : false;
        }
    }*/

    $ship_by = $use_holiday_sbd ? $use_holiday_sbd : $offset_sbd;

    /**
     * non-guaranteed holiday board override

    if($has_boards && !$holiday_delivery && $ship_by < '2017-12-28 00:00:00') {
        LL::log(LL::purple, 'non-guaranteed holiday boards');
        $ship_by = '2017-12-28 00:00:00';
    }*/

    /**
     * pick up to 21st override

    if($order['shipping_method']=='Pick Up' && $ship_by < '2017-12-21 00:00:00') {
        LL::log(LL::purple, 'pickup override');
        $ship_by = '2017-12-21 00:00:00';
    }*/

    /**
     * high demand item override

    if($high_demand_only && $ship_by < '2017-12-28 00:00:00') {
        LL::log(LL::yellow, 'contains high demand items only');
        $ship_by = '2017-12-28 00:00:00';
    }*/

    /*if($ship_by < date('Y-m-d 00:00:00')) {
        LL::log(LL::red, 'past sbd? '.$ship_by);
        $ship_by = false;
    }*/ // prevent past SBD

    if($ship_by) {
        $data = array("ship_by" => $ship_by, 'updated_by' => 546);
        $master->where("order_id", $order['order_id']);
        if($master->update("orders", $data, 1)) {
            echo " -- set to ".chr(27)."[1;33m$ship_by".chr(27)."[0m";
            /*$order['ship_by'] = $ship_by;
            $order['ship_days'] = $ship_zone_days['ship_days'];
            $order['shipping_offset'] = $shipping_info['shipping_offset'];
            $order['all_types'] = $product_types;
            $processed[] = $order;*/
            $i++;
        } else {
            //$errors[] = $order['order_id'].' not updated: '.mysql_error();
            LL::log(LL::red, 'error: '.$master->getLastError());
        }
        //LL::log(LL::light_blue, 'test -- set to '.$ship_by);
    } else {
        echo " -- do nothing";
    }

    echo "\n";
}

