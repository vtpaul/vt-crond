<?php

/**
 * check if running
 */
exec('ps aux | grep -v grep | grep -v '.getmypid().' | grep -v "/bin/sh -c" | grep '.basename(__FILE__), $check);
if(count($check)>0) {
    mail('paul@victorytailgate', 'Ebay script running long', $check[0]);
    die("\n\nAlready running: {$check[0]}\n\n");
}

require_once('../shared-resources/_configs/configs.inc');
require_once('../shared-resources/lurdlogger.php');
require_once('../shared-resources/_models/vt-classes/Product.php');
require_once('../shared-resources/_models/ebay-api/get-common/keys.php');
require_once('../shared-resources/_models/ebay-api/get-common/eBaySession.php');

$debug = in_array('debug', $argv);
if($debug)
	LL::log(LL::purple, "\nDEBUG MODE, NO INSERTS");


$days_off = array('2017-12-25', '2017-12-26', '2018-01-01');

//SiteID must also be set in the Request's XML
//SiteID = 0  (US) - UK = 3, Canada = 2, Australia = 15, ....
//SiteID Indicates the eBay site to associate the call with
$siteID = 0;
//the call being made:
$verb = 'GetOrders';

//Time with respect to GMT
// once a day, look back over a day to cover outages
$seconds = in_array('big', $argv)!==false || (date('Hi')>'0657' && date('Hi')<'0703') ? 90000 : 2400;

$CreateTimeFrom = gmdate("Y-m-d\TH:i:s", time() - $seconds);
$CreateTimeTo = gmdate("Y-m-d\TH:i:s", time() - 300);

/*if(isset($argv[2], $argv[3])) {
    $CreateTimeFrom = gmdate("Y-m-d\TH:i:s", strtotime($argv[2]));
    $CreateTimeTo = gmdate("Y-m-d\TH:i:s", strtotime($argv[3]));
}*/

///Build the request Xml string
$requestXmlBody  = '<?xml version="1.0" encoding="utf-8" ?>';
$requestXmlBody .= '<GetOrdersRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
$requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
$requestXmlBody .= "<ModTimeFrom>$CreateTimeFrom</ModTimeFrom><ModTimeTo>$CreateTimeTo</ModTimeTo>";
$requestXmlBody .= '<OrderRole>Seller</OrderRole>';
$requestXmlBody .= '<OrderStatus>Completed</OrderStatus>';
$requestXmlBody .= "<RequesterCredentials><eBayAuthToken>$userToken</eBayAuthToken></RequesterCredentials>";
$requestXmlBody .= '</GetOrdersRequest>';

//Create a new eBay session with all details pulled in from included keys.php
$session = new eBaySession($userToken, $devID, $appID, $certID, $serverUrl, $compatabilityLevel, $siteID, $verb);

//send the request and get response
$responseXml = $session->sendHttpRequest($requestXmlBody);
if (stristr($responseXml, 'HTTP 404') || $responseXml == '')
    die("\n**Error sending request**\n");

//Xml string is parsed and creates a DOM Document object
$responseDoc = new DomDocument();
$responseDoc->loadXML($responseXml);

//get any error nodes
$errors = $responseDoc->getElementsByTagName('Errors');
$response = simplexml_import_dom($responseDoc);
$entries = $response->PaginationResult->TotalNumberOfEntries;

//if there are error nodes
if ($errors->length > 0) {
    LL::log(LL::red, 'eBay returned the following error(s):');
    //display each error
    //Get error code, ShortMesaage and LongMessage
    $code = $errors->item(0)->getElementsByTagName('ErrorCode');
    $shortMsg = $errors->item(0)->getElementsByTagName('ShortMessage');
    $longMsg = $errors->item(0)->getElementsByTagName('LongMessage');
    
    //Display code and shortmessage
    LL::log(LL::red, $code->item(0)->nodeValue . ' : ' . $shortMsg->item(0)->nodeValue);
    
    //if there is a long message (ie ErrorLevel=1), display it
    if (count($longMsg) > 0)
        LL::log(LL::red, $longMsg->item(0)->nodeValue);

    // opsgenie
    $body = '{
        "message": "eBay Order Import error",
        "description": "API Error: '.$shortMsg->item(0)->nodeValue. ' / '.$longMsg->item(0)->nodeValue.'",
        "teams": [{"name": "Web"}],
        "tags": ["eBay", "Crond"],
        "priority": "P3"
    }';
    sendAlert($body);
} else { //If there are no errors, continue
    if ($entries == 0) {
		LL::logn(LL::white, '.');
	} else {

        LL::log(LL::light_blue, "\n".date("Y-m-d H:i:s"));
        LL::log(LL::blue, $CreateTimeFrom.' - '.$CreateTimeTo);
		LL::log(LL::yellow, "$entries to check...");
		
		// we will send an email for orders with boards/towers in them
		$master->where('title like "Boards: %"');
		$types_array = $master->get('product_types', null, 'product_type_id');
		$board_types = array_column($types_array, 'product_type_id');
		$alert_orders = array();
		$alert_comments = array();

		$orders = $response->OrderArray->Order;
		if ($orders != null) {
			
			foreach ($orders as $order) {

                $prod_errors = array();

                /*if(@$argv[1]=='list') {
                    print_r($response);
                    echo "\n\n";
                    continue;
                }*/

                $payment_type = 0;
                $payment_method = $order->CheckoutStatus->PaymentMethod;
                if($payment_method=='PayPal')
                    $payment_type = 2;
                elseif(stripos($payment_method, 'credit')!==false)
                    $payment_type = 1;

                if($order->PaidTime=='') {
                    LL::log(LL::white, "record ".$order->ShippingDetails->SellingManagerSalesRecordNumber." pay status: " . $order->CheckoutStatus->eBayPaymentStatus);
                    continue;
                }
				
				// check if order exists:
				$site_order_num = (int)$order->ShippingDetails->SellingManagerSalesRecordNumber;
				$master->where('site', 6);
				$master->where('site_order_num', $site_order_num);
				$order_result = $master->getOne('orders', array('order_id', 'ship_by'));
				if(!$debug && !empty($order_result)) {
					LL::log(LL::light_gray, 'Order exists: '.$order_result['order_id'].' / '.$site_order_num.' / c: '.$order->CreatedTime.' / sbd: '.$order_result['ship_by']);
					continue;
				}

                $alert = false;

				$orderStatus = $order->OrderStatus;

				//if the order is completed, print details
				if ($orderStatus) {
					
					// get the amount paid
					$AmountPaid = $order->AmountPaid;
					$AmountPaidAttr = $AmountPaid->attributes();

					// get the sales tax, if any 
					$SalesTaxAmount = $order->ShippingDetails->SalesTax->SalesTaxAmount;
					$SalesTaxAmountAttr = $SalesTaxAmount->attributes();

					// get the shipping service selected by the buyer
					$ShippingServiceSelected = $order->ShippingServiceSelected;
					if($ShippingServiceSelected) {
						$ShippingCostAttr = $ShippingServiceSelected->ShippingServiceCost->attributes();
					} else {
                        ob_start();
                        var_dump($order);
                        $dump = ob_get_clean();
                        mail('paul@victorytailgate', 'Ebay no shipping service', $dump);
                    }
				   
					// get the buyer's shipping address 
					$shippingAddress = $order->ShippingAddress;					
					$name_parts = explode(' ', $shippingAddress->Name);
					$shipping_method = stripos($ShippingServiceSelected->ShippingService, 'standard')!==0 ? 'Standard Shipping' : $ShippingServiceSelected->ShippingService;
                    $comments = (string)$order->BuyerCheckoutMessage;
                    $tid = (string)$order->ExternalTransaction->ExternalTransactionID;
					
					$order_data = array(
						'name' => '', // will get from first transaction
						'email' => '', // will get from first transaction
						'phone' => "$shippingAddress->Phone",
						'subtotal' => "$order->Subtotal",
						'shipping' => 0, // will select earliest from items
						'tax' => "$SalesTaxAmount",
						'discount' => 0,
						'total' => "$order->Total",
						'date_added' => $master->now(),
						'status' => 7, // paid
                        'payment_type' => $payment_type,
						'shipping_first_name' => $name_parts[0],
						'shipping_last_name' => end($name_parts),
						'shipping_address1' => "$shippingAddress->Street1",
						'shipping_address2' => "$shippingAddress->Street2",
						'shipping_city' => "$shippingAddress->CityName",
						'shipping_state' => "$shippingAddress->StateOrProvince",
						'shipping_zip' => explode('-', $shippingAddress->PostalCode)[0],
						'shipping_method' => $shipping_method,
						'comments' => $comments,
						'shipping_type' => 90, // residential
						'site' => 6,
						'site_order_num' => $site_order_num,
						'transaction_id' => $tid,
						'ship_by' => '' // will select earliest from items
					);

                    if(@$argv[1]=='list') {
                        print_r($order_data);
                        echo "\n\n";
                        continue;
                    }

					$transactions = $order->TransactionArray;
					$ordered_items = array();
					$buyer_name = '';
					$buyer_email = '';
					$shipping_total = 0;
					$sbd = '';
					if ($transactions) {
						// iterate through each transaction for the order
						foreach ($transactions->Transaction as $transaction) {
                            if($debug)
                                LL::log(LL::white, json_encode($transaction), LL::light_gray, '---');

							// get the OrderLineItemID, Quantity, buyer's email and SKU
							$pSKU = $transaction->Item->SKU;
							if ($pSKU) {
								$sku = (string)$pSKU;
							}
							
							// if the item is listed with variations, get the variation SKU
							$VariationSKU = $transaction->Variation->SKU;
							if ($VariationSKU != null) {
								$sku = (string)$VariationSKU;
							}
							$transactionPriceAttr = $transaction->TransactionPrice->attributes();
							
							$price = (string)$transaction->TransactionPrice;
							$qty = (string)$transaction->QuantityPurchased;

                            // store lowest sbd
                            $edt = @(string)$transaction->ShippingServiceSelected->ShippingPackageInfo->EstimatedDeliveryTimeMin;
                            if(!empty($edt)) {
                                $item_sbd = date('Y-m-d H:i:s', strtotime('-1 weekday', strtotime($edt)));
                                if(empty($sbd) || ($sbd && $item_sbd<$sbd))
                                    $sbd = $item_sbd;
                            }
							
							$master->where('p.amazon_sku', $sku);
							$master->orWhere('p.id', $sku);
							$master->join('team_list l', 'p.team_id=l.team_id', 'LEFT');
							$prod = $master->getOne('products p', array('p.id', 'p.name', 'p.product_type', 'l.team_name', 'p.team_id', 'p.stock'));
                            if(!$prod) {
                                LL::log(LL::red, "sku $sku not found");
                                $prod_errors[] = $sku;
                                continue;
                            }
							
							$ptype = $prod['product_type'];
							$info = '';
							if(in_array($ptype, $board_types)) {
								if($ptype==26 || $ptype==27) $info = 'Bags:<br>4 corn-filled red<br>4 corn-filled royal blue';
								elseif($ptype==12) $info = 'Bags:<br>4 all-weather red<br>4 all-weather royal blue';
								else $info = 'Bags: 8 corn-filled best matching colors<br>Plus any additional bags listed with this order.';
							}
				
							$ordered_items[] = array(
								'order_id' => 0,
								'product_id' => $prod['id'],
								'name' => $prod['name'],
								'price' => $price,
								'date_added' => $master->now(),
								'product_type' => $prod['product_type'],
								'quantity' => $qty,
								'info' => $info,
                                'stock' => $prod['stock']
							);
							
							// if tumble, free carrying case (include team name in info) 224353 Onyx Stained or No Stain
							if(in_array($prod['product_type'], array(10091, 266))) {
                                $cc = null;
                                if($prod['team_id'] > 0) {
                                    // check for team bag
                                    $slave->where('team_id', $prod['team_id']);
                                    $slave->where('product_type', 269);
                                    $slave->where('active', 'y');
                                    $cc = $slave->getOne('products', array('id', 'name'));
                                }
                                $ccid = count($cc)>0 ? $cc['id'] : 224353;
                                $ccname = count($cc)>0 ? $cc['name'] : 'Tumble Tower Carrying Case';
								//$info = strpos($prod['name'], 'Stained')!==false ? 'Onyx Stained' : 'No Stain';
								$info = '';
								$ordered_items[] = array(
									'order_id' => 0,
									'product_id' => $ccid,
									'name' => $ccname,
									'price' => 0,
									'date_added' => $master->now(),
									'product_type' => 269,
									'quantity' => $qty,
									'info' => $info
								);
                            } elseif($prod['product_type']==10014) { // gameday tower
                                $cc = [];
                                /*if($prod['team_id'] > 0) {
                                    // check for team bag
                                    $slave->where('team_id', $prod['team_id']);
                                    $slave->where('product_type', 10055);
                                    $slave->where('active', 'y');
                                    $cc = $slave->getOne('products', array('id', 'name'));
                                }*/
                                $ccid = count($cc)>0 ? $cc['id'] : 1222675;
                                $ccname = count($cc)>0 ? $cc['name'] : 'Black Gameday Tower Carrying Case';
                                $ordered_items[] = array(
                                    'order_id' => 0,
                                    'product_id' => $ccid,
                                    'name' => $ccname,
                                    'price' => 0,
                                    'date_added' => $master->now(),
                                    'product_type' => 10055,
                                    'quantity' => $qty,
                                    'info' => ''
                                );
                            } elseif($prod['product_type']==148) { // jr package
								$info = $prod['team_name'];
								$ordered_items[] = array(
									'order_id' => 0,
									'product_id' => 24156,
									'name' => 'Custom Junior Bag Set (corn-filled)',
									'price' => 0,
									'date_added' => $master->now(),
									'product_type' => 85, // bags: custom
									'quantity' => $qty,
									'info' => $info
								);
								$ordered_items[] = array(
									'order_id' => 0,
									'product_id' => 24155,
									'name' => 'Custom Junior Carrying Case',
									'price' => 0,
									'date_added' => $master->now(),
									'product_type' => 166, // acc: cc junior
									'quantity' => $qty,
									'info' => $info
								);
							} elseif(in_array($prod['id'], array(1087025,1087026,1087027,1087029,1087030,1087031,1087033,1087034,1087035,1087036,1087037,1087038,1087039,1087040,1087041,1087042,1087043,1087044,1087045,1087046,1087047,1087048,1087049,1087050,1087051,1087052,1087053,1087054,1087055,1087056,1087057,1087058,1087059,1087060,1087061,1087062,1087063,1087064,1087065,1087066,1087067,1087068,1087069,1087070,1087072,1087073,1087074,1087075,1087076,1087077,1087078,1087079,1087080,1087081,1087082,1087083,1087084,1087085,1087086,1087087,1087088,1087089,1087090,1087091,1087092,1087093))) {
                                // march madness package
                                $ordered_items[0]['info'] = 'March madness package';
                                mail('paul@victorytailgate.com', 'eBay MM order', "Check log for recent import: ".$prod['id']);

                                // associate corn filled team bag by team (team_id => bag_sku)
                                $bag_array = array(9 => 24472, 12 => 30408, 22 => 8795, 36 => 168255, 50 => 24807, 90 => 921238, 99 => 14418, 100 => 14550, 107 => 17817, 120 => 8853, 130 => 8855, 131 => 27637, 133 => 8857, 134 => 31003, 145 => 43859, 147 => 29033, 149 => 16699, 155 => 14264, 156 => 23249, 157 => 214402, 158 => 17946, 180 => 18360, 185 => 18352, 198 => 30304, 201 => 23581, 208 => 24841, 211 => 29815, 226 => 17849, 227 => 24674, 245 => 8875, 246 => 18806, 248 => 24032, 250 => 21808, 277 => 9218, 284 => 26143, 285 => 8889, 286 => 16711, 287 => 23786, 288 => 27691, 289 => 23747, 296 => 29695, 299 => 27998, 307 => 8883, 309 => 19017, 320 => 8835, 341 => 9196, 365 => 19134, 402 => 33402, 416 => 26770, 454 => 18284, 470 => 18132, 492 => 18965, 517 => 8803, 537 => 8831, 548 => 9095, 569 => 18124, 600 => 19186, 623 => 25562, 662 => 923245, 710 => 18973, 713 => 18961, 718 => 9105, 732 => 19370, 733 => 18459, 752 => 17799, 907 => 31881);

                                $master->where('id', $bag_array[$prod['team_id']]);
                                $bag_set = $master->getOne('products', array('id', 'name'));
                                $ordered_items[] = array(
                                    'order_id' => 0,
                                    'product_id' => $bag_set['id'],
                                    'name' => $bag_set['name'],
                                    'price' => 0,
                                    'date_added' => $master->now(),
                                    'product_type' => 83,
                                    'quantity' => $qty,
                                    'info' => 'Included in march madness package'
                                );
                            }
							
							if(in_array($prod['product_type'], array(10091, 266)) || in_array($prod['product_type'], $board_types))
								$alert = true;
							
							// get missing order info
							$buyer_name = $transaction->Buyer->UserFirstName.' '.$transaction->Buyer->UserLastName;
							$buyer_email = (string)$transaction->Buyer->Email;
							$shipping_total += (float)$transaction->ActualShippingCost;
						}
					}

					if(empty($sbd) && !empty($ordered_items)) {
						// get leadtime from database
						$product_types = array_column($ordered_items, 'product_type');
						$master->where("product_type_id", $product_types, "IN");
						$master->where("shipping_offset IS NOT NULL");
						$leadtime = $master->getOne("product_types", "MAX(shipping_offset) as shipping_offset");
                        //$leadtime = $leadtime['shipping_offset'] > 0 ? $leadtime['shipping_offset'] : 3;
                        $leadtime = $leadtime['shipping_offset'] > 0 ? $leadtime['shipping_offset'] : 3;

                        if($shipping_method=='Pickup') {
                            --$leadtime; // one less day for local pickup
                            $order_data['shipping_address1'] = 'Pickup';
                            $order_data['shipping_address2'] = '';
                            $order_data['shipping_city'] = 'Pickup';
                            $order_data['shipping_state'] = '';
                            $order_data['shipping_zip'] = '';
                        }

                        $sbd = date('Y-m-d H:i:s', strtotime("+$leadtime weekday", time()));
					}

                    $thresh = date('Y-m-d 00:00:00', strtotime('+3 weekday'));
                    if($sbd > $thresh)
                        $sbd = $thresh;

                    // skip weekends, days off
                    while(in_array(date('D', strtotime($sbd)), array('Sat', 'Sun')) || in_array(date('Y-m-d', strtotime($sbd)), $days_off)) {
                        $sbd = date('Y-m-d H:i:s', strtotime('+1 weekday', strtotime($sbd)));
                    }
					
					/**
					 * INSERT ORDER
					 */
					$order_data['name'] = "$buyer_name";
					$order_data['email'] = "$buyer_email";
					$order_data['shipping'] = $shipping_total;
					$order_data['ship_by'] = $sbd;
					
					if($debug) {
                        $order_id = 123;
						print_r($order_data);
					} else {
						// do insert
						$order_id = $master->insert('orders', $order_data);
                        $alert_orders[] = $order_id; // alert everything for now
						if(!$order_id) {
							LL::log(LL::red, "Order insert failed on $site_order_num:\n\t".$master->getLastError());
							continue;
						} else {
							LL::log(LL::green, "Order inserted: $order_id / $site_order_num / c: {$order->CreatedTime} / sbd: $sbd");
                            // email if comment existed
                            if(!empty($order_data['comments']))
                                $alert_comments[$order_id] = $order_data['comments'];
						}
					}
					
					/**
					 * INSERT ORDERED ITEMS
					 */
                    $cart = new Cart($master);
                    if(count($ordered_items)==0) {
                        $body = '{
                            "message": "eBay Order Import error",
                            "description": "No items on order '.$order_id.' / '.$site_order_num.'",
                            "teams": [{"name": "Web"}],
                            "tags": ["eBay", "Crond"],
                            "priority": "P3"
                        }';
                        sendAlert($body);
                    }

					foreach($ordered_items as $item) {
                        
                        $stock = isset($item['stock']) ? $item['stock'] : 0;
                        unset($item['stock']);
                        
                        // check slots
                        $slot_ids = false;
                        $slots = $cart->GetSlotNums($item['product_id'], $item['quantity']);
                        if(count($slots)>0) {
                            $slot_ids = array_keys($slots);
                            //$item['slot_nums'] = implode(',', $slots);
                        }

                        // do insert
                        $item['order_id'] = $order_id;

                        // split qty
                        for($i=0; $i<$item['quantity']; $i++) {
                            $insert = $item;
                            $insert['quantity'] = 1;
                            $insert['slot_nums'] = isset($slots[$i]) ? $slots[$i] : "";
                            if($debug) {
                                LL::log(LL::white, "Insert {$insert['quantity']} {$insert['name']} / {$insert['product_id']}");
                            } else {
                                $item_id = $master->insert('ordered_items', $insert);
                                if (!$item_id) {
                                    $msg = "Item insert failed on $order_id / $site_order_num:\n\t" . $master->getLastError();
                                    LL::log(LL::red, $msg);
                                    $body = '{
                                        "message": "eBay Order Import error",
                                        "description": "'.$msg.'",
                                        "teams": [{"name": "Web"}],
                                        "tags": ["eBay", "Crond"],
                                        "priority": "P3"
                                    }';
                                    sendAlert($body);
                                    continue;
                                } else {
                                    /**
                                     * remove slot
                                     */
                                    if($insert['slot_nums'] != "") {
                                        $master->where('slot_num', $insert['slot_nums']);
                                        $master->where('product_id', $insert['product_id']);
                                        $master->where('quantity', 0, '>');
                                        $upd_data = array('quantity' => $master->dec(1));
                                        if ($master->update('products_slots', $upd_data, 1))
                                            LL::log(LL::cyan, "slot {$insert['slot_nums']} used");
                                    }
                                }
                            }
                        }

                        // delete slot
                        /*if($slot_ids) {
                            $master->where('slot_id', $slot_ids, 'IN');
                            $master->delete('products_slots', count($slot_ids));
                            LL::log(LL::light_blue, "\tprev stock: $stock / slot_ids used: ".implode(',', $slot_ids));
                        }*/

                        // reduce stock
                        $item_update_data = array(
                            'stock' => $master->dec($item['quantity'])
                        );
                        $master->where('id', $item['product_id']);
                        $master->where('stock', $item['quantity'], '>');
                        $master->update('products', $item_update_data);
					}
					
				} //end if
			}
			
			/**
			 * POST BACK
			 */
			// add order id to ebay order note field?
			
			// send alerts
			if(!empty($alert_orders)) {
				LL::log(LL::yellow, count($alert_orders)." orders to alert");
				if(!$debug) {
					$headers = "Content-Type: text/html; charset=ISO-8859-1\r\n";
					$headers .= "MIME-Version: 1.0\r\n";
					$msg = '<strong>Check Imported eBay Orders:</strong>';

					foreach($alert_orders as $order_id)
						$msg .= '<br><a href="http://vt-prod-legacy-admin/admin/vtg.php?id='.$order_id.'">'.$order_id.'</a>';

                    if(!empty($alert_comments)) {
                        $msg .= '<br>See comments on the following orders:';
                        foreach ($alert_comments as $order_id => $comment)
                            $msg .= '<br><a href="http://vt-prod-legacy-admin/admin/vtg.php?id=' . $order_id . '">' . $order_id . '</a>: '.$comment;
                    }

					$msg .= "<br><br>Don't forget to add note in <a href=\"http://k2b-bulk.ebay.com/ws/eBayISAPI.dll?SalesRecordConsole&status=WaitShipment&currentPage=SCSold&ssPageName=STRK:ME:LNLK\">eBay</a>";

					mail('dropship@victorytailgate.com, pford@victorytailgate.com', 'Check Imported eBay Orders', $msg, $headers);
				}
			}
		} else {
			LL::log(LL::red, "No Order Found");
		}

        LL::log(LL::white, "END");
	}
}

$curl = new \Curl\Curl();
$curl->setHeader('content-Type', 'application/json');
$curl->setHeader("authorization", "GenieKey c8715792-5166-480b-8983-a21976478dda");
$curl->get('https://api.opsgenie.com/v2/heartbeats/EbayImport/ping');
if ($curl->error)
    LL::log(LL::red, $curl->errorMessage);


/**
 * Send OpsGenie alert
 */
function sendAlert($body) {
    $curl = new \Curl\Curl();
    $curl->setHeader('content-Type', 'application/json');
    $curl->setHeader("authorization", "GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf");
    $curl->post('https://api.opsgenie.com/v2/alerts', $body);
    if ($curl->error)
        LL::log(LL::red, $curl->errorMessage);
}