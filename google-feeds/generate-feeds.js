process.stdout.isTTY = true // This makes colors work in webstorm "oowee ..?"
process.env.TZ = 'America/New_York'
const async     = require('async')
const colors    = require('colors').enabled = true
const mysql     = require('mysql')
const fs        = require('fs')
const json2csv = require('json2csv').Parser
var client = require('scp2')
const ftp = require('ftp')
const moment = require('moment-timezone')
moment.tz.setDefault('America/New_York')

const mageip = '54.89.135.236'
const feedpath = '/var/www/magento2/pub/google-feeds/'
const nopush = process.argv.indexOf('nopush') >= 0
/** specific feeds argument */
let arg = process.argv.find(i => i.indexOf('feeds=') >= 0); //console.log(confArg)
const FEEDS = arg ? arg.substring(6).split(',') : []

var connection_mage = mysql.createConnection({
    host: 'vt-magento.cbtmqjwywgwy.us-east-1.rds.amazonaws.com',
    user: 'magento',
    password: 'z2sEGz42Mc7wVNTx347ZWhjxn3NNfGUT2xJNEjHL',
    database: 'magento'
});
var connection_vt = mysql.createConnection({
    host     : 'victory-prod-temp2-cluster.cluster-cbtmqjwywgwy.us-east-1.rds.amazonaws.com',
    user     : 'magento_sync',
    password : 'r4KEQaYgKqHYCB6GKCpdGsn3CqfL8BLdwdV3raQH',
    database : 'tgsgames_corn2'
});
var upcs = []
var lines = []
var combined = []
var feeds = [
    {
        name: "boards",
        upcs: "select id, upc, t.short_description from products p inner join product_types t on t.product_type_id=p.product_type where id!=4036 AND product_type IN (24,25,26,27) and p.active='y' and upc is not null and upc!='' and upc!=0;",
        prods: "select p.sku as id, pp.sku as id_true, v.name as title, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', replace(pp.sku, 'gp', 'pp'), '-0-', replace(substring_index(u.request_path, '/', -1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price*2+10.00, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Sporting Goods > Outdoor Games > Cornhole' as product_type, 'US:FL:Ground:30 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as gender from catalog_product_entity p inner join catalog_product_flat_1 v on v.row_id=p.row_id inner join catalog_product_link l on l.linked_product_id=p.entity_id inner join catalog_product_entity pp on pp.row_id=l.product_id inner join url_rewrite u on u.entity_id=pp.entity_id where instr(u.request_path, '/') > 1 and v.name not like '%baggo%' and v.parent_product_type_value='Cornhole Board' GROUP BY p.sku;"
    },
    {
        name: "desktop-boards",
        upcs: "select id, upc, t.short_description from products p inner join product_types t on t.product_type_id=p.product_type where id!=4036 AND product_type IN (73) and p.active='y' and upc is not null and upc!='' and upc!=0;",
        prods: "select v.sku as id, v.parent_product_type_value, v.name as title, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring(substring_index(u.request_path, '/', -1) from instr(substring_index(u.request_path, '/', -1), '-') +1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Sporting Goods > Outdoor Games > Cornhole' as product_type, 'US:FL:Ground:10 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_flat_1 v inner join catalog_product_entity p on p.row_id=v.row_id inner join url_rewrite u on u.entity_id=p.entity_id where instr(u.request_path, '/') > 1 and v.name like '%desktop%' and v.parent_product_type_value='Cornhole Board' GROUP BY v.sku;"
    },
    {
        name: "bags",
        upcs: "select id, upc from products where product_type IN (81) and active='y' and upc is not null and upc!='' and upc!=0;",
        prods: "select v.sku as id, v.name as title, 'Our cornhole bags are tournament-grade using regulation size and weight. Our bags are made of quality duck cloth material measuring 6\"x6\" and weigh between 15-16 ounces as prescribed by the ACA filled with whole kernel corn.' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring_index(u.request_path, '/', -1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price*2, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Cornhole > Accessories > Bags' as product_type, 'US:FL:Ground:5 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_entity p inner join catalog_product_flat_1 v on v.row_id=p.row_id inner join catalog_product_super_link l on l.product_id=p.entity_id inner join catalog_product_entity pp on pp.row_id=l.parent_id inner join url_rewrite u on u.entity_id=pp.entity_id where instr(u.request_path, '/') > 1 and v.parent_product_type_value='Cornhole Bag' and v.name not like '%solid%' GROUP BY v.sku;"
    },
    {
        name: "bags-solid-cf",
        upcs: "select id, upc from products where product_type IN (82) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select v.sku as id, v.parent_product_type_value, v.name as title, 'Our cornhole bags are tournament-grade using regulation size and weight. Our bags are made of quality duck cloth material measuring 6\"x6\" and weigh between 15-16 ounces as prescribed by the ACA filled with whole kernel corn.' as description, replace(concat('https://www.victorytailgate.com/', substring(substring_index(u.request_path, '/', -1) from instr(substring_index(u.request_path, '/', -1), '-') +1)), 'corn-filled', '') as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring(substring_index(u.request_path, '/', -1) from instr(substring_index(u.request_path, '/', -1), '-') +1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Cornhole > Accessories > Bags' as product_type, 'US:FL:Ground:5 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_flat_1 v inner join catalog_product_entity p on p.row_id=v.row_id inner join url_rewrite u on u.entity_id=p.entity_id where instr(u.request_path, '/') = 0 and v.name like '%solid%' and v.name like '%corn filled%' and v.parent_product_type_value='Cornhole Bag' GROUP BY v.sku;"
    },
    {
        name: "baggo",
        upcs: "select id, upc from products where product_type IN (10020) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select v.sku as id, v.name as title, 'This Officially licensed BAGGO tailgate game is a portable cornhole game set, that is fun for all ages & occasions. Game boards are constructed of a lightweight blow-molded high-impact polyethylene, which provides lasting durability for a Lifetime of enjoyment. BAGGO is compact & can fit into the trunk of any car so that you can enjoy playing anywhere & everywhere.' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring_index(u.request_path, '/', -1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Sporting Goods > Outdoor Games > Cornhole' as product_type, 'US:FL:Ground:30 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_flat_1 v inner join catalog_product_entity p on p.row_id=v.row_id inner join catalog_product_link l on l.linked_product_id=p.entity_id inner join catalog_product_entity pp on pp.row_id=l.product_id   inner join url_rewrite u on u.entity_id=pp.entity_id where instr(u.request_path, '/') > 1 and v.name like '%baggo%' and v.parent_product_type_value='Cornhole Board' GROUP BY v.sku;"
    },
    {
        name: "tumble",
        upcs: "select id, upc from products where product_type IN (266) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select v.sku as id, v.parent_product_type_value, v.name as title, 'Show off your team spirit with this GIANT wooden tumble tower game! Blocks are laser engraved with the team logo(s) as shown in the product image. Our most popular and Largest Tumbling Tower, includes 54 solid wood game pieces that provides 18 vertical rows of tumbling tower fun and excitement. This set can be enjoyed at any outdoor or indoor party and is awesome for any group event. Each wooden block measures 1.5 inches x 3.5 inches x 10.5 inches and the Tower Size is 10.5 inches x 10.5 inches x 27 inches. Plays up to 5ft or so before tumbling. Great fun! Shipping weight is approximately 40 lbs. This game also Includes the team carrying case! This is an officially licensed product.' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring(substring_index(u.request_path, '/', -1) from instr(substring_index(u.request_path, '/', -1), '-') +1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Other Games > Tumble Towers' as product_type, 'US:FL:Ground:20 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_flat_1 v inner join catalog_product_entity p on p.row_id=v.row_id inner join catalog_product_link l on l.linked_product_id=p.entity_id inner join catalog_product_entity pp on pp.row_id=l.product_id inner join url_rewrite u on u.entity_id=pp.entity_id where instr(u.request_path, '/') > 1 and v.name like '%tumble%' and u.request_path not like '%onyx%' and v.name not like '%onyx%' and v.name not like '%gameday%' and v.parent_product_type_value='Tumble Tower' GROUP BY v.sku;"
    },
    {
        name: "tumble-onyx",
        upcs: "select id, upc from products where product_type IN (10091) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select v.sku as id, v.parent_product_type_value, v.name as title, 'Show off your team spirit with this GIANT wooden tumble tower game! Blocks are laser engraved with the team logo(s) as shown in the product image. Our most popular and Largest Tumbling Tower, includes 54 solid wood game pieces that provides 18 vertical rows of tumbling tower fun and excitement. This set can be enjoyed at any outdoor or indoor party and is awesome for any group event. Each wooden block measures 1.5 inches x 3.5 inches x 10.5 inches and the Tower Size is 10.5 inches x 10.5 inches x 27 inches. Plays up to 5ft or so before tumbling. Great fun! Shipping weight is approximately 40 lbs. This game also Includes the team carrying case! This is an officially licensed product.' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring(substring_index(u.request_path, '/', -1) from instr(substring_index(u.request_path, '/', -1), '-') +1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Other Games > Tumble Towers' as product_type, 'US:FL:Ground:20 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_flat_1 v inner join catalog_product_entity p on p.row_id=v.row_id inner join catalog_product_link l on l.linked_product_id=p.entity_id inner join catalog_product_entity pp on pp.row_id=l.product_id inner join url_rewrite u on u.entity_id=pp.entity_id where instr(u.request_path, '/') > 1 and v.name like '%tumble%' and u.request_path like '%onyx%' and v.name like '%onyx%' and v.name not like '%gameday%' and v.parent_product_type_value='Tumble Tower' GROUP BY v.sku;"
    },
    {
        name: "gameday",
        upcs: "select id, upc from products where product_type IN (10014) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select v.sku as id, v.parent_product_type_value, v.name as title, 'How high can you go? Stack the 54 laser engraved wooden blocks as high as 3.5 feet before it tumbles. Includes carrying case with handles for easy transportation from one event to the next. Be the tailgate hero with this fun addition.' as description, concat('https://www.victorytailgate.com/', substring(substring_index(u.request_path, '/', -1) from instr(substring_index(u.request_path, '/', -1), '-') +1)) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring(substring_index(u.request_path, '/', -1) from instr(substring_index(u.request_path, '/', -1), '-') +1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Other Games > Tumble Towers' as product_type, 'US:FL:Ground:20 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_flat_1 v inner join catalog_product_entity p on p.row_id=v.row_id inner join url_rewrite u on u.entity_id=v.entity_id where instr(u.request_path, '/') > 1 and v.name like '%gameday%' and v.parent_product_type_value='Tumble Tower' GROUP BY v.sku;"
    },
    {
        name: "tiki",
        upcs: "select id, upc from products where product_type IN (10044, 10045) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select v.sku as id, v.parent_product_type_value, v.name as title, 'This new Victory Tiki Toss is the ultimate game. Easy to set up, difficult to master this deluxe version of the classic ring toss includes a 5 foot telescope rod that allows for quick set up in the open air. Made of 100% bamboo and premium metal rink and hook, this game will last for a lifetime of memories and rivalries.' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring(substring_index(u.request_path, '/', -1) from instr(substring_index(u.request_path, '/', -1), '-') +1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Other Games > Tiki Toss' as product_type, 'US:FL:Ground:10 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_flat_1 v inner join catalog_product_entity p on p.row_id=v.row_id inner join url_rewrite u on u.entity_id=p.entity_id where instr(u.request_path, '/') > 1 and v.parent_product_type_value like '%tiki%' GROUP BY v.sku;"
    },
    {
        name: "checkers",
        upcs: "select id, upc from products where product_type IN (10043) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select v.sku as id, v.parent_product_type_value, v.name as title, 'This new Victory Checkers is your 2-in-1 tailgate go to! Boasting the classic checkerboard in your team colors on one side and all weather canvas on the other, this game also doubles as an outdoor blanket. Proudly made in the USA using double stitched PVC backed canvas, the bottom keeps you free of moisture or dirt on the ground. Included are 24 embellished wooden 5.25\" discs. and accompanied with a drawstring carrying case, this game is easy to grab and go to a tailgate, the beach or just fun in the backyard.' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring(substring_index(u.request_path, '/', -1) from instr(substring_index(u.request_path, '/', -1), '-') +1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Sporting Goods > Outdoor Recreation > Outdoor Games > Lawn Games' as google_product_category, 'Other Games > Giant Checkers' as product_type, 'US:FL:Ground:10 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_flat_1 v inner join catalog_product_entity p on p.row_id=v.row_id inner join url_rewrite u on u.entity_id=p.entity_id where instr(u.request_path, '/') > 1 and v.parent_product_type_value like '%checkers%' GROUP BY v.sku;"
    },
    {
        name: "canvas",
        upcs: "select id, upc from products where product_type IN (10035) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select p.sku as id, pp.sku as id_true, v.name as title, 'A perfect way to show your team spirit! Our canvases are made of a durable, high quality canvas featuring digitally printed graphics in HD. Installed onto sturdy wood frames to last a lifetime.' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring_index(u.request_path, '/', -1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Home & Garden > Decor > Artwork' as google_product_category, 'Canvas Art' as product_type, case when (v.name like '%36%' or v.name like '%48%') then 'US:FL:Ground:50 USD' else 'US:FL:Ground:15 USD' end as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_entity p inner join catalog_product_flat_1 v on v.row_id=p.row_id inner join catalog_product_super_link l on l.product_id=p.entity_id inner join catalog_product_entity pp on pp.row_id=l.parent_id inner join url_rewrite u on u.entity_id=pp.entity_id where instr(u.request_path, '/') > 1 and v.parent_product_type_value='Canvas Art' and v.name like '%12x12%' GROUP BY v.sku;"
    },
    {
        name: "decaldc",
        upcs: "select id, upc from products where product_type IN (10075,10076,10074) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select p.sku as id, pp.sku as id_true, v.name as title, 'A great option for anyone wanting to make their own cornhole boards. Already die-cut for easy application and ready to apply upon arrival. We recommend pairing our decals with our unfinished board set for an enthusiast’s DIY project. These make great gift' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring_index(u.request_path, '/', -1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Home & Garden > Decor > Home Decor Decals' as google_product_category, 'Decals' as product_type, 'US:FL:Ground:3 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_entity p inner join catalog_product_flat_1 v on v.row_id=p.row_id inner join catalog_product_super_link l on l.product_id=p.entity_id inner join catalog_product_entity pp on pp.row_id=l.parent_id inner join url_rewrite u on u.entity_id=pp.entity_id where instr(u.request_path, '/') > 1 and v.parent_product_type_value='Decal' and v.name like '%6x6%' GROUP BY v.sku;"
    },
    {
        name: "tables",
        upcs: "select id, upc from products where product_type IN (93,94) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select p.sku as id, v.name as title, 'Proudly show off your team spirit with this officially licensed portable 8 foot long tailgate table!' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring_index(u.request_path, '/', -1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Home & Garden > Decor > Home Decor Decals' as google_product_category, 'Decals' as product_type, 'US:FL:Ground:20 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_entity p inner join catalog_product_flat_1 v on v.row_id=p.row_id inner join url_rewrite u on u.entity_id=p.entity_id where instr(u.request_path, '/') > 1 and v.parent_product_type_value='Tailgate Table' GROUP BY v.sku;"
    },
    {
        name: "washer",
        upcs: "select id, upc from products where product_type IN (99) and active='y' and upc is not null and upc!='' and upc!=0",
        prods: "select p.sku as id, v.name as title, 'Show off your team spirit with our premium quality onyx stained washer game set. Team logos are laser engraved for durability and an awesome look. Set comes finished and ready to play.' as description, concat('https://www.victorytailgate.com/', u.request_path) as link, concat('https://www.victorytailgate.com/img/', v.sku, '-0-', replace(substring_index(u.request_path, '/', -1), '.html', '.jpg')) as image_link, 'new' as `condition`, 'in stock' as availability, concat(round(v.price, 2), ' USD') as price, 'Victory Tailgate' as brand, v.sku as mpn, '' as GTIN, 'Home & Garden > Decor > Home Decor Decals' as google_product_category, 'Decals' as product_type, 'US:FL:Ground:15 USD' as shipping, '' as size, '' as color, '' as 'age group', '' as	gender from catalog_product_entity p inner join catalog_product_flat_1 v on v.row_id=p.row_id inner join url_rewrite u on u.entity_id=p.entity_id where instr(u.request_path, '/') > 1 and v.parent_product_type_value='Washer Game' GROUP BY v.sku;"
    }
]

console.log(moment().format().blue)
async.eachSeries(feeds, (feed, nextFeed) => {
    if(FEEDS && FEEDS.length > 0 && FEEDS.indexOf(feed.name) == -1) {
        nextFeed()
    } else {
        lines = []
        process.stdout.write(feed.name + '...')
        async.waterfall([
            (cb) => {
                var sql = feed.upcs;
                connection_vt.query(sql, [], cb)
            }, (rows, fields, cb) => {
                upcs = rows

                var sql = feed.prods
                connection_mage.query(sql, [], cb)
            }, (rows, fields, cb) => {
                for (var row of rows) {
                    for (var o of upcs)
                        if (o.id == row.id) {
                            row.GTIN = o.upc
                            if('short_description' in o)
                                row.description = o.short_description
                        }

                    if (row.GTIN != '') {
                        if ('id_true' in row) {
                            //console.log('group id found...')
                            // id_true is the grouped id for the google feed, criteo needs the other (simple) id
                            var rowc = Object.assign({}, row)
                            delete rowc.id_true
                            combined.push(rowc)

                            // google
                            row.id = row.id_true
                            delete row.id_true
                            lines.push(row)
                        } else {
                            combined.push(row)
                            lines.push(row)
                        }
                    }
                }
                process.stdout.write(lines.length + '...')
                cb()
            }
        ], (err) => {
            if (err) console.error(err)
            else {
                var keys = ['id', 'title', 'description', 'link', 'image_link', 'condition', 'availability', 'price', 'brand', 'mpn', 'GTIN', 'google_product_category', 'product_type', 'shipping', 'size', 'color', 'age group', 'gender']
                var parser = new json2csv({fields: keys, del: '\t'})
                //console.log(lines)
                try {
                    var tsv = parser.parse(lines)
                    var filename = __dirname + '/' + feed.name + '.txt'
                    fs.writeFile(filename, tsv, (err) => {
                        if (err) console.error(err)
                        else if(nopush) { console.log(`no upload, wrote file: ${filename}`.magenta); nextFeed() }
                        else {
                            process.stdout.write('done...')

                            client.scp(filename, {
                                host: mageip,
                                username: 'root',
                                privateKey: fs.readFileSync('/root/.ssh/id_rsa'),
                                path: feedpath
                            }, (err)=> {
                                if (err) console.error(err)
                                else console.log('uploaded'.green)
                                nextFeed()
                            })
                        }
                    })
                } catch (error) {
                    console.error(error)
                    nextFeed()
                }
            }
        })
    }
}, (err) => {
    if(err) console.error(err)
    console.log('indies done'.yellow)
    console.log(combined.length + ' total')

    // for one-offs, no need to run combined
    if(FEEDS.length > 0 && FEEDS.indexOf('combined') == -1)
        process.exit()

    var keys = ['id', 'title', 'description', 'link', 'image_link', 'condition', 'availability', 'price', 'brand', 'mpn', 'GTIN', 'google_product_category', 'product_type', 'shipping', 'size', 'color', 'age group', 'gender']
    var parser = new json2csv({fields: keys, del: '\t'})
    var tsv = parser.parse(combined)
    var filename = __dirname + '/combined-vt.txt'
    fs.writeFile(filename, tsv, (err) => {
        if (err) console.error(err)
        else if(nopush) { console.log(`no upload, wrote file: ${filename}`.magenta); process.exit() }
        else {
            //console.log('combined done'.green)
            process.stdout.write('uploading combined feed...')
            async.waterfall([
                (nextPush) => {
                    // magento
                    client.scp(filename, {
                        host: mageip,
                        username: 'root',
                        privateKey: fs.readFileSync('/root/.ssh/id_rsa'),
                        path: feedpath
                    }, (err)=>{
                        if(err) console.error(err)
                        else process.stdout.write('uploaded to VT...'.green)
                        nextPush()
                    })
                }, (done) => {
                    // criteo
                    var ftpc = new ftp()
                    ftpc.on('ready', () => {
                        ftpc.put(filename, 'feed.txt', (err) => {
                            if(err) console.log(err)
                            else console.log('uploaded to Criteo...'.green)
                            ftpc.end()
                            done()
                        })
                    })
                    ftpc.connect({
                        host: 'data-ftp.criteo.com',
                        port: 21,
                        user: 'top_cae3998a-1bb1-4433-a856-5f2925fb4a14',
                        password: 'abdec906-013a-49a2-94cb-88a2460a5785'
                    })
                }
            ], () => {
                //if(err) console.error(err)
                console.log('done'.green + "\n")
                process.exit()
            })
        }
    })
})

