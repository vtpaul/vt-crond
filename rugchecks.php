<?php
require_once('../shared-resources/_configs/configs.inc');
require_once('../shared-resources/lurdlogger.php');

$alerts_email = 'it.alerts@victorytailgate.com';
$headers = "From: $alerts_email";

LL::log(LL::light_blue, "\n".date("Y-m-d H:i:s"));

/**
 * check slave

echo "check replication...";
$tries = 0;
$retry = true;
while($retry && $tries<5) {
	$retry = false;
	LL::log(LL::white, 'Trying connection...');
	try {
		$master_order = $master->getOne('orders', 'MAX(order_id) as order_id');
		$slave_order = $slave->getOne('orders', 'MAX(order_id) as order_id');
	} catch(Exception $e) {
		LL::log(LL::red, $e);
		$tries++;
		$retry = true;
		sleep(5);
	}
}
if($tries>=5) {
	LL::log(LL::red, 'Connection failed');
	mail($alerts_email, "Rugcheck can't connect to MySQL", "Rugcheck can't connect to MySQL", $headers);
	die();
}
if($master_order['order_id']!=$slave_order['order_id']) {
	LL::log(LL::white, "\nLast master order:", LL::red, $master_order['order_id'], LL::white, " - Last slave order:", LL::red, $slave_order['order_id']);
	$subj = "Replication Alert";
	$body = "Last master order:".$master_order['order_id']." \n Last slave order:".$slave_order['order_id']." \n";
	mail($alerts_email, $subj, $body, $headers);
} else {
	LL::log(LL::green, "ok");
}*/

/**
 * fix missing dropship info
 */
LL::log(LL::white, 'update order dropship info...');
$r = $slave->rawQuery("
select o.order_id from orders o
  LEFT JOIN marketplaces m ON o.site = m.site_id
WHERE
  o.date_added > date_sub(now(), interval 1 day) AND (o.site IN (11, 24, 28, 32, 38, 50, 10004, 10005, 10012)) AND
  o.dropship_carrier = '' AND o.dropship_account = '' AND o.dropship_zip = '';");
if(count($r)>0) echo json_encode($r)."\n";

$r = $slave->rawQuery("
UPDATE orders o
  LEFT JOIN marketplaces m ON o.site = m.site_id
SET o.dropship_account = m.ds_account, o.dropship_carrier = m.ds_carrier, o.dropship_zip = m.ds_zip, o.dropship_country = 'US'
WHERE
  o.date_added > date_sub(now(), interval 1 day) AND (o.site IN (11, 24, 28, 32, 38, 50, 10004, 10005, 10012)) AND
  o.dropship_carrier = '' AND o.dropship_account = '' AND o.dropship_zip = '';");

$r = $slave->rawQuery("
select o.order_id from orders o
  LEFT JOIN dealers d ON o.dealer_id = d.id
WHERE
o.date_added > date_sub(now(), interval 1 day) AND (o.dealer_id IN (512, 550, 706)) AND o.dropship_carrier = '' AND
o.dropship_account = '' AND o.dropship_zip = '';");
if(count($r)>0) echo json_encode($r)."\n";

$r = $slave->rawQuery("
UPDATE orders o
  LEFT JOIN dealers d ON o.dealer_id = d.id
SET o.dropship_account = d.ds_account, o.dropship_carrier = d.ds_carrier, o.dropship_zip = d.ds_zip, o.dropship_country = 'US'
WHERE
  o.date_added > date_sub(now(), interval 1 day) AND (o.dealer_id IN (512, 550, 706)) AND o.dropship_carrier = '' AND
o.dropship_account = '' AND o.dropship_zip = '';");

/**
 * mage missing vto
 */
LL::log(LL::white, 'update mage VTOs...');
$r = $missed = $magento->rawQuery("
update sales_order_grid g
  inner join sales_order o on g.entity_id=o.entity_id
  inner join quote q on q.entity_id=o.quote_id
set g.vt_order_id=q.vt_order_id
where g.vt_order_id is null and q.vt_order_id is not null;");
if(count($r)>0) print_r($r);
$r = $missed = $magento->rawQuery("
update sales_order o
  inner join quote q on q.entity_id=o.quote_id
set o.vt_order_id=q.vt_order_id
where o.vt_order_id is null and q.vt_order_id is not null;");
if(count($r)>0) print_r($r);

/**
 * check for dupe amazon orders
 */
echo "check amzn dupes...";
$dupes = $slave->rawQuery("
SELECT date_added,site_order_num,COUNT(*) AS cnt 
FROM orders 
WHERE date_added > date_sub(now(), interval 1 day)
AND status not in (3,4)
AND site=3
GROUP BY site_order_num
HAVING cnt>1
ORDER BY date_added DESC;");
if(count($dupes)>0) {
	LL::log(LL::red, "\n".count($dupes)." dupe Amazon orders");
	$body = $subj = count($dupes)." dupe amazon orders";
	$body .= "\n\n";
	foreach($dupes as $dupe)
		$body .= $dupe['site_order_num']." - ".$dupe['cnt']."\n";
	mail($alerts_email, $subj, $body, $headers);
} else {
	LL::log(LL::green, "ok");
}
	
/**
 * check for itemless orders
 */
echo "check itemless orders...";
$itemless = $slave->rawQuery("
SELECT o.date_added, o.order_id, o.name, o.email, count(i.item_id) AS cnt
FROM orders o
  LEFT JOIN ordered_items i ON o.order_id=i.order_id
WHERE o.date_added > date_sub(now(), interval 1 day)
  AND o.status in (2,7)
GROUP BY o.order_id
HAVING cnt < 1;");
if(count($itemless)>0) {
	//LL::log(LL::red, count($itemless));
    LL::log(LL::yellow, json_encode(array_column($itemless, 'order_id')));
	$body = $subj = count($itemless)." itemless orders:";
	foreach($itemless as $order) 
		$body .= " ".$order['order_id'];

    $alert = '{
        "message": "Itemless orders",
        "description": "'.$body.'",
        "teams": [{"name": "Web"}],
        "tags": ["Rugcheck", "Orders"],
        "priority": "P3"
    }';
    //sendAlert($alert);
} else {
	LL::log(LL::green, "ok");
}

/**
 * check for faulty sbd
 */
echo "check bad sbd...";
$sbd = $slave->rawQuery("
SELECT * FROM orders WHERE ship_by='1970-01-01 00:00:00'
AND date_added > date_sub(now(), interval 1 day);");
if(count($sbd)>0) {
	LL::log(LL::red, "\n".count($sbd)." bad SBDs");
	$body = $subj = count($sbd)." bad SBDs";
	$body .= "\n\n";
	foreach($sbd as $order) 
		$body .= $order['order_id']."\n";
	mail($alerts_email, $subj, $body, $headers);
} else {
	LL::log(LL::green, "ok");
}


/**
 * Amzn check for low order volume
 */
if(date("g")==7) {
    echo "check Amazon order volume...\n\t";
    $interval = 12;
    $orders = $slave->rawQuery("SELECT order_id, date(ship_by) as ship_by FROM orders WHERE date_added>=DATE_SUB(NOW(), INTERVAL $interval HOUR) AND site=3");
    $count = count($orders);
    if ($count == 0) {
        LL::log(LL::red, $count . " Amazon SC orders last $interval hours");
        //$body = $subj = "No Amazon SC orders in last 6 hours";
        //mail('paul@victorytailgate.com', $subj, $body, $headers);
        $body = '{
            "message": "Amazon SC low order volume",
            "description": "No orders last ' . $interval . ' hours",
            "teams": [{"name":"Web"}],
            "tags": ["Amazon", "Order Volume"],
            "priority": "P3"
        }';
        sendAlert($body);
    } else {
        LL::log(LL::green, $count . " Amazon SC orders last 6 hours");
        /*foreach($orders as $order)
            LL::log(LL::yellow, "\t".$order['order_id'].' / '.$order['ship_by']);*/
    }
}


/**
 * VT check for low order volume
 */
if(date("g")==7) {
    echo "check VT order volume...\n\t";
    $interval = 12;
    $orders = $slave->rawQuery("SELECT count(*) AS cnt FROM orders WHERE date_added>=DATE_SUB(NOW(), INTERVAL $interval HOUR) AND site=0 AND status NOT IN (3,4)");
    if ($orders[0]['cnt'] == 0) {
        LL::log(LL::red, $orders[0]['cnt'] . " VT orders last $interval hours");
        $body = '{
        "message": "VT low order volume",
        "description": "No orders last ' . $interval . ' hours",
        "teams": [{"name":"Web"}],
        "tags": ["VT", "Order Volume"],
        "priority": "P3"
    }';
        sendAlert($body);
    } else {
        LL::log(LL::green, $orders[0]['cnt'] . " VT orders last hour");
    }
}


/**
 * check for unshipped orders with all shipped items
 */
echo "check unshipped";
$orders = $slave->rawQuery("
select o.order_id, o.date_added, if(o.shipping_method like '%pickup%', 1, 0) as pickup from orders o
inner join ordered_items i on o.order_id=i.order_id
where i.item_status in (7,5)
and o.status!=3 and o.status!=4
group by o.order_id
limit 1000;
");
$unshipped = array();
foreach($orders as $order) {
	echo ".";
	$slave->where('order_id', $order['order_id']);
	$items = $slave->get('ordered_items');
	foreach($items as $item) {
		// if anything in the orders isn't shipped or picked up, go next
		if(!in_array($item['item_status'], array(4,7,5,8,10,11))) {
			continue 2;
		}
	}
	$unshipped[] = $order['order_id'];
}
if(count($unshipped)>0) {
	
	// paul 2016-11-01: spoke to Nicole, just update the orders instead of sending alert
	$master->where('order_id', $unshipped, "IN");
	if($master->update('orders', array('status' => 3))) {
		echo "\norders marked shipped:\n";
		foreach($unshipped as $order_id) 
			LL::log(LL::yellow, $order_id);
	}
	
	$subj = $body = 'Unshipped orders with all shipped items - updated';
	$body .= "\n\n";
	foreach($unshipped as $order_id) {
		$body .= $order_id."\n";
	}
	// just let paul know for now, no actual need to alert
	//mail('paul@victorytailgate.com', $subj, $body);
} else {
	LL::log(LL::green, "ok");
}

/**
 * check for in-stock apparel orders
 */
echo "check in-stock apparel orders...";
$params = array(171,176,177,178,179,180,181,185,186,251,257,0,1);
$in_stock_apparel = $slave->rawQuery("
SELECT oi.order_id 
FROM ordered_items oi 
INNER JOIN orders o ON o.order_id = oi.order_id 
INNER JOIN products p ON p.id = oi.product_id 
WHERE p.`product_type` IN (?,?,?,?,?,?,?,?,?,?,?)
AND p.stock>? 
AND o.date_added BETWEEN DATE_SUB(NOW(), INTERVAL ? HOUR) AND NOW()
GROUP BY oi.order_id;
",$params);
if(count($in_stock_apparel)>0)
{
	echo "\n".count($in_stock_apparel)." New in-stock apparel orders\n";
	$body = $subj = count($in_stock_apparel)." New in-stock apparel orders";
	$body .= "\n\n";
	foreach($in_stock_apparel as $order) {
		LL::log(LL::yellow, $order['order_id']);
		$body .= $order['order_id']."\n";
	}
	mail('colson@victorytailgate.com, katlynbrandofino@victorytailgate.com', $subj, $body);
} else {
	echo "none\n";
}

LL::log(LL::white, "END\n");

// just to confirm that it's running after changes
//mail('paul@victorytailgate.com', 'RC done', 'RC done');

/**
 * Send OpsGenie alert
 */
function sendAlert($body) {
    $curl = new \Curl\Curl();
    $curl->setHeader('Content-Type', 'application/json');
    $curl->setHeader("authorization", "GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf");
    $curl->post('https://api.opsgenie.com/v2/alerts', $body);
    if ($curl->error) {
        LL::log(LL::red, 'send alert error: ' . $curl->errorMessage);
    }
}
