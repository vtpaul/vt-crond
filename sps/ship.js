/**
 * Created by paul on 4/3/18.
 */
require('colors').enabled = true

const util = require('util')
const request = require('request')
const fs = require('fs-extra')
const xml2js = require('xml2js')
const async = require('async')
const moment = require('moment-timezone')
const mysql = require('mysql')
const checkdigit = require('checkdigit')
moment.tz.setDefault('America/New_York')
const sftpClient = require('ssh2-sftp-client');
const CONN = {
    host: 'sftp.spscommerce.net',
    port: '10022',
    username: 'victrytail',
    password: 'wgezqq23_zR'
}

const test = process.argv.indexOf('test') >= 0
const one = process.argv.indexOf('one') >= 0
const except = process.argv.indexOf('except') >= 0
/** interval */
let arg = process.argv.find(i => i.indexOf('interval=') >= 0)
const INTERVAL = arg ? arg.substring(9) : 1
/** specific order */
arg = process.argv.find(i => i.indexOf('order=') >= 0)
const ORDER = arg ? arg.substring(6) : null

const PROCDIR = `${__dirname}/orders/processed`
const ORDERSHIPDIR = `${__dirname}/orders/shipped`
const SHIPDIR = `${__dirname}/shipment`
const PUSHDIR = test ? `/testin` : `/in`

var connection = mysql.createConnection({
    host     : 'victory-prod-temp2-cluster.cluster-cbtmqjwywgwy.us-east-1.rds.amazonaws.com',
    user     : 'vt-backend',
    password : "gwuERd3C7wLrHCNKthpzcYme3tF9jtLfKbyVmjkU",
    database : "tgsgames_corn2",
});

const buyerSkuMap = require('./buyerSkuMap.json')

console.log(moment().format().blue)
console.log('SHIPPING'.blue)

async.waterfall([
    (cb) => {
        /** get all shipped */
        process.stdout.write(`get shipped items...`)

        let orderRange = ORDER ? `and o.order_id IN (${ORDER})` : '' /*`ot.date_added > date_sub(now(), interval ${INTERVAL} day)`*/

        var sql = `
        select
          o.order_id,
          it.ordered_item_id,
          it.quantity_shipped,
          i.quantity,
          i.product_id,
          i.product_type,
          o.site,
          o.shipping_first_name,
          o.shipping_last_name,
          o.shipping_address1,
          o.shipping_address2,
          o.shipping_city,
          o.shipping_state,
          o.shipping_zip,
          o.site_order_num,
          o.customer_po,
          ot.tracking_num,
          ot.date_added,
          lower(ot.carrier) as carrier,
          pt.title as description,
          p.weight,
          p.upc_old,
          s.shipment_json
        from order_tracking ot
          left join ordered_item_tracking it on it.track_id=ot.track_id
          inner join ordered_items i on i.item_id=it.ordered_item_id
          inner join orders o on o.order_id=ot.order_id
          left join shipments s on s.shp=ot.shp
          left join products p on p.id=i.product_id
          left join product_types pt on pt.product_type_id = i.product_type
        where
          o.status = 3
          and o.site in (38)
          and ot.tracking_status != 1
          ${orderRange}
        order by o.order_id asc;`
        //var pos = Object.keys(orders)
        var q = connection.query(sql, [], (err, rows) => {
            //console.log(rows, q.sql)
            console.log('found:', rows.length, "\n")
            if(test) console.log(rows)
            cb(err, rows)
        })
    }, (rows, cb) => {
        /** group orders */
        var orderGroup = {}
        for (var item of rows) {
            if (!(item.customer_po in orderGroup))
                orderGroup[item.customer_po] = []
            orderGroup[item.customer_po].push(item)
        }
        cb(null, orderGroup)
    }, (orders, cb) => {
        async.eachOfSeries(orders, (items, ponum, next) => {
            process.stdout.write('PO: ' + ponum + ' VTO: ' + items[0].order_id);
            async.waterfall([
                /** read from orders/processed first */
                (step) => fs.readdir(PROCDIR, {}, (err, files) => {
                    let paths = files.map(x => `${PROCDIR}/${x}`)
                    step(err, paths)
                }),
                /**
                 * read from orders/shipped second,
                 * to catch orders where more tracking was added days later
                 * and the EDI doc has already been moved to shipped folder
                 */
                (paths, step) => fs.readdir(ORDERSHIPDIR, {}, (err, files) => {
                    paths = paths.concat(files.map(x => `${ORDERSHIPDIR}/${x}`))
                    step(err, paths)
                }), (paths, step) => {
                    async.eachSeries(paths, (path, nextFile) => {
                        process.stdout.write('.')
                        async.waterfall([
                            (cb) => fs.readFile(path, 'utf-8', cb),
                            (data, cb) => xml2js.parseString(data.toString(), cb),
                            (parsed, cb) => {
                                if('Order' in parsed && parsed.Order.Header[0].OrderHeader[0].PurchaseOrderNumber[0] === items[0].customer_po) {
                                    console.log(`\nmatch found: ${path}`.yellow)
                                    parsed.path = path
                                    cb(parsed)
                                } else cb()
                            }
                        ], nextFile)
                    }, (err) => {
                        if(!err || 'errno' in err) {
                            console.log(`\nno match found`.red)
                            step('01')
                        } else {
                            step(null, err)
                        }
                    })
                }, (orderObj, step) => {
                    /**
                     * build xml shell
                     */
                    var shipID = items[0].order_id
                    var shipdate = moment(items[0].date_added).format('YYYY-MM-DD')
                    var shiptime = moment(items[0].date_added).format('HH:mm:ss')
                    var weight = items.reduce((total, i) => {
                        return total + i.weight
                    }, 0)
                    var quantity = items.reduce((total, i) => {
                        return total + (i.quantity_shipped || i.quantity)
                    }, 0)

                    var carrier = 'Unknown'
                    var CarrierTransMethodCode = 'M'
                    var CarrierAlphaCode = 'FDEG'
                    var shipment = items[0].shipment_json ? JSON.parse(items[0].shipment_json).selected_rate : false
                    if (shipment) {
                        carrier = shipment.carrier
                        // LT freight, M ground, A air, 7 "mail"
                        CarrierTransMethodCode = shipment.service.indexOf('freight') > -1 ? 'LT' : 'M'
                        /*
                         FDCC	FedEx Custom Critical
                         FDEG	FEDEX GROUND
                         FDEN	FEDEX (AIR)
                         FXFE	FedEx Freight
                         FLJF	FLT LOGISTICS LLC
                         FTNA	Fortune Transportation
                         FXFE	FedEx LTL Freight East
                         FXFW	FedEx LTL Freight West (formerly VIKN - Viking)
                         FXNL	FedEx Freight National (formerly Watkins)
                         */
                        CarrierAlphaCode = shipment.service.indexOf('freight') > -1 ? 'FXFE' : 'FDEG'
                    }

                    var QuantityAndWeight = [{
                        PackingMedium: 'CTN', // Carton
                        LadingQuantity: quantity,
                        Weight: weight,
                        WeightUOM: 'LB'
                    }]

                    var obj = {
                        Shipments: {
                            '$': {xmlns: 'http://www.spscommerce.com/RSX'},
                            Shipment: [{
                                Header: [{
                                    ShipmentHeader: [{
                                        TradingPartnerId: orderObj.Order.Header[0].OrderHeader[0].TradingPartnerId,
                                        ShipmentIdentification: [shipID],
                                        ShipDate: [shipdate],
                                        TsetPurposeCode: ['00'], // original
                                        ShipNoticeDate: [moment().format("YYYY-MM-DD"),],
                                        ShipNoticeTime: [moment().format("HH:mm:ss"),],
                                        ASNStructureCode: ['0001'],
                                        BillOfLadingNumber: [shipID],
                                        CarrierProNumber: [items[0].tracking_num], // tracking
                                    }],
                                    Dates: [{
                                        DateTimeQualifier: ['011'], // actual ship
                                        Date: [shipdate],
                                        Time: [shiptime],
                                    }],
                                    Address: [
                                        { // ship from
                                            AddressTypeCode: ['SF'],
                                            LocationCodeQualifier: 15,
                                            AddressLocationNumber: '01',
                                            AddressName: ['Landstreet'],
                                            Address1: ['2437 E. Landstreet Road'],
                                            Address2: [''],
                                            City: ['Orlando'],
                                            State: ['FL'],
                                            PostalCode: ['32824'],
                                            Country: ['USA'],
                                        }, { // ship to
                                            AddressTypeCode: ['ST'],
                                            LocationCodeQualifier: 15,
                                            AddressLocationNumber: '01',
                                            Address1: [items[0].shipping_address1],
                                            Address2: [items[0].shipping_address2],
                                            City: [items[0].shipping_city],
                                            State: [items[0].shipping_state],
                                            PostalCode: [items[0].shipping_zip],
                                            Country: ['USA'],
                                        }
                                    ],
                                    CarrierInformation: [{
                                        CarrierTransMethodCode: CarrierTransMethodCode,
                                        CarrierAlphaCode: CarrierAlphaCode,
                                        CarrierRouting: carrier,
                                        ServiceLevelCodes: [{ServiceLevelCode: 'G2'}] // G2 standard service, ND next day air, SC second day air,
                                    }],
                                    QuantityAndWeight: QuantityAndWeight,
                                }], OrderLevel: [{
                                    OrderHeader: orderObj.Order.Header[0].OrderHeader,
                                    QuantityAndWeight: QuantityAndWeight,
                                    PackLevel: [],
                                }]
                            }]
                        }
                    }

                    // for Beall's
                    if (items[0].site == 28)
                        obj.Shipments.Shipment[0].Header[0].CarrierInformation[0].StatusCode = ['CC']

                    // BBB
                    if (items[0].site == 33 || items[0].site == 10005) {
                        obj.Shipments.Shipment[0].OrderLevel[0].Address = [{
                            AddressTypeCode: 'BY',
                            LocationCodeQualifier: 15,
                            AddressLocationNumber: '01',
                        }]
                    }

                    var tracking = {}
                    for (var item of items) {
                        if (!(item.tracking_num in tracking))
                            tracking[item.tracking_num] = []
                        tracking[item.tracking_num].push(item)
                    }

                    step(null, obj, orderObj, tracking)
                }, (obj, orderObj, tracking, step) => {
                    /**
                     * add items for tracking num
                     */
                    let tracking_sent = []
                    async.eachOfSeries(tracking, (items, tracking_num, shipStep) => {
                        console.log(`${items.length} items for ${tracking_num}...`)

                        /** SSCC barcode (20 digits): [application identifier: 00] [extension digit: 0] [gs1#: 8600440001] [UPC: we create] [check digit] */
                        var sscc = checkdigit.mod10.apply(`0008600440001${tracking_num.toString().substr(-6)}`)
                        if(test) console.log('sscc:', sscc)

                        var pack = {
                            Pack: [{
                                PackLevelType: 'P',
                                ShippingSerialID: sscc,
                                CarrierPackageID: [tracking_num],
                            }],
                            References: [{
                                ReferenceQual: ['2I'], // tracking num
                                ReferenceID: [tracking_num],
                                Description: ['Tracking number'],
                            }],
                            ItemLevel: []
                        }
                        var split = null
                        async.eachSeries(items, (item, nextItem) => {
                            async.waterfall([
                                (cb) => {
                                    /**
                                     * convert 4 sets back to 8
                                     */
                                    if([81, 10092].indexOf(item.product_type) == -1) {
                                        cb()
                                    } else {
                                        process.stdout.write(`\trevert 4 bag set...`)
                                        var q = connection.query(`select p.id, p.upc_old from bundled_products b inner join products p on p.id=b.input_product_id where output_product_id = ? limit 1;`, [item.product_id], (err, rows) => {
                                            if (err || rows.length == 0) {
                                                console.log('product not found'.red)
                                                cb()
                                            } else if(split == rows[0].id) {
                                                // second bag of split, reset flag and skip insert
                                                console.log(`second bag, skip:`.white, item.product_id)
                                                split = null
                                                cb('split')
                                            } else {
                                                split = rows[0].id
                                                console.log(`converted 4 bag set:`.white, item.product_id, '=>', rows[0].id)
                                                item.product_id = rows[0].id
                                                item.upc = rows[0].upc_old
                                                // decrement order quantity
                                                obj.Shipments.Shipment[0].Header[0].QuantityAndWeight[0].LadingQuantity -= 1
                                                //console.log(obj.Shipments.Shipment[0].Header[0].QuantityAndWeight[0].LadingQuantity)
                                                cb()
                                            }
                                        })
                                    }
                                }, (cb) => {
                                    /**
                                     * get buyer part number and line sequence number from 850 doc
                                     */
                                    var LineSequenceNumber, BuyerPartNumber, ProductDescription, ConsumerPackageCode
                                    var lineItems850 = orderObj.Order.LineItem
                                    process.stdout.write(`\tmatch ${item.product_id}...`)
                                    for (item850 of lineItems850) {
                                        if (
                                            ('VendorPartNumber' in item850.OrderLine[0] && item850.OrderLine[0].VendorPartNumber[0] == item.product_id)
                                            || ('ConsumerPackageCode' in item850.OrderLine[0] && item850.OrderLine[0].ConsumerPackageCode[0] == item.upc)
                                            || ('BuyerPartNumber' in item850.OrderLine[0] && buyerSkuMap[item850.OrderLine[0].BuyerPartNumber[0].toString()] == item.product_id)
                                        ) {
                                            console.log('found'.yellow)
                                            LineSequenceNumber = item850.OrderLine[0].LineSequenceNumber[0]
                                            BuyerPartNumber = 'BuyerPartNumber' in item850.OrderLine[0] ? item850.OrderLine[0].BuyerPartNumber[0] : null
                                            ConsumerPackageCode = 'ConsumerPackageCode' in item850.OrderLine[0] ? item850.OrderLine[0].ConsumerPackageCode[0] : null
                                            ProductDescription = item850.ProductOrItemDescription[0].ProductDescription[0]
                                            break
                                        }
                                    }
                                    //console.log(LineSequenceNumber, BuyerPartNumber, ProductDescription, ConsumerPackageCode)
                                    if (!except && (!LineSequenceNumber || LineSequenceNumber == 'undefined')) {
                                        console.log('not found'.yellow)
                                    } else {
                                        pack.ItemLevel.push({
                                            ConsumerPackageCode: [ConsumerPackageCode],
                                            ShipmentLine: [{
                                                LineSequenceNumber: LineSequenceNumber,
                                                BuyerPartNumber: BuyerPartNumber,
                                                ShipQty: item.quantity_shipped,
                                                ShipQtyUOM: 'EA',
                                            }], ProductOrItemDescription: [{
                                                ProductCharacteristicCode: '08',
                                                ProductDescription: ProductDescription || 'No description',
                                            }]
                                        })
                                        tracking_sent.push(tracking_num)
                                    }

                                    cb()
                                }
                            ], (err) => {
                                if(err == 'split') nextItem() // not real error, just continue
                                else nextItem(err)
                            })
                        }, (err) => {
                            //console.log(pack.ItemLevel, pack.ItemLevel.length)
                            if(pack.ItemLevel.length > 0) {
                                obj.Shipments.Shipment[0].OrderLevel[0].PackLevel.push(pack)
                            }
                            shipStep(err)
                        })
                    }, (err) => {
                        if(!err && obj.Shipments.Shipment[0].OrderLevel[0].PackLevel.length === 0) {
                            err = `no items to ship`
                        }

                        step(err, obj, orderObj.path, [...new Set(tracking_sent.map(x => x.toString()))])
                    })
                }, (obj, docPath, tracking_sent, step) => {
                    /**
                     * create xml
                     */
                    var builder = new xml2js.Builder()
                    var xml = builder.buildObject(obj)
                    var fname = `SH_${ponum}.xml`
                    let localpath = `${SHIPDIR}/${fname}`
                    if(test) console.log(localpath.cyan)

                    fs.writeFile(localpath, xml, err => {
                        if (err) {
                            console.log('xml build error'.red)
                            cb(err)
                        } else {
                            process.stdout.write(`xml done (${fname})...`)

                            var sftp = new sftpClient()
                            let unshipped = 0

                            sftp.on('end', () => {
                                step(null, docPath, unshipped)
                            })

                            sftp.connect(CONN)
                                .catch((e) => {
                                    console.log('ftp connect error'.red, e)
                                    cb()
                                }).then(() => {
                                    sftp.put(localpath, `${PUSHDIR}/${fname}`)
                                        .catch((e) => {
                                            console.log('upload error'.red, e)
                                            cb()
                                        }).then(() => {
                                            console.log(`file uploaded`.green)
                                            let q = connection.query(`update order_tracking set tracking_status = 1 where tracking_num in ? limit ?`, [[tracking_sent], tracking_sent.length], (err, result) => {
                                                if (err) console.log('tracking update error:'.red, err, q.sql)
                                                else console.log(`tracking status updated for:`.green, JSON.stringify(tracking_sent))
                                                sftp.end()
                                            })
                                        })
                                })
                        }
                    })
                }, (docPath, unshipped, step) => {
                    // move file to shipped dir
                    if (!test && unshipped === 0 && docPath.indexOf('shipped') === -1) {
                        process.stdout.write(`move ${docPath} to shipped...`)
                        let pathParts = docPath.split('/')
                        let docName = pathParts[pathParts.length - 1]
                        fs.rename(docPath, `${ORDERSHIPDIR}/${docName}`, (err) => {
                            if (err) console.log('move error'.red, err)
                            else {
                                console.log(`moved\n`.green)
                                step()
                            }
                        })
                    } else {
                        step()
                    }
                }
            ], (err) => {
                if (err) {
                    console.log(`error: ${err}`.red)
                    if(err !== '01') sendAlert(JSON.stringify(err), next)
                    else next()
                } else if (one) next('just one')
                else {
                    next()
                }
            })
        }, (err) => {
            cb(err)
        })
    }
], (err) => {
    console.log(`done`.blue)
    if (err) {
        console.log(`error 02: ${err}`.red)
        sendAlert(JSON.stringify(err), () => {})
    }

    /** report */
    //fs.readdir(PROCDIR, {}, step)

    connection.end()
    process.exit()
})

const sendAlert = (msg, callback) => {
    //callback()
    var body = {
        "message": 'SPS ship error',
        "description": msg,
        "teams": [{"name": "Web"}],
        "tags": ["SPS"],
        "priority": "P3"
    }
    request({
        method: 'POST',
        url: 'https://api.opsgenie.com/v2/alerts',
        headers: { 'content-type': 'application/json', 'authorization': 'GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf' },
        body: JSON.stringify(body)
    }, (err) => {
        if(err) console.log(err)
        else console.log('alert sent'.blue)
        callback()
    })
}
