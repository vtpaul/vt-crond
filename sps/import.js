/**
 * Created by paul on 4/3/18.
 */

require('colors').enabled = true

const fs = require('fs-extra')
const xml2js = require('xml2js')
const async = require('async')
const mysql = require('mysql')
const request = require('request')

const moment = require('moment-timezone')
moment.tz.setDefault('America/New_York')

const sftpClient = require('ssh2-sftp-client');
const CONN = {
    host: 'sftp.spscommerce.net',
    port: '10022',
    username: 'victrytail',
    password: 'wgezqq23_zR'
}

const test = process.argv.indexOf('test') >= 0
const one = process.argv.indexOf('one') >= 0
const noack = process.argv.indexOf('noack') >= 0

const ORDERDIR = `${__dirname}/orders`
const PUSHDIR = test ? `/testin` : `/in`
const PULLDIR = `/out`

var connection = mysql.createConnection({
    host     : 'victory-prod-temp2-cluster.cluster-cbtmqjwywgwy.us-east-1.rds.amazonaws.com',
    user     : 'vt-backend',
    password : "gwuERd3C7wLrHCNKthpzcYme3tF9jtLfKbyVmjkU",
    database : "tgsgames_corn2"
})

const boardTypes = [135,12,127,149,150,146,147,25,28,29,145,148,162,27,250,1,26,152,153,24,151,10015,10012,10016,10020]
const bagTypes = [10086,10093,82,3,10092,81,79,83,139,78,85,18,74,154,155]

const buyerSkuMap = require('./buyerSkuMap.json')

console.log(moment().format().blue)
console.log('IMPORTING'.yellow)

async.waterfall([
    (cb) => {
        if(test) {
            cb()
            return
        }

        /**
         * pull down orders
         */
        let sftp = new sftpClient();
        console.log('connecting to ftp...')
        sftp.connect(CONN).then(() => {
            return sftp.list(PULLDIR);
        }).then((files) => {
            console.log(`${files.length} doc(s) to download`.yellow)
            var orders = []
            async.forEachSeries(files, (fdata, next) => {
                console.log(`read ${fdata.name}...`)
                sftp.get(`${PULLDIR}/${fdata.name}`).then((stream) => {
                    console.log(`\tsave...`)
                    stream.once('end', () => {
                        // remove from ftp
                        sftp.delete(`${PULLDIR}/${fdata.name}`).then(() => {
                            console.log(`\tfile removed`)
                            next(one ? 'one only' : false)
                        })
                    }).pipe(fs.createWriteStream(`${ORDERDIR}/${fdata.name}`))
                })
            }, (err) => {
                if(err) console.error(err.red)
                else console.log(`done\n`.blue)
                sftp.end()
                cb()
            })
        }).catch((e) => {
            console.log('ftp connect error'.red, e)
            //sendAlert(e.message, cb)
            cb()
        })
    }, (cb) => fs.readdir(ORDERDIR, {}, cb),
    (files, cb) => {
        var toProcess = files.filter(file => file.indexOf('PO') > -1)
        console.log(`${toProcess.length} doc(s) to process`.yellow)
        async.forEachSeries(toProcess, (file, next) => {
            /*if(file.indexOf('dat') > -1)*/ processOrder(file, (err) => {
                if(err) console.error(err.red)
                next(one ? 'one only' : false)
            })
            /*else next()*/
        }, cb)
    }
], (err) => {
    if(err) console.error(err.red)
    console.log(`done\n`.blue)

    /** heartbeat */
    request({
        method: 'POST',
        url: 'https://api.opsgenie.com/v2/heartbeats/SPSImport/ping',
        headers: { 'content-type': 'application/json', 'authorization': 'GenieKey c8715792-5166-480b-8983-a21976478dda' }
    }, (err) => {
        if(err) console.log(err)
        process.exit()
    })

})

const processOrder = (file, callback) => {
    console.log(`\nprocessing ${file}`.yellow)
    async.waterfall([
        (cb) => fs.readFile(`${ORDERDIR}/${file}`, 'utf-8', cb),
        (file, cb) => xml2js.parseString(file.toString(), cb),
        (orders, cb) => {
            /**
             * loop and process orders
             */
            async.forEachSeries(orders, (node, next) => {
                var order = 'Order' in node ? node['Order'][0] : node
                var ponum = order.Header[0].OrderHeader[0].PurchaseOrderNumber[0]
                console.log('PO:', ponum)
                var purpose = order.Header[0].OrderHeader[0].TsetPurposeCode[0]
                /**
                 *  00 Original
                    01 Cancellation
                    05 Replace
                    06 Confirmation
                    07 Duplicate
                 */
                // check if exists
                connection.query(`select order_id from orders where customer_po = ?`, [ponum], (err, rows) => {
                    if (!test && rows.length > 0) {
                        console.log(`${ponum} already exists as ${rows[0].order_id} (${purpose})`.magenta)
                        if(purpose == '01') { // cancel
                            console.log('Cancel code found'.magenta)
                            connection.query(`update orders set status = 4 where order_id = ? limit 1;`, [rows[0].order_id], next)
                        } else if (purpose == '05') {
                            console.log('Replace?'.magenta)
                            next()
                        } else if (purpose == '06') {
                            console.log('Confirmation?'.magenta)
                            next()
                        } else if (purpose == '07') {
                            console.log('Duplicate?'.magenta)
                            next()
                        } else next()
                    } else {
                        var data = {} // to be passed around functions
                        data.origFile = file
                        async.waterfall([
                            (procStep) => insertOrder(order, data, procStep),
                            (data, procStep) => orderAck(order, data, procStep),
                            //(data, procStep) => orderInvoice(order, data, procStep)
                        ], (err) => {
                            if (err) console.error(err.red)
                            next(err)
                        })
                    }
                })
            }, cb)
        }, (cb) => {
            /**
             * get em outta hea
             */
            if(!test) {
                fs.rename(`${ORDERDIR}/${file}`, `${ORDERDIR}/processed/${file}`, () => {
                    console.log(`\tfile moved`)
                    cb()
                })
            } else process.exit()
        }
    ], callback)
}

const insertOrder = (order, data, cb) => {
    process.stdout.write('insert order...')

    var vto // "total sales amount" for invoice
    var site = order.Header[0].OrderHeader[0].TradingPartnerId[0] == 'CDKALLVICTORYTA' ? 38 : 8888 // todo: map this?
    async.waterfall([
        (step) => {
            /**
             * TradingPartnerId:
             *  fanatics: CDKALLVICTORYTA
             */
            var q = connection.query('select ds_carrier, ds_account, ds_zip from marketplaces where site_id = ?', [site], (err, results) => {
                //console.log(q.sql, err, results); process.exit();
                step(err, results ? results[0] : {})
            })
        }, (dsinfo, step) => {
            var discount_codes = ['C310', 'C300', 'C350', 'CA00', 'I170', 'I310', 'I530']
            var shipping_charge_codes = ['G821', 'G830']
            var total = order.Summary[0].TotalAmount != undefined ? parseFloat(order.Summary[0].TotalAmount[0]) : 0
            total = Math.round(total * 100) / 100
            data.total = total
            var tax = 'Taxes' in order.Header[0] ? order.Header[0].Taxes.reduce((total, obj) => {
                return total + obj.TaxAmount[0]
            }) : 0
            tax = Math.round(tax * 100) / 100

            /**
             * shipping and discounts
             */
            var shipping = 0
            var discount = 0
            if (order.Header[0].ChargesAllowances != undefined) {
                shipping = order.Header[0].ChargesAllowances.reduce((total, obj) => {
                    return shipping_charge_codes.indexOf(obj.AllowChrgCode[0]) > -1 ? total + parseFloat(obj.AllowChrgAmt[0]) : total
                }, 0)
                discount = order.Header[0].ChargesAllowances.reduce((total, obj) => {
                    return discount_codes.indexOf(obj.AllowChrgCode[0]) > -1 ? total + parseFloat(obj.AllowChrgAmt[0]) : total
                }, 0)
            }
            shipping = Math.round(shipping * 100) / 100
            discount = Math.round(discount * 100) / 100

            // package number (ReferenceQual = IID)
            var references = {}
            if ('References' in order.Header[0]) {
                for (var ref of order.Header[0].References) {
                    references[ref.ReferenceQual[0]] = ref.ReferenceID[0]
                }
            }
            var package_num = references.IID

            var subtotal = Math.round((total - tax - shipping + discount) * 100) / 100
            data.subtotal = subtotal

            /**
             * Find contact name. Search address node for ST type, else use contact node if available
             * AddressTypeCode:
             *  BT: bill to
             *  RT: return addr
             *  VN: vendor
             *  ST: ship to
             */
            var address = {}
            for(var adr of order.Header[0].Address) {
                address[adr.AddressTypeCode] = adr
            }

            var shipping_addr = address['ST'];
            var email = 'Contacts' in shipping_addr ? shipping_addr.Contacts[0].PrimaryEmail[0] : ''
            var phone = 'Contacts' in shipping_addr && 'PrimaryPhone' in shipping_addr.Contacts[0] ? shipping_addr.Contacts[0].PrimaryPhone[0] : ''

            var giftmsg = ''
            var comments = `From EDI doc: ${data.origFile}`
            if('Notes' in order.Header[0]) {
                var comment_array = []
                var gift_array = []
                for(var note of order.Header[0].Notes) {
                    if(note.NoteCode[0] == 'GEN') comment_array.push(note.Note[0])
                    else if(note.NoteCode[0] == 'GFT') gift_array.push(note.Note[0])
                }
                comments = comment_array.join('<br>')
                giftmsg = gift_array.join('<br>')
            }

            data.sbd = null
            /*for(var date of order.Header[0].Dates) {
                if(date.DateTimeQualifier[0] == '010')
                    data.sbd = date.Date[0]
            }*/

            var order_query = `
                insert into orders (
                    name,
                    email,
                    phone,
                    subtotal,
                    tax,
                    discount,
                    shipping,
                    total,
                    date_added,
                    paid_date,
                    status,
                    shipping_full_name,
                    shipping_first_name,
                    shipping_last_name,
                    shipping_address1,
                    shipping_address2,
                    shipping_city,
                    shipping_state,
                    shipping_zip,
                    site,
                    site_order_num,
                    customer_po,
                    ship_by,
                    gift,
                    giftmsg,
                    comments,
                    pack_slip_note,
                    dropship_carrier,
                    dropship_account,
                    dropship_zip,
                    updated_by
                ) values (?)`

            var orderdata = [
                shipping_addr.AddressName[0],
                email,
                phone,
                subtotal, // subtotal
                tax,
                discount,
                shipping,
                total,
                moment().format('YYYY-MM-DD HH:mm:ss'), //order.Header[0].OrderHeader[0].PurchaseOrderDate[0],
                moment().format('YYYY-MM-DD HH:mm:ss'), //order.Header[0].OrderHeader[0].PurchaseOrderDate[0],
                7, // status paid
                shipping_addr.AddressName[0],
                shipping_addr.AddressName[0].split(' ')[0],
                shipping_addr.AddressName[0].split(' ').pop(),
                shipping_addr.Address1[0],
                shipping_addr.Address2 ? shipping_addr.Address2[0] : '',
                shipping_addr.City[0],
                shipping_addr.State[0],
                shipping_addr.PostalCode[0],
                site,
                //order.Header[0].OrderHeader[0].PurchaseOrderNumber[0],
                order.Header[0].OrderHeader[0].PurchaseOrderNumber[0], // site_order_num
                order.Header[0].OrderHeader[0].PurchaseOrderNumber[0], // customer_po
                data.sbd,
                giftmsg.length > 0 ? 1 : 0, // gift
                giftmsg,
                comments,
                package_num,
                dsinfo.ds_carrier || '',
                dsinfo.ds_account || '',
                dsinfo.ds_zip || '',
                546 // crond
            ]
            /* fedex, 648225768, 32256 */
            if(test) { // <--- FOR TESTING
                console.log(orderdata)
                step(null, {insertId:987654}, {})
            } else connection.query(order_query, [orderdata], step)

        }, (result, fields, step) => {
            vto = result.insertId
            data.vto = vto
            console.log(`${vto}`.green)

            var ordertotal = 0
            var subtotal = 0
            var bagsInOrder = false

            /**
             * ORDERED ITEMS
             */
            let gid = 0
            async.eachSeries(order.LineItem, (item, nextitem) => {
                let buyerPartNum = item.OrderLine[0].BuyerPartNumber[0]
                let sku = buyerSkuMap[buyerPartNum.toString()] || null
                if (!sku) sku = 'VendorPartNumber' in item.OrderLine[0] ? item.OrderLine[0].VendorPartNumber[0] : null
                var upc = (sku == '' && 'ConsumerPackageCode' in item.OrderLine[0]) ? item.OrderLine[0].ConsumerPackageCode[0] : null

                var unitprice = parseFloat(item.OrderLine[0].PurchasePrice[0]);
                var qty = parseInt(item.OrderLine[0].OrderQty[0])
                var discount = 'ChargesAllowances' in item ? item.ChargesAllowances.reduce((total, obj) => {
                    return discount_codes.indexOf(obj.AllowChrgCode[0]) > -1 ? total + parseFloat(obj.AllowChrgAmt[0]) : total
                }, 0) : 0;
                var tax = 'Taxes' in item ? item.Taxes.reduce((total, obj) => {
                    return total + obj.TaxAmount[0]
                }) : 0;

                subtotal += unitprice * qty
                ordertotal +=  unitprice * qty - tax - discount
                //console.log('st ', subtotal, 'ot ', ordertotal)
                let bagSplit = 1
                async.waterfall([
                    (itemstep) => {
                        /**
                         * SPLIT 8 BAGS INTO 4
                         */
                        connection.query(`select id, product_type from products where id = ? or upc_old = ? limit 1;`, [sku, upc], (err, result) => {
                            //console.log(q.sql)
                            if(err || result.length==0) {
                                itemstep(`item not found on ${order.Header[0].OrderHeader[0].PurchaseOrderNumber[0]} / ${data.origFile}: ${sku} / ${upc}`)
                            } else {
                                var skus = [result[0].id]
                                if (result.length > 0 && [78, 79, 83].indexOf(result[0].product_type) > -1) {
                                    // find 4 bags
                                    var q = connection.query(`select b.output_product_id from bundled_products b inner join products p on p.id=b.output_product_id where input_product_id = ? and p.active='y'`, [result[0].id], (err, rows) => {
                                        //console.log(q.sql, err, rows)
                                        if (err || rows.length < 2) itemstep(err, skus)
                                        else {
                                            bagSplit = 2
                                            skus = rows.map(x => x.output_product_id)
                                            console.log('splitting 8 bag set:', sku, '=>', JSON.stringify(skus))
                                            itemstep(err, skus)
                                        }
                                    })
                                } else itemstep(err, skus)
                            }
                        })
                    }, (skus, itemstep) => {
                        /**
                         * get slots and ptype
                         */
                        var prod_query = `
                            select 
                                p.id, 
                                p.name, 
                                p.product_type,
                                p.team_id,
                                group_concat(concat_ws('/', s.slot_id, s.slot_num), ',') as slots
                            from products p 
                                left join products_slots s on s.product_id = p.id and s.quantity > 0
                            where p.id IN (?)
                            group by p.id`
                        var q = connection.query(prod_query, [skus], (err, rows) => {
                            if(test) console.log(rows)
                            itemstep(err, rows)
                        })
                    }, (prods, itemstep) => {
                        /**
                         * build query
                         */
                        async.eachSeries(prods, (prod, nextProd) => {
                            //console.log('insert', prod.name)
                            var pid = prod.id || 0
                            var pname = prod.name
                            var ptype = prod.product_type
                            if(!bagsInOrder && bagTypes.indexOf(ptype) > -1) {
                                //console.log('bags found'.blue)
                                bagsInOrder = true
                            }
                            var slot_data = (prod.slots) ? prod.slots.split(',') : []

                            var info = ''
                            // prod specific notes:
                            /*if(boardTypes.indexOf(ptype) > -1)
                                info += 'Bags: 8 corn-filled best matching colors<br>Plus any additional bags listed with this order.'
                            if(ptype == 10020)
                                info += 'BAGGO Bags: 8 corn-filled best matching colors<br>Plus any additional bags listed with this order.'*/

                            if('Notes' in order.Header[0]) {
                                var info_array = []
                                for(var note of order.Header[0].Notes) {
                                    if(note.NoteCode[0] == 'GEN') info_array.push(note.Note[0])
                                }
                                info += info_array.join('<br>')
                            }

                            /**
                             * SPLIT QUANTITY
                             */
                            var loop = [...Array(qty).keys()]
                            async.eachSeries(loop, (value, nextqty) => {
                                process.stdout.write(`inserting ${pid}...`)

                                let board = boardTypes.indexOf(ptype) > -1
                                let boardSplit = board && [10020, 73].indexOf(ptype) === -1 ? 2 : 1;

                                //var slot_id = slot[0]
                                var slot = slot_data.shift() || []
                                var slot_num = slot[1] || ''

                                var item_query = `insert into ordered_items (order_id, product_id, name, style, price, date_added, updated_date, quantity, slot_nums, category, refund_amt, item_status, cwg_customer_price, updated_by, product_type, info, tax, discount, group_id) values ?`

                                let item_vals = []
                                for (let j = 0; j < boardSplit; j++) {
                                    let thisInfo = ''
                                    if (boardSplit > 1) {
                                        thisInfo = (j % 2 === 0) ? 'ONE BOARD PRINT A' : 'ONE BOARD PRINT B'
                                        console.log(`board ${j % 2 + 1}`)
                                    }
                                    let split = bagSplit * boardSplit

                                    item_vals.push([
                                        vto,
                                        pid,
                                        pname, //item.ProductOrItemDescription[0].ProductDescription[0],
                                        '', // style
                                        unitprice / split,
                                        moment().format('YYYY-MM-DD HH:mm:ss'),
                                        moment().format('YYYY-MM-DD HH:mm:ss'),
                                        1, // qty
                                        slot_num,
                                        0, // todo: category needed?
                                        0, //item.amount_refunded / qty, // refund
                                        0, // status
                                        0, // cwg
                                        546, // updated by
                                        ptype, // ptype
                                        thisInfo + info,
                                        tax / split,
                                        discount / split,
                                        gid
                                    ])
                                }

                                async.waterfall([
                                    (step) => {
                                        /**
                                         * ADDONS
                                         */
                                        let addons = []

                                        /**
                                         * TOWER CC
                                         */
                                        if([10091, 266].indexOf(ptype) > -1) {
                                            if (prod.team_id > 0) {
                                                // check for team bag
                                                connection.query(`select id, name, product_type from products where team_id = ? and product_type = 269 and active = 'y'`, [prod.team_id], (err, result) => {
                                                    if(err)
                                                        console.log(err)
                                                    if(result)
                                                        addons.push(result[0])
                                                    step(null, addons)
                                                })
                                            } else {
                                                addons.push({id:224353, name:'Team Tumble Tower Carrying Case', product_type: 269})
                                                step(null, addons)
                                            }

                                        /**
                                         * GAMEDAY TOWER CC
                                         */
                                        } else if ([10014].indexOf(ptype) > -1) {
                                            addons.push({id: 1222675, name: 'Black Gameday Tower Carrying Case', product_type: 10055})
                                            step(null, addons)

                                        /**
                                         * BAGS FOR BOARD
                                         */
                                        } else if (!bagsInOrder && boardTypes.indexOf(ptype) > -1) {
                                            var q = connection.query(`select p.id, p.name, p.product_type from selected_products_addons a inner join products p on p.id=a.selected_addon_id where a.marketplace_id = 38 and p.product_type = 82 and a.product_id = ?`, [pid], (err, result) => {
                                                if(err)
                                                    console.log(err)
                                                if(result.length > 0) {
                                                    for(prod of result) {
                                                        prod.info = 'Bundled item'
                                                        addons.push(prod)
                                                    }
                                                } else {
                                                    info += 'Bags: 8 corn-filled best matching colors<br>Plus any additional bags listed with this order.'
                                                }

                                                step(null, addons)
                                            })
                                        } else step(null, addons)
                                    }, (addons, step) => {
                                        for(addon of addons) {
                                            console.log(`addon: ${addon.name}`)
                                            item_vals.push([
                                                vto,
                                                addon.id,
                                                addon.name,
                                                '', // style
                                                0,
                                                moment().format('YYYY-MM-DD HH:mm:ss'),
                                                moment().format('YYYY-MM-DD HH:mm:ss'),
                                                1, // qty
                                                '',
                                                0, // todo: category needed?
                                                0, //item.amount_refunded / qty, // refund
                                                0, // status
                                                0, // cwg
                                                546, // updated by
                                                addon.product_type, // ptype
                                                addon.info || '',
                                                0,
                                                0,
                                                gid
                                            ])
                                        }

                                        if(test) {
                                            result.insertId = 1234;
                                            console.log(`${result.insertId}`.green)
                                            step()
                                        } else {
                                            connection.query(item_query, [item_vals], (err, result) => {
                                                if (err) console.log(`tried ${q.sql}`)
                                                else if (result.insertId > 0) {
                                                    console.log(`${result.insertId}`.green)
                                                }
                                                step(err)
                                            })
                                        }
                                    }
                                ], nextqty)
                            }, nextProd)
                        }, itemstep)
                    }
                ], nextitem)
            }, (err) => {
                if(err) {
                    console.log(err.red)
                    sendAlert(err, step)
                } else {
                    data.total = ordertotal
                    data.subtotal = subtotal
                    if (test) step()
                    else {
                        connection.query(`update orders set total = ?, subtotal = ?, updated_by = 546 where order_id = ? limit 1`, [ordertotal, subtotal, vto], (err) => {
                            if (err) console.log(err)
                            else console.log('order total updated'.yellow)

                            step()
                        })
                    }
                }
            })
        }
    ], (err) => {
        cb(err, data)
    })
}

const orderAck = (order, data, cb) => {
    if(noack) {
        cb()
        return
    }

    process.stdout.write('gen acknowledgement...')

    var orderdate = order.Header[0].OrderHeader[0].PurchaseOrderDate

    // transform original order object to create ack object
    var copy = JSON.parse(JSON.stringify(order))

    //console.log(order.Header[0].QuantityTotals)

    delete copy.Header[0].OrderHeader[0].PrimaryPOTypeCode
    delete copy.Header[0].OrderHeader[0].Division
    delete copy.Header[0].PaymentTerms
    delete copy.Header[0].Contacts
    delete copy.Header[0].Address[0]
    delete copy.Header[0].FOBRelatedInstruction
    delete copy.Header[0].CarrierInformation
    delete copy.Header[0].Notes
    delete copy.Header[0].ChargesAllowances
    delete copy.Header[0].QuantityTotals

    //console.log(order.Header[0].QuantityTotals)

    copy.Header[0].OrderHeader[0].AcknowledgementType = 'AK'
    copy.Header[0].OrderHeader[0].AcknowledgementDate = moment().format("YYYY-MM-DD")

    for(var item of copy.LineItem) {
        delete item.PhysicalDetails
        delete item.OrderLine[0].ProductID
        delete item.OrderLine[0].NRFStandardColorAndSize
        delete item.OrderLine[0].ConsumerPackageCode

        item.LineItemAcknowledgement = {
            ItemStatusCode: 'IA',
            ItemScheduleQty: item.OrderLine[0].OrderQty,
            ItemScheduleUOM: item.OrderLine[0].ItemScheduleUOM,
            ItemScheduleQualifier: 'EA',
            ItemScheduleDate: orderdate
        }
    }

    var obj = {
        OrderAcks: {
            '$': {xmlns: 'http://www.spscommerce.com/RSX'},
            OrderAck: copy
        }
    }

    var builder = new xml2js.Builder()
    var xml = builder.buildObject(obj)
    let fname = `PR_${copy.Header[0].OrderHeader[0].PurchaseOrderNumber}.xml`;
    let localpath = `${__dirname}/ack/${fname}`
    fs.writeFile(localpath, xml, err => {
        if(err) cb(err.red)
        else {
            console.log('xml created'.green)
            if(test) cb(err, data)
            else {
                var sftp = new sftpClient()
                sftp.on('end', () => {
                    cb(err, data)
                })
                sftp.connect(CONN).then(() => {
                    sftp.put(localpath, `${PUSHDIR}/${fname}`).then(() => {
                        console.log(`\tfile uploaded`)
                        sftp.end()
                    }).catch((e) => {
                        console.log('upload error'.red)
                        sendAlert(e.message, cb)
                    })
                }).catch((e) => {
                    console.log('ftp connect error'.red)
                    sendAlert(e.message, cb)
                })
            }
        }
    })
}

const sendAlert = (msg, callback) => {
    var body = {
        "message": 'SPS error',
        "description": msg,
        "teams": [{"name": "Web"}],
        "tags": ["SPS"],
        "priority": "P3"
    }
    request({
        method: 'POST',
        url: 'https://api.opsgenie.com/v2/alerts',
        headers: { 'content-type': 'application/json', 'authorization': 'GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf' },
        body: JSON.stringify(body)
    }, (err) => {
        if(err) console.log(err)
        else console.log('alert sent'.blue)
        callback()
    })
}