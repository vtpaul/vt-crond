/**
 * Created by paul on 4/3/18.
 */

require('colors').enabled = true

const request = require('request')
const fs = require('fs-extra')
const xml2js = require('xml2js')
const async = require('async')
const moment = require('moment-timezone')
const mysql = require('mysql')
const checkdigit = require('checkdigit')
moment.tz.setDefault('America/New_York')
const sftpClient = require('ssh2-sftp-client');
const CONN = {
    host: 'sftp.spscommerce.net',
    port: '10022',
    username: 'victrytail',
    password: 'wgezqq23_zR'
}

const test = process.argv.indexOf('test') >= 0
const one = process.argv.indexOf('one') >= 0
const except = process.argv.indexOf('except') >= 0

//const PROCDIR = `${__dirname}/orders/processed`
const INVDIR = `${__dirname}/invoice`
const ORDERINVDIR = `${__dirname}/orders/invoiced`
const ORDERSHIPDIR = `${__dirname}/orders/shipped`
const PUSHDIR = test ? `/testin` : `/in`

var connection = mysql.createConnection({
    host     : 'victory-prod-temp2-cluster.cluster-cbtmqjwywgwy.us-east-1.rds.amazonaws.com',
    user     : 'vt-backend',
    password : "gwuERd3C7wLrHCNKthpzcYme3tF9jtLfKbyVmjkU",
    database : "tgsgames_corn2",
});

const buyerSkuMap = {
    '0008571084': 810402,
    '0008571110': 1070450,
    '0008933233': 1070450,
    '0008933238': 1070843,
    '0008571091': 25152,
    '0008571106': 26027,
    '0008571108': 25190,
    '0008571107': 25217,
    '0008571103': 25168,
    '0008571109': 25207,
    '0008571105': 25198,
    '0008571088': 25128,
    '0008571112': 25235,
    '0008571086': 25124,
    '0008571089': 25130,
    '0009534918': 806834,
    '0008571111': 25215,
    '0008571104': 25176,
    '0011897867': 9511310,
    '0011897869': 9511322,
    '0009534811': 807642,
}

console.log(moment().format().blue)
console.log('INVOICING'.blue)

let numSent = 0
async.waterfall([
    (cb) => {
        // check for pass po
        let passed = process.argv.find(i => i.indexOf('po=') >= 0)
        if(passed) {
            let files = passed.substring(3).split(',')
            cb(null, files)
        } else {
            fs.readdir(ORDERSHIPDIR, {}, (err, files) => {
                if (err) console.log('readdir error'.red)
                cb(err, files)
            })
        }
    }, (files, cb) => {
        console.log(`${files.length} to check`.yellow)
        let orderObj, origFname
        async.forEachSeries(files, (file, next) => {
            // make obj {po:filename}
            async.waterfall([
                (cb) => fs.readFile(`${ORDERSHIPDIR}/${file}`, 'utf-8', (err, files) => {
                    cb(err, files)
                }),
                (file, cb) => xml2js.parseString(file.toString(), (err, data) => {
                    if(err) console.error(`parseString error`.red)
                    cb(err, data)
                }),
                (data, cb) => {
                    orderObj = data.Order
                    var po = orderObj.Header[0].OrderHeader[0].PurchaseOrderNumber[0]
                    var orders = {}
                    origFname = file
                    orders[po] = file
                    console.log(orders)
                    cb(null, orders)
                }, (orders, cb) => {
                    /**
                     * get items to send
                     */
                    process.stdout.write(`check order...`)
                    var sql = `
                    select
                        o.order_id, o.invoice_number, o.total, o.subtotal,
                        max(t.date_added) as ship_date
                    from orders o
                        left join order_tracking t on t.order_id=o.order_id
                    where 
                        o.invoice_number is not null and o.invoice_number != ''
                        and o.site in (38)
                        and o.customer_po = ?
                    group by o.order_id 
                    order by o.order_id asc;`
                    var pos = Object.keys(orders)
                    var q = connection.query(sql, [pos], (err, rows) => {
                        //console.log(q.sql); process.exit();
                        if(test) console.log(rows.map((v) => v.invoice_number))
                        cb(err, rows, orders)
                    })
                }, (rows, orders, cb) => {
                    /**
                     * make xml and send
                     */
                    console.log(rows.length == 0 ? 'no invoice'.white : `inv#: ${rows[0].invoice_number}-${rows[0].order_id}`)
                    async.eachSeries(rows, (order, nextOrder) => {
                        let fname
                        async.waterfall([
                            (step) => {
                                process.stdout.write('build invoice...'.yellow)
                                //console.log(orderObj)

                                var ponum = orderObj.Header[0].OrderHeader[0].PurchaseOrderNumber
                                fname = `IN_${ponum}.xml`
                                var copy = JSON.parse(JSON.stringify(orderObj))

                                // remove stuff we dont need from order header
                                delete copy.Header[0].Contacts
                                delete copy.Header[0].Address[0]
                                delete copy.Header[0].FOBRelatedInstruction
                                delete copy.Header[0].Notes
                                delete copy.Header[0].ChargesAllowances

                                delete copy.Header[0].OrderHeader[0].PrimaryPOTypeCode
                                delete copy.Header[0].OrderHeader[0].ShipCompleteCode
                                delete copy.Header[0].OrderHeader[0].Division
                                delete copy.Header[0].OrderHeader[0].CustomerOrderNumber

                                //copy.Header[0].PaymentTerms = [{ TermsDescription: 'Net30' }] // todo

                                // use order header to make invoice header then delete
                                copy.Header[0].InvoiceHeader = [
                                    Object.assign({
                                        InvoiceNumber: `${order.invoice_number}-${order.order_id}`, // vto??
                                        InvoiceDate: moment().format("YYYY-MM-DD"),
                                        InvoiceTypeCode: 'DR', // debit
                                        BuyersCurrency: 'USD',
                                        SellersCurrency: 'USD',
                                        //CustomerOrderNumber: , // fanatics' order number
                                        //BillOfLadingNumber: [],
                                        //CarrierProNumber: [],
                                        ShipDate: moment(order.ship_date).format("YYYY-MM-DD"),
                                    }, copy.Header[0].OrderHeader[0])
                                ]
                                delete copy.Header[0].OrderHeader

                                // rename node orderline to invoiceline
                                async.forEachSeries(copy.LineItem, (item, nextItem) => {
                                    item.InvoiceLine = [Object.assign({
                                        ShipQty: item.OrderLine[0].OrderQty
                                    }, item.OrderLine[0])]
                                    delete item.OrderLine
                                    nextItem()
                                }, (err) => {
                                    if(err) console.log(err)

                                    // Total merchandise amount. Sum of price * quantity
                                    copy.Summary[0].TotalSalesAmount = orderObj.Summary[0].TotalSalesAmount != undefined ? order.Summary[0].TotalSalesAmount[0] : order.subtotal
                                    copy.Summary[0].TotalSalesAmount = copy.Summary[0].TotalSalesAmount.toFixed(2)

                                    // Total amount of the transaction. Sum of the item qty * price +/­ the charges, allowances, and taxes, if sent.
                                    copy.Summary[0].TotalAmount = orderObj.Summary[0].TotalAmount != undefined ? order.Summary[0].TotalAmount[0] : order.total
                                    copy.Summary[0].TotalAmount = copy.Summary[0].TotalAmount.toFixed(2)

                                    var obj = {
                                        Invoices: {
                                            '$': {xmlns: 'http://www.spscommerce.com/RSX'},
                                            Invoice: copy
                                        }
                                    }

                                    /**
                                     * create xml
                                     */
                                    var builder = new xml2js.Builder()
                                    var xml = builder.buildObject(obj)
                                    let localpath = `${INVDIR}/${fname}`
                                    if (test) console.log(localpath.cyan)
                                    fs.writeFile(localpath, xml, err => {
                                        if (err) {
                                            console.log('xml build error'.red)
                                            step(err)
                                        } else {
                                            var sftp = new sftpClient()
                                            sftp.on('end', () => {
                                                step(err)
                                            })
                                            sftp.connect(CONN).then(() => {
                                                sftp.put(localpath, `${PUSHDIR}/${fname}`).then(() => {
                                                    console.log(`file uploaded:`.green, fname)
                                                    ++numSent
                                                    sftp.end()
                                                }).catch((e) => {
                                                    console.log('upload error'.red)
                                                    step()
                                                    //sendAlert(e.message, cb)
                                                })
                                            }).catch((e) => {
                                                console.log('ftp connect error'.red)
                                                step()
                                                //sendAlert(e.message, cb)
                                            })
                                        }
                                    })
                                })
                            }, (step) => {
                                // move file to shipped dir
                                process.stdout.write(`move local file...`)
                                fs.rename(`${ORDERSHIPDIR}/${origFname}`, `${ORDERINVDIR}/${origFname}`, (err) => {
                                    if(err) console.log('move error'.red, err.message)
                                    else console.log('ok'.green)
                                    step()
                                })
                            }
                        ], (err) => {
                            if (err) {
                                console.log(`error 01: ${err}`.red)
                                if(!test) sendAlert(JSON.stringify(err), nextOrder)
                                else nextOrder()
                            } else if (one) {
                                console.log('stopping at one'.magenta)
                                process.exit()
                                //nextOrder('just one')
                            }
                            else nextOrder()
                        })
                    }, (err) => {
                        cb(err)
                    })
                }
            ], (err) => {
                next(err)
            })
        }, (err) => {
            cb(err)
        })
    }
], (err) => {
    connection.end()
    if (err) {
        console.log(`error 02: ${err}`.red)
        sendAlert(JSON.stringify(err), () => {})
    }
    console.log(`done, sent:`.green, numSent)
    process.exit()
})

const sendAlert = (msg, callback) => {
    var body = {
        "message": 'SPS invoice error',
        "description": msg,
        "teams": [{"name": "Web"}],
        "tags": ["SPS"],
        "priority": "P3"
    }
    request({
        method: 'POST',
        url: 'https://api.opsgenie.com/v2/alerts',
        headers: { 'content-type': 'application/json', 'authorization': 'GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf' },
        body: JSON.stringify(body)
    }, (err) => {
        if(err) console.log(err)
        else console.log('alert sent'.blue)
        callback()
    })
}