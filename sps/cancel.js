/**
 * Created by paul on 4/3/18.
 */

require('colors').enabled = true

const fs = require('fs-extra')
const xml2js = require('xml2js')
const async = require('async')
const mysql = require('mysql')
const request = require('request')

const moment = require('moment-timezone')
moment.tz.setDefault('America/New_York')

const sftpClient = require('ssh2-sftp-client');
const CONN = {
    host: 'sftp.spscommerce.net',
    port: '10022',
    username: 'victrytail',
    password: 'wgezqq23_zR'
}

const test = process.argv.indexOf('test') >= 0
const one = process.argv.indexOf('one') >= 0
const noack = process.argv.indexOf('noack') >= 0

const ORDERDIR = `${__dirname}/orders`
const PUSHDIR = test ? `/testin` : `/in`

var connection = mysql.createConnection({
    host     : 'victory-prod-temp2-cluster.cluster-cbtmqjwywgwy.us-east-1.rds.amazonaws.com',
    user     : 'vt-backend',
    password : "gwuERd3C7wLrHCNKthpzcYme3tF9jtLfKbyVmjkU",
    database : "tgsgames_corn2"
})

console.log(moment().format().blue)
console.log('CHECK FOR CANCELED'.blue)

async.waterfall([
    (cb) => fs.readdir(`${__dirname}/orders/processed`, {}, cb),
    (files, cb) => {
        var toProcess = files.filter(file => file.indexOf('PO') > -1)
        console.log(`${toProcess.length} to check`.yellow)
        async.forEachSeries(toProcess, (file, next) => {
            processOrder(file, (err) => {
                if(err) console.error(err.red)
                next(one ? 'one only' : false)
            })
        }, cb)
    }
], (err) => {
    if(err) console.error(err.red)
    console.log(`done\n`.green)
    process.exit()
})

const processOrder = (file, callback) => {
    console.log(`checking ${file}`.yellow)
    let move = false
    async.waterfall([
        (cb) => fs.readFile(`${ORDERDIR}/processed/${file}`, 'utf-8', cb),
        (file, cb) => xml2js.parseString(file.toString(), cb),
        (orders, cb) => {
            /**
             * loop and process orders
             */
            async.forEachSeries(orders, (node, next) => {
                var order = 'Order' in node ? node['Order'][0] : node
                var ponum = order.Header[0].OrderHeader[0].PurchaseOrderNumber[0]
                console.log(ponum)
                connection.query(`select order_id, status from orders where customer_po = ? and status = ?`, [ponum, 4], (err, rows) => {
                    if (rows.length > 0) {
                        console.log(`CANCEL ${ponum} / ${rows[0].order_id}`.magenta)
                        var data = {}
                        data.origFile = file
                        data.vto = rows[0]
                        async.waterfall([
                            (procStep) => orderAck(order, data, procStep),
                        ], (err) => {
                            if (err) console.error(err.red)
                            else {
                                move = true
                                next(err)
                            }
                        })
                    } else {
                        next()
                    }
                })
            }, cb)
        }, (cb) => {
            /**
             * get em outta hea
             */
            if(!test && move===true) {
                fs.rename(`${ORDERDIR}/processed/${file}`, `${ORDERDIR}/canceled/${file}`, () => {
                    console.log(`\tfile moved`)
                    cb()
                })
            } else cb()
        }
    ], callback)
}

const orderAck = (order, data, cb) => {
    process.stdout.write('gen acknowledgement...')

    var orderdate = order.Header[0].OrderHeader[0].PurchaseOrderDate

    // transform original order object to create ack object
    var copy = JSON.parse(JSON.stringify(order))

    //console.log(order.Header[0].QuantityTotals)

    delete copy.Header[0].OrderHeader[0].PrimaryPOTypeCode
    delete copy.Header[0].OrderHeader[0].Division
    delete copy.Header[0].PaymentTerms
    delete copy.Header[0].Contacts
    delete copy.Header[0].Address[0]
    delete copy.Header[0].FOBRelatedInstruction
    delete copy.Header[0].CarrierInformation
    delete copy.Header[0].Notes
    delete copy.Header[0].ChargesAllowances
    delete copy.Header[0].QuantityTotals

    //console.log(order.Header[0].QuantityTotals)

    copy.Header[0].OrderHeader[0].AcknowledgementType = 'RD'
    copy.Header[0].OrderHeader[0].AcknowledgementDate = moment().format("YYYY-MM-DD")

    for(var item of copy.LineItem) {
        delete item.PhysicalDetails
        delete item.OrderLine[0].ProductID
        delete item.OrderLine[0].NRFStandardColorAndSize
        delete item.OrderLine[0].ConsumerPackageCode

        item.LineItemAcknowledgement = {
            ItemStatusCode: 'R2',
            ItemScheduleQty: item.OrderLine[0].OrderQty,
            ItemScheduleUOM: item.OrderLine[0].ItemScheduleUOM,
            ItemScheduleQualifier: 'EA',
            ItemScheduleDate: orderdate
        }
    }

    var obj = {
        OrderAcks: {
            '$': {xmlns: 'http://www.spscommerce.com/RSX'},
            OrderAck: copy
        }
    }

    var builder = new xml2js.Builder()
    var xml = builder.buildObject(obj)
    let fname = `PR_${copy.Header[0].OrderHeader[0].PurchaseOrderNumber}.xml`;
    let localpath = `${__dirname}/ack/${fname}`
    fs.writeFile(localpath, xml, err => {
        if(err) cb(err.red)
        else {
            console.log(`xml created: ${fname}`.green)
            if(test) {
                cb(err, data)
            } else {
                var sftp = new sftpClient()
                sftp.on('end', () => {
                    cb(err, data)
                })
                sftp.connect(CONN).then(() => {
                    sftp.put(localpath, `${PUSHDIR}/${fname}`).then(() => {
                        console.log(`\tfile uploaded`)
                        sftp.end()
                    }).catch((e) => {
                        console.log('upload error'.red)
                        sendAlert(e.message, cb)
                    })
                }).catch((e) => {
                    console.log('ftp connect error'.red)
                    sendAlert(e.message, cb)
                })
            }
        }
    })
}

const sendAlert = (msg, callback) => {
    var body = {
        "message": 'SPS error',
        "description": msg,
        "teams": [{"name": "Web"}],
        "tags": ["SPS"],
        "priority": "P3"
    }
    request({
        method: 'POST',
        url: 'https://api.opsgenie.com/v2/alerts',
        headers: { 'content-type': 'application/json', 'authorization': 'GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf' },
        body: JSON.stringify(body)
    }, (err) => {
        if(err) console.log(err)
        else console.log('alert sent'.blue)
        callback()
    })
}