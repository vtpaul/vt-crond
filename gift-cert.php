<?php
require_once(__DIR__.'/../shared-resources/_configs/configs.inc');
require_once(__DIR__.'/../shared-resources/lurdlogger.php');

// get pending certs
$slave->join('orders o', 'o.order_id=i.order_id', 'INNER');
$slave->where('i.product_id', array(4610,4611,4612,4613,3438922,3438923), 'IN');
$slave->where('i.item_status', 0);
$certs = $slave->get('ordered_items i', null, array('i.order_id, i.item_id, i.product_id, i.info, o.email, o.name'));

$code_bank = strtoupper('abcdefghijklmnopqrstuvwxyz0123456789');
$code_length = 8;

if(count($certs)>0) {
    LL::log(LL::light_blue, "\n".date("Y-m-d H:i:s"));

    foreach($certs as $cert) {
        LL::log(LL::white, json_encode($cert));

        /**
         * generate code
         */
        $code = '';
        $max = strlen($code_bank) - 1;
        for ($i = 0; $i < $code_length; $i++) {
            $code .= $code_bank[mt_rand(0, $max)];
        }

        /**
         * generate image
         */
        // get gc amount
        $gcamnt = 0;
        switch ($cert['product_id']) {
            case 4610:
                $gcamnt = 50;
                break;
            case 4611:
                $gcamnt = 100;
                break;
            case 4612:
                $gcamnt = 150;
                break;
            case 4613:
                $gcamnt = 200;
                break;
            case 3438922:
                $gcamnt = 125;
                break;
            case 3438923:
                $gcamnt = 250;
                break;
        }

        if (empty($cert['info'])) {
            LL::log(LL::red, 'No info');
            // opsgenie
            $body = '{
                "message": "Gift cert error",
                "description": "Gift cert no info",
                "teams": [{"name": "Web"}],
                "tags": ["Gift Certificate", "Crond"],
                "priority": "P3"
            }';
            sendAlert($body);
            continue;
        }

        // parse text
        $search1 = 'Gift Recipient: ';
        $start1 = strpos($cert['info'], $search1) + strlen($search1);
        $end1 = strpos($cert['info'], '<br>', $start1);
        $recipient = substr($cert['info'], $start1, $end1 - $start1);
        $recipient = str_replace("&quot;", '"', $recipient);
        $recipient = str_replace("&amp;", '&', $recipient);

        $search2 = 'Gift Message: ';
        $start2 = strpos($cert['info'], $search2, $end1) + strlen($search2);
        $end2 = strpos($cert['info'], '<br>', $start2);
        if(!$end2) $end2 = strlen($cert['info']);
        $message = str_replace("<br />\r\n", ' ', substr($cert['info'], $start2, $end2 - $start2)); //echo $message; continue;
        $message = str_replace("&quot;", '"', $message);
        $message = str_replace("&amp;", '&', $message);
        //LL::log(LL::white, $message);

        $image = new Imagick();
        if (!$handle = fopen(__DIR__ . "/images/gcblank.jpg", 'rb')) {
            LL::log(LL::red, 'file not found');
            continue;
        }
        $image->readImageFile($handle);

        $draw = new ImagickDraw();
        //$draw->setFont('/usr/share/fonts/truetype/dejavu/DejaVuSerif.ttf');
        $draw->setFont('/usr/share/fonts/truetype/dejavu/DejaVuSerif-Bold.ttf');
        $draw->setTextAlignment(\Imagick::ALIGN_CENTER);

        // code
        $draw->setFontSize(40);
        $draw->setFillColor('white');
        $image->annotateImage($draw, 635, 1522, 0, "Code: $code");

        // message
        $draw->setFillColor('gray');
        if(strlen($message) > 100) {
            $draw->setFontSize(16);
            $message = wordwrap($message);
            $image->annotateImage($draw, 635, 1070, 0, $message);
        } else {
            $draw->setFontSize(20);
            $image->annotateImage($draw, 635, 1080, 0, $message);
        }

        // sender / recipient
        //$fontsize = strlen($sender) > 20 ? 30 : 40;
        $draw->setFontSize(34);
        $draw->setFillColor('#f4bb0d');
        $image->annotateImage($draw, 400, 1165, 0, $recipient);
        $sender = explode(' ', $cert['name'])[0];
        $image->annotateImage($draw, 880, 1165, 0, $sender);

        // amount
        $draw->setFontSize(80);
        $image->annotateImage($draw, 635, 1026, 0, "$$gcamnt");

        $image->setImageFormat('pdf');
        $gcfile = __DIR__ . "/images/giftcerts/$code.pdf";
        $image->setImageFilename($gcfile);
        if (!$image->writeImage()) {
            LL::log(LL::red, 'Image create error');
            // opsgenie
            $body = '{
                "message": "Gift cert error",
                "description": "Image create error",
                "teams": [{"name": "Web"}],
                "tags": ["Gift Certificate", "Crond"],
                "priority": "P3"
            }';
            sendAlert($body);
            continue;
        } else {
            LL::log(LL::yellow, "File created: $gcfile");
        }

        //continue;

        // insert promo code
        if (!$master->insert('promos', array(
            'code' => $code,
            'amount_off' => $gcamnt,
            'free_ship' => 'n',
            'date_added' => $master->now(),
            'gift_card' => 1,
            'uses' => 0,
            'balance' => $gcamnt
        ))) {
            LL::log(LL::red, 'Promo create error');
        }

        $body = "<p>Thank you for choosing Victory Tailgate!</p>
        <p>We have attached your gift certificate within this email. To redeem your gift, <b>enter the gift code in the promo code field on our website</b>, either on the cart or at the checkout screen. If you experience any problems, please contact our office at 407-704-8775, or send an email to info@victorytailgate.com, and we can process your order manually with the discount.
        <p>Please let us know if you have any questions or concerns.</p>";

        $mail = new PHPMailer;
        $mail->CharSet = "utf-8";
        $mail->isHTML(true);          // Specify main and backup SMTP servers
        $mail->isSMTP();
        $mail->SMTPAuth = true;        // SMTP password
        $mail->SMTPSecure = 'tls';         // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;
        $mail->Host = 'smtp.mandrillapp.com';                   // Enable SMTP authentication
        $mail->Username = 'nick@victorytailgate.com';          // SMTP username
        $mail->Password = 'uw6gpNRDBFaLynRSs0M0ag';                   // TCP port to connect to
        $mail->From = 'noreply@victorytailgate.com';
        $mail->FromName = 'Victory Tailgate';
        $mail->addAddress($cert['email']);
        $mail->addBCC('paul@victorytailgate.com');
        $mail->Subject = "Here's your Victory Tailgate Gift Certificate!";
        $mail->MsgHTML($body);
        $mail->addAttachment($gcfile);
        if (!$mail->send()) {
            LL::log(LL::red, "Mailer error");
            // opsgenie
            $body = '{
                "message": "Gift cert error",
                "description": "Mail send error",
                "teams": [{"name": "Web"}],
                "tags": ["Gift Certificate", "Crond"],
                "priority": "P3"
            }';
            sendAlert($body);
        } else {
            LL::log(LL::green, "Email sent");

            // destroy image
            unlink($gcfile);

            // order comment
            if($master->insert('order_comments', array('order_id' => $cert['order_id'], 'comment' => "Gift Code: $code $gcamnt<br>Email sent", 'date_added' => $master->now())))
                LL::log(LL::green, 'Order updated');
            else LL::log(LL::red, 'Order update error: '.$master->getLastError());

            // update item
            $master->where('item_id', $cert['item_id']);
            if($master->update('ordered_items', array('item_status' => 7), 1))
                LL::log(LL::green, 'Item updated');
            else LL::log(LL::red, 'Item update error: '.$master->getLastError());
        }

    }
}

/**
 * Send OpsGenie alert
 */
function sendAlert($body) {
    $curl = new \Curl\Curl();
    $curl->setHeader('content-Type', 'application/json');
    $curl->setHeader("authorization", "GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf");
    $curl->post('https://api.opsgenie.com/v2/alerts', $body);
    if ($curl->error)
        LL::log(LL::red, $curl->errorMessage);
}