<?php
//ini_set('display_errors', '0');
//error_reporting(E_ALL & ~E_STRICT);

/**
 * check if running
 */
exec('ps aux | grep -v grep | grep -v '.getmypid().' | grep -v "/bin/sh -c" | grep '.basename(__FILE__), $check);
if(count($check)>0) {
    mail('paul@victorytailgate', 'Amazon script running long', $check[0]);
    die("\nAlready running: {$check[0]}\n\n");
}

require_once('../shared-resources/_configs/configs.inc');
require_once('../shared-resources/lurdlogger.php');
require_once('../shared-resources/_models/vt-classes/Product.php');

$test = false; //LL::log(LL::red, '== TEST SCRIPT ==');

$holidays = array('2017-12-24', '2017-12-25', '2017-12-26', '2018-01-01');

$states = array( 'AL'=>'Alabama', 'AK'=>'Alaska', 'AZ'=>'Arizona', 'AR'=>'Arkansas', 'CA'=>'California', 'CO'=>'Colorado', 'CT'=>'Connecticut', 'DE'=>'Delaware', 'DC'=>'District of Columbia', 'FL'=>'Florida', 'GA'=>'Georgia', 'HI'=>'Hawaii', 'ID'=>'Idaho', 'IL'=>'Illinois', 'IN'=>'Indiana', 'IA'=>'Iowa', 'KS'=>'Kansas', 'KY'=>'Kentucky', 'LA'=>'Louisiana', 'ME'=>'Maine', 'MD'=>'Maryland', 'MA'=>'Massachusetts', 'MI'=>'Michigan', 'MN'=>'Minnesota', 'MS'=>'Mississippi', 'MO'=>'Missouri', 'MT'=>'Montana', 'NE'=>'Nebraska', 'NV'=>'Nevada', 'NH'=>'New Hampshire', 'NJ'=>'New Jersey', 'NM'=>'New Mexico', 'NY'=>'New York', 'NC'=>'North Carolina', 'ND'=>'North Dakota', 'OH'=>'Ohio', 'OK'=>'Oklahoma', 'OR'=>'Oregon', 'PA'=>'Pennsylvania', 'RI'=>'Rhode Island', 'SC'=>'South Carolina', 'SD'=>'South Dakota', 'TN'=>'Tennessee', 'TX'=>'Texas', 'UT'=>'Utah', 'VT'=>'Vermont', 'VA'=>'Virginia', 'WA'=>'Washington', 'WV'=>'West Virginia', 'WI'=>'Wisconsin', 'WY'=>'Wyoming' );
$states_lower = array_map('strtolower', $states);

LL::log(LL::light_blue, "\n".date("Y-m-d H:i:s"));

$list_only = in_array('list', $argv) ? true : false;
$days_off = array('2017-12-25', '2017-12-26', '2018-01-01');
$error = false;

if(!$list_only) {
    /*LL::logn(LL::white, "Check blank amz skus...");
    $upd_sku = $master->rawQuery("UPDATE products SET amazon_sku = id WHERE (amazon_sku IS NULL OR amazon_sku='') AND active='y' AND date_added > date_sub(now(), INTERVAL 1 HOUR)");
    $color = $master->count > 0 ? LL::yellow : LL::light_gray;
    LL::log($color, $master->count . " prods updated");*/
} else {
    LL::log(LL::cyan, '-- list only --');
}

$list = array();
// once a day, look back over a day to cover outages
if(in_array('big', $argv)!==false || (date('Hi')>'0657' && date('Hi')<'0703')) $limit = "-25 hours";
elseif(in_array('med', $argv)!==false) $limit = "-12 hours";
else $limit = "-55 minutes";
// limit override
/*if(isset($argv[1]))
    $limit = $argv[1];*/

$lower = date('Y-m-d H:i:s', strtotime($limit));
$upper = date('Y-m-d H:i:s', strtotime("-20 minutes"));
echo "$limit - $lower - $upper\n";

try {
    $amz = new AmazonOrderList("YourAmazonStore"); //store name matches the array key in the config file
    $amz->setLimits('Modified', $lower, $upper); //accepts either specific timestamps or relative times
    //$amz->setFulfillmentChannelFilter("MFN"); //no Amazon-fulfilled orders
    if(in_array('afn', $argv))
        $amz->setFulfillmentChannelFilter("AFN"); //no Amazon-fulfilled orders
    $amz->setOrderStatusFilter(
        array("Unshipped", "PartiallyShipped", "Shipped")
    ); //no shipped or pending orders
    $amz->setUseToken(); //tells the object to automatically use tokens right away
    $amz->fetchOrders(); //this is what actually sends the request
    $list = $amz->getList();
} catch (Exception $ex) {
    echo 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
}
$count = count($list);
$raw = $amz->getRawResponses();
$r = $raw[0];
if($r['ok']==0) {
    LL::log(LL::red, $r['error'].': '.$r['body']);
    $body = '{
        "message": "Amazon SC Order Feed error",
        "description": "Error: '.$r['error'].'",
        "teams": [{"name": "Web"}],
        "tags": ["Amazon", "Crond"],
        "priority": "P4"
    }';
    sendAlert($body);
    //print_r($r);
} elseif($count==0) {
    LL::logn(LL::white, '.');
} else {
    LL::log(LL::yellow, "$count to check...");

    $slave->where('title like "Boards: %"');
    $result = $slave->get('product_types', null, 'product_type_id');
    $board_types = array_column($result, 'product_type_id');
    $board_types[] = 10020; // Baggo

    $slave->where('title like "Bags: %"');
    $result = $slave->get('product_types', null, 'product_type_id');
    $bag_types = array_column($result, 'product_type_id');

    foreach ($list as $order) {
        if($order->getFulfillmentChannel()=='AFN') LL::log(LL::purple, '-- AMAZON FULFILLED --');
        //else LL::log(LL::white, $order->getFulfillmentChannel());

        $pdate = date('Y-m-d H:i:s', strtotime($order->getPurchaseDate()));
        $moddate = date('Y-m-d H:i:s', strtotime($order->getLastUpdateDate()));

        $esd = date('Y-m-d 00:00:00', strtotime($order->getEarliestShipDate()));
        $lsd = date('Y-m-d 00:00:00', strtotime($order->getLatestShipDate()));
        $thresh = date('Y-m-d 00:00:00', strtotime('+3 weekday'));

        /**
         * if earliest sbd in amazon is within three days, don't subtract a day internally
         * if it's truly 1-3 days in amazon, don't make it more aggressive or production will freak out

        if($esd <= $thresh) {
        // use earliest
        $sbd = $esd;
        } else {
        // earliest - 1
        $sbd = date('Y-m-d H:i:s', strtotime('-1 weekday', strtotime($esd)));
        }*/

        // latest - 1
        $sbd = $esd;
        if($sbd > $thresh)
            $sbd = $thresh;

        // holiday fix
        if(date('Y-m-d', strtotime($sbd))=='2018-01-01') {
            LL::log(LL::yellow, 'Holiday move');
            $sbd = '2018-01-02 00:00:00';
        }

        // skip weekends, days off
        //LL::logn(LL::yellow, 'calc sbd...');
        while(in_array(date('D', strtotime($sbd)), array('Sat', 'Sun')) || in_array(date('Y-m-d', strtotime($sbd)), $days_off)) {
            //LL::logn(LL::yellow, 'skip...');
            $sbd = date('Y-m-d H:i:s', strtotime('+1 weekday', strtotime($sbd)));
            if($sbd==$lsd) {
                LL::logn(LL::red, 'LSD! ');
                break;
            }
        }

        $site = $order->getFulfillmentChannel()=='AFN' ? 10007 : 3;

        // check if order exists:
        $amzn_order_id = $order->getAmazonOrderId();
        $slave->where('site', $site);
        $slave->where('site_order_num', $amzn_order_id);
        $order_result = $slave->getOne('orders', array('order_id', 'ship_by'));
        if(!$test && !empty($order_result)) {

            if($list_only) {
                echo "$amzn_order_id, {$order_result['order_id']}, $esd, $lsd, {$order_result['ship_by']}\n";
                continue;
            }

            LL::log(LL::light_gray, 'Order exists: '.$order_result['order_id']); //.' / '.$order_result['ship_by']);
            //LL::log(LL::white, (float)$order->getOrderTotal()['Amount']);

            // fix sbd
            if(is_null($order_result['ship_by'])) {
                $master->where('order_id', $order_result['order_id']);
                $master->where('site_order_num', $amzn_order_id);
                if ($master->update('orders', array('ship_by' => $sbd)))
                    LL::log(LL::yellow, "\tupdated sbd to $sbd");
            }

            continue;
        }

        if($list_only) {
            //echo "$moddate, $amzn_order_id, $esd, $lsd, est: $sbd\n";
            print_r($order->getData());
            continue;
        }

        $address = $order->getShippingAddress(); //address is an array
        $ship_nameparts = explode(' ', $address['Name']);

        // state fix
        $ship_state = $address['StateOrRegion'];
        if(strlen($ship_state)>2) {
            // state is full text, get abbr
            $key = array_search(strtolower($ship_state), $states_lower);
            if($key)
                $ship_state = $key;
            //mail('paul@victorytailgate.com', 'Amazon state fix', "check $amzn_order_id");
        }

        //$order_total = $order->getOrderTotal();

        $status = $order->getFulfillmentChannel()=='AFN' ? 3 : 7; // auto-ship amazon fulfilled
        $comments = $order->getFulfillmentChannel()=='AFN' ? 'AMAZON FULFILLED' : ''; // auto-ship amazon fulfilled

        //LL::log(LL::yellow, (float)$order->getOrderTotal()['Amount']);
        $order_total = (float)$order->getOrderTotal()['Amount'];
        $order_data = array(
            'name' => $order->getBuyerName(),
            'email' => $order->getBuyerEmail() ? $order->getBuyerEmail() : 'info@victorytailgate.com',
            'phone' => is_null($address['Phone']) ? '' : $address['Phone'],
            'subtotal' => 0, // will select earliest from items
            'tax' => 0, // will select earliest from items
            'shipping' => 0, // will select earliest from items
            'total' => $order_total,
            'date_added' => $master->now(),
            'status' => $status,
            'shipping_first_name' => $ship_nameparts[0],
            'shipping_last_name' => end($ship_nameparts),
            'shipping_address1' => is_null($address['AddressLine1']) ? '' : $address['AddressLine1'],
            'shipping_address2' => is_null($address['AddressLine2']) ? '' : $address['AddressLine2'],
            'shipping_city' => is_null($address['City']) ? '' : $address['City'],
            'shipping_state' => is_null($ship_state) ? '' : $ship_state,
            'shipping_zip' => explode('-', $address['PostalCode'])[0],
            'comments' => $comments,
            'shipping_full_name' => $address['Name'],
            'site' => $site,
            'site_order_num' => $amzn_order_id,
            'ship_by' => $sbd,
            'updated_by' => 546
        );
        //print_r($order_data);

        /**
         * GET ORDERED ITEMS
         */
        $items = array();
        try {
            $amz = new AmazonOrderItemList("YourAmazonStore"); //store name matches the array key in the config file
            $amz->setOrderId($amzn_order_id);
            $amz->fetchItems(); //this is what actually sends the request
            $items = $amz->getItems();
        } catch (Exception $ex) {
            $msg = 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
            LL::log(LL::red, $msg);
            $body = '{
                "message": "Amazon SC Order Feed error",
                "description": "'.$msg.'",
                "teams": [{"name": "Web"}],
                "tags": ["Amazon", "Crond"],
                "priority": "P3"
            }';
            sendAlert($body);
            continue;
        }

        if(count($items)==0) {
            $msg = "No items found on order $amzn_order_id";
            LL::log(LL::red, $msg);
            $body = '{
                "message": "Amazon SC Order Feed error",
                "description": "'.$msg.'",
                "teams": [{"name": "Web"}],
                "tags": ["Amazon", "Crond"],
                "priority": "P3"
            }';
            sendAlert($body);
            continue;
        }

        $ordered_items = [];

        $subtotal = 0;
        $shipping = 0;
        $tax = 0;
        $gift = 0;
        $giftmsg = '';

        $cart = new Cart($master);
        $hasBags = false;

        /**
         * shows what types are grouped together
         */
        $shippingGroupTemplates = [
            "boardSet" => [
                "board1" => [149, 150, 146, 12, 127, 25, 27, 26, 24, 152, 153, 151, 10063, 10015, 10059, 10012, 10016, 10058],
                "board2" => [149, 150, 146, 12, 127, 25, 27, 26, 24, 152, 153, 151, 10063, 10015, 10059, 10012, 10016, 10058],
                "bag1" => [10104, 10086, 10093, 82, 3, 10092, 81],
                "bag2" => [10104, 10086, 10093, 82, 3, 10092, 81],
                "acc" => [158, 163, 164, 157, 88, 133, 75, 272, 10097, 16, 10056]
            ]
        ];
        $currentShippingGroups = []; // will store available slots when groups are started

        /**
         * initial product loop
         */
        $itemsToAdd = [];
        $gid_index = 1;
        foreach($items as $item) {

            $sku = $item['SellerSKU'];

            $slave->where('p.amazon_sku', $sku);
            $slave->orWhere('p.id', $sku);
            $prod = $slave->getOne('products p', array('p.id', 'p.product_type'));

            //$item['group_id'] = $this_gid;
            $qty = $item['QuantityOrdered'];

            $subtotal += (float)$item['ItemPrice']['Amount'];
            $tax += (float)$item['ItemTax']['Amount'];
            $shipping += (@(float)$item['ShippingPrice']['Amount'] + @(float)$item['ShippingTax']['Amount']);

            // flag dem bags
            if($prod && in_array($prod['product_type'], $bag_types)) {
                //if($hasBags == false) LL::log(LL::blue, 'Bags found');
                $hasBags = true;
            }

            // split 8 bags to 4
            if (in_array($prod['product_type'], array(78, 79, 83))) {

                // find 4 bags
                $bags = $slave->rawQuery("select b.output_product_id from bundled_products b inner join products p on p.id=b.output_product_id where input_product_id = {$prod['id']} and p.active='y'");
                if(count($bags) < 2) { $itemsToAdd[] = $item; }
                else {
                    LL::log(LL::cyan, "splitting 8 bag set: {$prod['id']} => ".json_encode(array_column($bags, 'output_product_id')));
                    foreach($bags as $bag) {
                        $itemsToAdd[] = array_merge($item, array(
                            'SellerSKU' => $bag['output_product_id'],
                            'ItemPrice' => array('Amount' => (float)round($item['ItemPrice']['Amount'] / 2, 3))
                        ));
                    }
                }
            } else {
                $itemsToAdd[] = $item;
            }
        }

        foreach($itemsToAdd as $item) {
            $sku = $item['SellerSKU'];
            $slave->where('p.amazon_sku', $sku);
            $slave->orWhere('p.id', $sku);
            $slave->join('team_list l', 'p.team_id=l.team_id', 'LEFT');
            $prod = $slave->getOne('products p', array('p.id', 'p.name', 'p.product_type', 'l.team_name', 'p.stock', 'p.team_id'));
            if(!$prod) {
                $item_json = json_encode($item);
                $error = "Product $sku not found on order $amzn_order_id<br />";
                LL::log(LL::red, "\tItem insert failed: {$master->getLastError()}");
                continue;
            }

            //$stock = Product::getAvailableStock($slave, $prod['id']);
            $price = (float)round($item['ItemPrice']['Amount']/$item['QuantityOrdered'],2);
            $qty = $item['QuantityOrdered'];
            $type = $prod['product_type'];

            /**
             * get group ID
             */
            LL::log(LL::yellow, "start {$prod['name']} ($type)");
            $this_gid = getActiveGroupId($currentShippingGroups, $type);

            // if there was no active group, see if we can start one
            if(!$this_gid) {
                foreach ($shippingGroupTemplates as $group_name => $group) {
                    foreach ($group as $i => $type_row) {
                        foreach ($type_row as $j => $check_type) {
                            if ($type == $check_type) {
                                // start active group
                                $this_gid = $gid_index++;
                                $currentShippingGroups[$this_gid] = $shippingGroupTemplates[$group_name];
                                LL::log(LL::cyan, "$group_name group started ($this_gid)");
                                // remove slot from active group
                                getActiveGroupId($currentShippingGroups, $prod['product_type']);
                                break 3;
                            }
                        }
                    }
                }

                if(!$this_gid) {
                    $this_gid = $gid_index++;
                    LL::log(LL::blue, "no group template, using GID: $this_gid");
                }
            }

            $item['group_id'] = $this_gid;

            $info = '';

            // check gift message
            if(isset($item['GiftMessageText'])) {
                $gift = 1;
                $giftmsg .= $item['GiftMessageText'];
            }

            $item_status = $order->getFulfillmentChannel()=='AFN' ? 7 : 0; // auto-ship amazon fulfilled

            $slots = $cart->GetSlotNums($prod['id'], $qty);
            if(in_array($type, $board_types) && count($slots)>0)
                $type = 28; // in stock new

            // handle bundle
            $bundle_items = $slave->rawQuery("select p.id, p.name, p.product_type from selected_products_addons a inner join products p on p.id=a.selected_addon_id where a.marketplace_id = 3 and a.product_id = {$prod['id']}");
            if($bundle_items) {
                foreach($bundle_items as $bundle_add) {
                    $ordered_items[] = array(
                        'order_id' => 0,
                        'product_id' => $bundle_add['id'],
                        'name' => $bundle_add['name'],
                        'price' => 0,
                        'date_added' => $master->now(),
                        'product_type' => $bundle_add['product_type'],
                        'quantity' => $qty,
                        'info' => 'Bundled item',
                        'item_status' => $item_status,
                        'group_id' => $item['group_id']
                    );
                }
            } else {
                // no bundle in the DB? If it's a board, need note to manually add bags!
                if(in_array($type, $board_types))
                    $info .= 'Bags: 8 corn-filled best matching colors<br>Plus any additional bags listed with this order.';
            }

            $ordered_items[] = array(
                'order_id' => 0,
                'product_id' => $prod['id'],
                'name' => $prod['name'],
                'price' => $price,
                'date_added' => $master->now(),
                'product_type' => $type,
                'quantity' => $qty,
                'info' => $info,
                'stock' => $prod['stock'],
                'slots' => $slots,
                'item_status' => $item_status,
                'group_id' => $item['group_id']
            );

            // if tumble, free carrying case (include team name in info) 224353 Onyx Stained or No Stain
            if(in_array($type, array(10091, 266))) {
                $cc = null;
                if($prod['team_id'] > 0) {
                    // check for team bag
                    $slave->where('team_id', $prod['team_id']);
                    $slave->where('product_type', 269);
                    $slave->where('active', 'y');
                    $cc = $slave->getOne('products', array('id', 'name'));
                }
                $ccid = count($cc)>0 ? $cc['id'] : 224353;
                $ccname = count($cc)>0 ? $cc['name'] : 'Team Tumble Tower Carrying Case';
                //$info = strpos($prod['name'], 'Stained')!==false ? 'Onyx Stained' : 'No Stain';
                $info = '';
                $ordered_items[] = array(
                    'order_id' => 0,
                    'product_id' => $ccid,
                    'name' => $ccname,
                    'price' => 0,
                    'date_added' => $master->now(),
                    'product_type' => 269,
                    'quantity' => $qty,
                    'info' => $info,
                    'item_status' => $item_status,
                    'group_id' => $item['group_id']
                );
            } elseif($type==10014) { // gameday tower
                $cc = [];
                $ccid = count($cc)>0 ? $cc['id'] : 1222675;
                $ccname = count($cc)>0 ? $cc['name'] : 'Black Gameday Tower Carrying Case';
                $ordered_items[] = array(
                    'order_id' => 0,
                    'product_id' => $ccid,
                    'name' => $ccname,
                    'price' => 0,
                    'date_added' => $master->now(),
                    'product_type' => 10055,
                    'quantity' => $qty,
                    'info' => '',
                    'item_status' => $item_status,
                    'group_id' => $item['group_id']
                );
            } elseif($type==148) { // jr package
                $info = $prod['team_name'];
                $ordered_items[] = array(
                    'order_id' => 0,
                    'product_id' => 24156,
                    'name' => 'Custom Junior Bag Set (corn-filled)',
                    'price' => 0,
                    'date_added' => $master->now(),
                    'product_type' => 85, // bags: custom
                    'quantity' => $qty,
                    'info' => $info,
                    'item_status' => $item_status
                );
                $ordered_items[] = array(
                    'order_id' => 0,
                    'product_id' => 24155,
                    'name' => 'Custom Junior Carrying Case',
                    'price' => 0,
                    'date_added' => $master->now(),
                    'product_type' => 166, // acc: cc junior
                    'quantity' => $qty,
                    'info' => $info,
                    'item_status' => $item_status,
                    'group_id' => $item['group_id']
                );
            }
        }

        if($test) {
            print_r(json_encode($ordered_items));
            echo "\n ---------- \n";
            continue;
        }

        /**
         * INSERT ORDER
         */
        $order_data['subtotal'] = $subtotal;
        $order_data['tax'] = $tax;
        $order_data['shipping'] = $shipping;
        $order_data['gift'] = $gift;
        $order_data['giftmsg'] = $giftmsg;

        $order_id = $master->insert('orders', $order_data);
        if(!$order_id) {
            $error = "Order insert failed on order $amzn_order_id:<br />
                tried: {$master->getLastQuery()}<br />
                error: {$master->getLastError()}<br />";

            LL::log(LL::red, "Order insert failed on $amzn_order_id");
            LL::log(LL::red, "\t{$master->getLastError()}");
            continue;
        } else {
            LL::log(LL::green, "Order inserted: $order_id / $amzn_order_id / $sbd / (lsd:$lsd)");
        }

        /**
         * INSERT ORDERED ITEMS
         */
        foreach($ordered_items as $item)
        {
            // canceled items come in as 0 qty, don't include these
            if($item['quantity']==0)
                continue;

            $stock = isset($item['stock']) ? $item['stock'] : 0;
            unset($item['stock']);
            $slots = isset($item['slots']) ? $item['slots'] : array();
            unset($item['slots']);

            // check slots
            $slot_ids = false;
            if(count($slots)>0) {
                $slot_ids = array_keys($slots);
            }

            $item['order_id'] = $order_id;
            $item['updated_by'] = 546;

            // split qty
            for($i=0; $i<$item['quantity']; $i++) {
                $split = in_array($item['product_type'], $board_types) && !in_array($item['product_type'], array(10020, 73)) ? 2 : 1;
                for($j=0; $j<$split; $j++) {
                    $insert = $item;
                    $insert['quantity'] = 1;
                    $insert['slot_nums'] = isset($slots[$i]) ? $slots[$i] : "";
                    $insert['price'] /= $split;

                    if ($split>1 && $insert['slot_nums']=='') {
                        $insert['info'] = ($j % 2 == 0) ? 'ONE BOARD PRINT A<br>'.$insert['info'] : 'ONE BOARD PRINT B';
                    }

                    $item_id = $master->insert('ordered_items', $insert);
                    if (!$item_id) {
                        $error = "Item insert failed: {$master->getLastError()}";
                        /*$body = '{
                            "message": "Amazon SC Order Feed error",
                            "description": "'.$error.'",
                            "teams": [{"name": "Web"}],
                            "tags": ["Amazon", "Crond"],
                            "priority": "P3"
                        }';
                        sendAlert($body);*/
                        LL::log(LL::red, "\t$error");
                        continue;
                    } else {
                        /**
                         * remove slot
                         */
                        if ($insert['slot_nums'] != "") {
                            $master->where('slot_num', $insert['slot_nums']);
                            $master->where('product_id', $insert['product_id']);
                            $master->where('quantity', 0, '>');
                            $upd_data = array('quantity' => $master->dec(1));
                            if ($master->update('products_slots', $upd_data, 1))
                                LL::log(LL::cyan, "slot {$insert['slot_nums']} used");
                        }
                    }
                }
            }

            // delete slot
            /*if($slot_ids) {
                $master->where('slot_id', $slot_ids, 'IN');
                $master->delete('products_slots', count($slot_ids));
                LL::log(LL::light_blue, "\tprev stock: $stock / slot_ids used: ".implode(',', $slot_ids));
            }*/

            // reduce stock
            $item_update_data = array(
                'stock' => $master->dec($item['quantity'])
            );
            $master->where('id', $item['product_id']);
            $master->where('stock', $item['quantity'], '>');
            $master->update('products', $item_update_data);
        }
    }

    LL::log(LL::white, "END\n--------------------------");
}

if($error) {
    $mail = new PHPMailer;
    $mail->isSMTP();                                       // Set mailer to use SMTP
    $mail->Host = 'smtp.mandrillapp.com';                  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                                // Enable SMTP authentication
    $mail->Username = 'nick@victorytailgate.com';          // SMTP username
    $mail->Password = 'uw6gpNRDBFaLynRSs0M0ag';            // SMTP password
    $mail->SMTPSecure = 'tls';                             // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                                     // TCP port to connect to
    $mail->From = 'noreply@victorytailgate.com';
    $mail->FromName = 'Amazon Import Script';
    $mail->addAddress('paul@victorytailgate.com');
    $mail->Subject = 'Amazon Import Error';
    $mail->MsgHTML($error);
    $mail->isHTML(true);
    $mail->CharSet = "utf-8";
    if (!$mail->send()) {
        LL::log(LL::red, "error sending notice");
    } else {
        LL::log(LL::blue, "notice sent");
    }
    $body = '{
        "message": "Amazon SC Order Feed error",
        "description": "'.str_replace('<br>', ' ', $error).'",
        "teams": [{"name": "Web"}],
        "tags": ["Amazon", "Crond"],
        "priority": "P3"
    }';
    sendAlert($body);
}

$curl = new \Curl\Curl();
$curl->setHeader('content-Type', 'application/json');
$curl->setHeader("authorization", "GenieKey c8715792-5166-480b-8983-a21976478dda");
$curl->get('https://api.opsgenie.com/v2/heartbeats/AmazonSCImport/ping');
if ($curl->error)
    LL::log(LL::red, 'heartbeat error: '.$curl->errorMessage);

/**
 * Send OpsGenie alert
 */
function sendAlert($body) {
    $curl = new \Curl\Curl();
    $curl->setHeader('content-Type', 'application/json');
    $curl->setHeader("authorization", "GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf");
    $curl->post('https://api.opsgenie.com/v2/alerts', $body);
    if ($curl->error)
        LL::log(LL::red, 'send alert error: '.$curl->errorMessage);
}

function getActiveGroupId(&$currentShippingGroups, $product_type) {
    LL::log(LL::blue, "checking active groups for $product_type...");
    // check active groups for available slot based on product type
    foreach($currentShippingGroups as $gid => $group) {
        LL::logn(LL::blue, "check group $gid...");
        foreach($group as $group_item => $type_row) {
            if(in_array($product_type, $type_row)) {
                // remove row from active group to signify it's slot is filled
                unset($currentShippingGroups[$gid][$group_item]);
                LL::log(LL::green, "$group_item slot used");
                return $gid;
            }
        }
    }

    //LL::log(LL::red, 'none');
    return false;
}
