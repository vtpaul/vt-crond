require('colors').enabled = true
/**
 * object format:
 * box_type: {
 *   product_type_name: [ max_qty: [...product_types_to_include] ],
 *   product_type_name: [ max_qty: [...product_types_to_include] ],
 *   ...
 * }
 */
const SHIPPING_GROUP_TEMPLATES = {
    "boardSet": {
        "board": [2, [149, 150, 146, 12, 127, 25, 27, 26, 24, 152, 153, 151, 10063, 10015, 10059, 10012, 10016, 10058] ],
        "4bagSet": [2, [10104, 10086, 10093, 82, 3, 10092, 81] ],
        "scoreKeeper": [1, [10004] ],
        "acc": [99, [158, 163, 164, 157, 88, 133, 75, 272, 10097, 16, 10056, 10004] ]
    },
    "decals": {
        "decal": [5, [10075,10076,10074] ]
    },
    "wallDecals": {
        "decal": [5, [10070,10071,10072,10073] ]
    }
}

module.exports = class Shipping {
    constructor () {
        this.freeGid = 1
        this.currentShippingGroups = {}
    }
    
    assignGroupByProductType(productType) {
        let gid = this.assignToActiveGroup(productType)
        if (!gid) gid = this.startNewGroup(productType)
        if (!gid) gid = this.freeGid++

        return gid
    }

    assignToActiveGroup(productType) {
        console.log('assign ', productType)
        for (let gid in this.currentShippingGroups) {
            let group = this.currentShippingGroups[gid]
            //if (typeof group !== "object") continue
            for (let slotName in group) {
                let slotData = group[slotName],
                    groupQty = slotData[0],
                    groupTypes = slotData[1]
                //if (!Array.isArray(groupTypes)) continue
                if (groupQty > 0 && groupTypes.indexOf(productType) > -1) {
                    this.currentShippingGroups[gid][slotName][0] -= 1
                    console.log(`using ${slotName} in group ${gid}`)
                    console.log(this.currentShippingGroups[gid])
                    console.log(this.currentShippingGroups)

                    return parseInt(gid)
                }
            }
        }

        return false
    }

    startNewGroup(productType) {
        console.log('start new for ', productType)
        for (let groupName in SHIPPING_GROUP_TEMPLATES) {
            let group = SHIPPING_GROUP_TEMPLATES[groupName]
            for (let slotName in group) {
                let slotData = group[slotName],
                    groupQty = slotData[0],
                    groupTypes = slotData[1]
                for (let checkType of groupTypes) {
                    if (productType === checkType) {
                        let gid = this.freeGid++
                        this.currentShippingGroups[gid] = Object.assign({}, SHIPPING_GROUP_TEMPLATES[groupName])
                        console.log(SHIPPING_GROUP_TEMPLATES[groupName])
                        this.currentShippingGroups[gid][slotName] = [--groupQty, groupTypes]
                        console.log(`new ${groupName} group started: (${gid})`)
                        console.log(this.currentShippingGroups)

                        return gid
                    }
                }
            }
        }
        process.exit()
        return false
    }
}
