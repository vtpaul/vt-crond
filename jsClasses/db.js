const { readFile } = require('fs')
const request = require('request-promise')
const mysql = require('mysql')

module.exports = class DB {
    // todo: change gql argument to agnostic query argument
    constructor (config, gql = null, variables = {}) {
        this.dbPool = mysql.createPool(config.db)
        this.gqlOptions = {
            url: config.gql.baseUrl,
            headers: config.gql.headers,
            json: true
        }

        if (gql) return this.execute(gql, variables)
    }

    loadQuery (fileName) {
        return new Promise((resolve, reject) => {
            readFile(fileName, 'utf-8', (err, data) => {
                err ? reject(err.message.red) : resolve(data)
            })
        })
    }

    async executeGql (gql, variables = {}) {
        if (gql.indexOf('/') > -1) {
            gql = await this.loadQuery(gql)
        }

        this.gqlOptions.body = {
            'query': gql,
            'variables': variables
        }
        this.gqlOptions.method = 'POST'

        return new Promise((resolve, reject) => {
            request(this.gqlOptions)
                .then(response => {
                    response.errors ?
                        reject(response.errors[0].message) :
                        resolve(response.data)
                })
                .catch(err => reject(err.error.errors ? err.error.errors[0].message : err.error))
        })
    }
    
    async executeSql (sql, variables = []) {
        if (sql.indexOf('/') > -1) { 
            sql = await this.loadQuery(sql) 
        }

        return new Promise((resolve, reject) => {
            this.dbPool.getConnection((err, conn) => {
                if (err) reject(err)
                else {
                    conn.query(sql, variables, (err, rows) => {
                        conn.release()
                        err ? reject(err.message) : resolve(rows)
                    })
                }
            })
        })
    }
}