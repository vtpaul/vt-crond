/**
 * Created by paul on 4/3/18.
 */
require('colors').enabled = true

const util = require('util')
const fs = require('fs-extra')
const xml2js = require('xml2js')
const async = require('async')
const request = require('request')
const sftpClient = require('ssh2-sftp-client')
//const ssh = require('node-ssh')
const moment = require('moment-timezone')
const momentBiz = require('moment-business')
const pad = require('pad')
const checkdigit = require('checkdigit')
moment.tz.setDefault('America/New_York')

const x12 = require('./classes/x12.js')
const sps = require('./classes/sps.js')
const vtdb = require('./classes/vtdb.js')

/** load config */
let confArg = process.argv.find(i => i.indexOf('config=') >= 0); //console.log(confArg)
let conf = confArg ? confArg.substring(7) : 'config.json'
console.log(`config: ${conf}`.blue)
const CONFIGFILE = require(`./${conf}`)

/** parse args */
const action = process.argv[2] || 'import'
const test = process.argv.indexOf('test') >= 0
const one = process.argv.indexOf('one') >= 0
const noack = process.argv.indexOf('noack') >= 0

/** stuff that probably shouldn't live here */
const boardTypes = [135,12,127,149,150,146,147,25,28,29,145,148,162,27,250,1,26,152,153,24,151,10015,10012,10016,10020]
const bagTypes = [10086,10093,82,3,10092,81,79,83,139,78,85,18,74,154,155]

let PROVDIR, ORDERDIR, SKUMAP = []

const downloadOrders = (connection, callback) => {
    /**
     * pull down orders
     */
    let sftp = new sftpClient()
    let CONN = connection.get
    if(CONN.privateKey)
        CONN.privateKey = fs.readFileSync(CONN.privateKey)
    let PULLDIR = CONN.dir
    sftp.connect(CONN).then(() => {
        console.log('connected')
        return sftp.list(PULLDIR);
    }).then((files) => {
        console.log(`${files.length} orders to import`.yellow)
        if(test)
            console.log('files:', files)
        let orders = []
        async.forEachSeries(files, (fdata, next) => {
            console.log(`save file ${fdata.name}...`)
            sftp.get(`${PULLDIR}/${fdata.name}`).then((stream) => {
                process.stdout.write(`\tsaved, remove from ftp...`)
                stream.once('end', () => {
                    // remove from ftp
                    sftp.delete(`${PULLDIR}/${fdata.name}`).then(() => {
                        //if(test) process.exit() // not supposed to get here in test
                        console.log(`removed`)
                        next(one ? 'one' : false)
                    })
                }).pipe(fs.createWriteStream(`${ORDERDIR}/${fdata.name}`))
            })
        }, (err) => {
            if (err) console.error('error:', err.red)
            else console.log(`done\n`.green)
            sftp.end()
            callback()
        })
    }).catch((err) => {
        console.log(err, 'catch error');
    });
}

const processOrder = (file, conn, callback) => {
    console.log(`processing ${conn.format} ${file}`.yellow)
    if(conn.format == 'xml') processXml(file, conn, callback)
    else if(conn.format == 'x12') processX12(file, conn, callback)
    else {
        console.log('no format?'.red)
        process.exit()
    }
}

const processX12 = function(file, conn, callback) {
    let orderData,
        itemData = [],
        metaData = {} // to be passed around functions
    metaData.rejected = []

    async.waterfall([
        (cb) => x12.parse(`${ORDERDIR}/${file}`, cb),
        (order, cb) => {
            console.log(util.inspect(order, false, null, true /* enable colors */))
            metaData.parsed = order
            let ponum = order.orderDetail.poNumber

            console.log(`PO: ${ponum}`)

            let purpose = order.orderDetail.purposeCode
            /**
             * 00 Original
             * 01 Cancellation
             * 05 Replace
             * 06 Confirmation
             * 07 Duplicate
             */
            // check if exists
            vtdb.query(`select order_id from orders where customer_po = ?`, [ponum], (err, rows) => {
                if (!test && rows.length > 0) {
                    console.log(`${ponum} already exists as ${rows[0].order_id} (${purpose})`.magenta)
                    if (purpose == '01') { // cancel
                        console.log('Cancel code found'.magenta)
                        vtdb.query(`update orders set status = 4 where order_id = ? limit 1;`, [rows[0].order_id], cb)
                    } else if (purpose == '05') {
                        console.log('Replace?'.magenta)
                        cb()
                    } else if (purpose == '06') {
                        console.log('Confirmation?'.magenta)
                        cb()
                    } else if (purpose == '07') {
                        console.log('Duplicate?'.magenta)
                        cb()
                    } else cb()
                } else {
                    metaData.origFile = file
                    async.waterfall([
                        (procStep) => {
                            let senderID = order.header.interchangeControl.senderId
                            let site = CONFIGFILE.sites[senderID]
                            metaData.site = site

                            console.log(`site ${site}: ${senderID}`)

                            let discount_codes = ['C310', 'C300', 'C350', 'CA00', 'I170', 'I310', 'I530']
                            let shipping_charge_codes = ['G821', 'G830']
                            let total = 0 //order.Summary[0].TotalAmount != undefined ? parseFloat(order.Summary[0].TotalAmount[0]) : 0
                            let tax = 0
                            let shipping = 0
                            let discount = 0
                            let subtotal = total - tax - shipping + discount

                            let package_num = ''

                            let address = {}
                            for(let key in order.names.main) {
                                let val = order.names.main[key]
                                address[val.entityIdCode] = val
                            }
                            let shipping_addr = address.ST || {}

                            let giftmsg = ''

                            async.eachSeries(order.items, (item, nextItem) => {
                                let data = {}

                                // todo: check item status?
                                data.sku = item.productIdQualifier == 'VN' ? item.productId : ''
                                data.upc = item.productIdQualifier == 'UP' ? item.productId : ''
                                data.id = item.productId
                                let prodColumn = data.sku != '' ? 'sku' : 'upc'
                                vtdb.query(`select id from products where ${prodColumn} = ?`, [data.id], (err, rows) => {
                                    data.statusCode = rows.length > 0 ? 'IA' : 'R2'
                                    data.qty = parseInt(item.quantity)
                                    data.price = parseFloat(item.price);

                                    // todo: parse discounts
                                    data.discount = 0 /*'ChargesAllowances' in item ? item.ChargesAllowances.reduce((total, obj) => {
                                        return discount_codes.indexOf(obj.AllowChrgCode[0]) > -1 ? total + parseFloat(obj.AllowChrgAmt[0]) : total
                                    }, 0) : 0;*/

                                    // todo: parse taxes
                                    data.tax = 0 /*'Taxes' in item ? item.Taxes.reduce((total, obj) => {
                                        return total + obj.TaxAmount[0]
                                    }) : 0;*/

                                    // todo: handle notes again
                                    data.info = ''
                                    /*if('Notes' in order.Header[0]) {
                                        let info_array = []
                                        for(let note of order.Header[0].Notes) {
                                            if(note.NoteCode[0] == 'GEN') info_array.push(note.Note[0])
                                        }
                                        info += info_array.join('<br>')
                                    }*/

                                    subtotal += data.price * data.qty
                                    total +=  data.price * data.qty - tax - discount
                                    itemData.push(data)
                                    nextItem()
                                })
                            }, () => {
                                orderData = [
                                    shipping_addr.name || 'No Name', //shipping_addr.AddressName[0],
                                    'email',
                                    'phone',
                                    0, // subtotal
                                    tax,
                                    discount,
                                    shipping,
                                    total,
                                    moment().format('YYYY-MM-DD HH:mm:ss'),
                                    moment().format('YYYY-MM-DD HH:mm:ss'),
                                    4, //7, // status paid
                                    '', //shipping_addr.AddressName[0],
                                    '', //shipping_addr.AddressName[0].split(' ')[0],
                                    '', //shipping_addr.AddressName[0].split(' ').pop(),
                                    '', //shipping_addr.Address1[0],
                                    '', //shipping_addr.Address2 ? shipping_addr.Address2[0] : '',
                                    '', //shipping_addr.City[0],
                                    '', //shipping_addr.State[0],
                                    '', //shipping_addr.PostalCode[0],
                                    site,
                                    ponum, // site_order_num
                                    ponum, // customer_po
                                    null, // SBD
                                    giftmsg.length > 0 ? 1 : 0, // gift
                                    giftmsg,
                                    '', // comments
                                    package_num
                                ]

                                //console.log('order:'.yellow, orderData)
                                insertOrder(orderData, metaData, procStep)
                            })
                        }, (metaData, procStep) => {
                            async.forEachSeries(itemData, (item, nextInsert) => {
                                if(item.statusCode == 'IA') insertItem(item, metaData, nextInsert)
                                else {
                                    metaData.rejected.push(item.id)
                                    console.log('skip rejected item'.red)
                                    nextInsert()
                                }
                            }, procStep)
                        }, (procStep) => acknowledgeX12(orderData, metaData, conn.put, procStep)
                        //(data, procStep) => orderInvoice(order, data, procStep)
                    ], (err) => {
                        if (err) console.log('error:', err)
                        else console.log('inserts done')
                        process.exit()
                        cb(err)
                    })
                }
            })
        }, (cb) => {
            /**
             * get em outta hea
             */
            if (test) cb()
            else {
                fs.rename(`${ORDERDIR}/${file}`, `${ORDERDIR}/processed/${file}`, () => {
                    console.log(`\tfile moved`)
                    cb()
                })
            }
        }
    ], callback)
}

const processXml = function(file, conn, callback) {
    let orderData,
        itemData = [],
        metaData = {} // to be passed around functions
    metaData.rejected = []

    async.waterfall([
        (cb) => fs.readFile(`${ORDERDIR}/${file}`, 'utf-8', cb),
        (file, cb) => xml2js.parseString(file.toString(), cb),
        (orders, cb) => {
            /**
             * loop and process orders
             */
            async.forEachSeries(orders, (node, next) => {
                let order = 'Order' in node ? node['Order'][0] : node
                metaData.parsed = order
                let ponum = order.Header[0].OrderHeader[0].PurchaseOrderNumber[0]
                console.log(`PO: ${ponum}`)
                let purpose = order.Header[0].OrderHeader[0].TsetPurposeCode[0]
                /**
                 * 00 Original
                 01 Cancellation
                 05 Replace
                 06 Confirmation
                 07 Duplicate
                 */
                // check if exists
                vtdb.query(`select order_id from orders where customer_po = ?`, [ponum], (err, rows) => {
                    if (!test && rows.length > 0) {
                        console.log(`${ponum} already exists as ${rows[0].order_id} (${purpose})`.magenta)
                        if (purpose == '01') { // cancel
                            console.log('Cancel code found'.magenta)
                            vtdb.query(`update orders set status = 4 where order_id = ? limit 1;`, [rows[0].order_id], next)
                        } else if (purpose == '05') {
                            console.log('Replace?'.magenta)
                            next()
                        } else if (purpose == '06') {
                            console.log('Confirmation?'.magenta)
                            next()
                        } else if (purpose == '07') {
                            console.log('Duplicate?'.magenta)
                            next()
                        } else next()
                    } else {
                        metaData.origFile = file
                        async.waterfall([
                            (procStep) => {
                                //console.log(order.Meta)
                                let senderID = order.Meta[0].InterchangeSenderID[0]
                                let site = CONFIGFILE.sites[senderID]
                                console.log(`site ${site}: ${senderID}`)

                                metaData.site = site

                                let discount_codes = ['C310', 'C300', 'C350', 'CA00', 'I170', 'I310', 'I530']
                                let shipping_charge_codes = ['G821', 'G830']
                                let total = order.Summary[0].TotalAmount != undefined ? parseFloat(order.Summary[0].TotalAmount[0]) : 0
                                metaData.total = total
                                let tax = 'Taxes' in order.Header[0] ? order.Header[0].Taxes.reduce((total, obj) => {
                                    return total + obj.TaxAmount[0]
                                }) : 0

                                /**
                                 * shipping and discounts
                                 */
                                let shipping = 0
                                let discount = 0
                                if (order.Header[0].ChargesAllowances != undefined) {
                                    shipping = order.Header[0].ChargesAllowances.reduce((total, obj) => {
                                        return shipping_charge_codes.indexOf(obj.AllowChrgCode[0]) > -1 ? total + parseFloat(obj.AllowChrgAmt[0]) : total
                                    }, 0)
                                    discount = order.Header[0].ChargesAllowances.reduce((total, obj) => {
                                        return discount_codes.indexOf(obj.AllowChrgCode[0]) > -1 ? total + parseFloat(obj.AllowChrgAmt[0]) : total
                                    }, 0)
                                }

                                // package number (ReferenceQual = IID)
                                let references = {}
                                if ('References' in order.Header[0]) {
                                    for (let ref of order.Header[0].References) {
                                        references[ref.ReferenceQual[0]] = ref.ReferenceID[0]
                                    }
                                }
                                let package_num = references.IID || ''

                                let subtotal = total - tax - shipping + discount
                                metaData.subtotal = subtotal

                                /**
                                 * Find contact name. Search address node for ST type, else use contact node if available
                                 * AddressTypeCode:
                                 *  BT: bill to
                                 *  RT: return addr
                                 *  VN: vendor
                                 *  ST: ship to
                                 */
                                let address = {}
                                for(let adr of order.Header[0].Address) {
                                    address[adr.AddressTypeCode] = adr
                                }

                                let shipping_addr = address['ST']; //console.log(shipping_addr)
                                let email =
                                    'Contacts' in shipping_addr
                                    && 'PrimaryEmail' in shipping_addr.Contacts[0] ?
                                        shipping_addr.Contacts[0].PrimaryEmail[0] :
                                        ''
                                let phone = 'Contacts' in shipping_addr && 'PrimaryPhone' in shipping_addr.Contacts[0] ? shipping_addr.Contacts[0].PrimaryPhone[0] : ''

                                let giftmsg = ''
                                let comments = `From EDI doc: ${metaData.origFile}`
                                if('Notes' in order.Header[0]) {
                                    let comment_array = []
                                    let gift_array = []
                                    for(let note of order.Header[0].Notes) {
                                        if('NoteCode' in note && note.NoteCode[0] == 'GFT') gift_array.push(note.Note[0])
                                        else comment_array.push(note.Note[0])
                                    }
                                    comments = comment_array.join('<br>')
                                    giftmsg = gift_array.join('<br>')
                                }

                                metaData.sbd = null
                                /*for(let date of order.Header[0].Dates) {
                                 if(date.DateTimeQualifier[0] == '010')
                                 data.sbd = date.Date[0]
                                 }*/
                                //console.log(order.items)
                                //console.log('sku map:'.yellow, SKUMAP)
                                async.eachSeries(order.LineItem, (item, nextItem) => {
                                    console.log('check item:', item.OrderLine[0].BuyerPartNumber[0])
                                    let data = {
                                        sku: null,
                                        upc: null,
                                        qty: 0,
                                        price: 0,
                                        discount: 0,
                                        tax: 0,
                                        info: '',
                                        statusCode: null
                                    }

                                    // todo: check item status?
                                    let buyerPartNum = item.OrderLine[0].BuyerPartNumber[0]
                                    if (SKUMAP[buyerPartNum.toString()]) {
                                        console.log('item in map'.blue)
                                        data.sku = SKUMAP[buyerPartNum.toString()]
                                    } else {
                                        console.log('get prod ID...')
                                        if('VendorPartNumber' in item.OrderLine[0]) data.sku = item.OrderLine[0].VendorPartNumber[0]
                                        else if('ProductID' in item.OrderLine[0]) data.sku = item.OrderLine[0].ProductID[0].PartNumber[0]
                                        if('ConsumerPackageCode' in item.OrderLine[0]) data.upc = item.OrderLine[0].ConsumerPackageCode[0]
                                    }
                                    data.id = data.sku || data.upc
                                    let prodColumn = data.sku ? 'id' : 'upc'
                                    //console.log('data:', data)
                                    let q = vtdb.query(`select id from products where ${prodColumn} = ?`, [data.id], (err, rows) => {
                                        //console.log(rows, q.sql)
                                        data.statusCode = rows.length > 0 ? 'IA' : 'R2'
                                        data.qty = parseInt(item.OrderLine[0].OrderQty[0])
                                        data.price = parseFloat(item.OrderLine[0].PurchasePrice[0]);

                                        // todo: parse discounts
                                        /*'ChargesAllowances' in item ? item.ChargesAllowances.reduce((total, obj) => {
                                         return discount_codes.indexOf(obj.AllowChrgCode[0]) > -1 ? total + parseFloat(obj.AllowChrgAmt[0]) : total
                                         }, 0) : 0;*/

                                        // todo: parse taxes
                                        /*'Taxes' in item ? item.Taxes.reduce((total, obj) => {
                                         return total + obj.TaxAmount[0]
                                         }) : 0;*/

                                        // todo: handle notes again
                                        /*if('Notes' in order.Header[0]) {
                                         let info_array = []
                                         for(let note of order.Header[0].Notes) {
                                         if(note.NoteCode[0] == 'GEN') info_array.push(note.Note[0])
                                         }
                                         info += info_array.join('<br>')
                                         }*/

                                        subtotal += data.price * data.qty
                                        total += data.price * data.qty - tax - discount
                                        itemData.push(data)
                                        nextItem()
                                    })
                                }, () => {
                                    orderData = [
                                        shipping_addr.AddressName[0],
                                        email,
                                        phone,
                                        subtotal, // subtotal
                                        tax,
                                        discount,
                                        shipping,
                                        total,
                                        moment().format('YYYY-MM-DD HH:mm:ss'), //order.Header[0].OrderHeader[0].PurchaseOrderDate[0],
                                        moment().format('YYYY-MM-DD HH:mm:ss'), //order.Header[0].OrderHeader[0].PurchaseOrderDate[0],
                                        7, // status paid
                                        shipping_addr.AddressName[0],
                                        shipping_addr.AddressName[0].split(' ')[0],
                                        shipping_addr.AddressName[0].split(' ').pop(),
                                        shipping_addr.Address1[0],
                                        shipping_addr.Address2 ? shipping_addr.Address2[0] : '',
                                        shipping_addr.City[0],
                                        shipping_addr.State[0],
                                        shipping_addr.PostalCode[0],
                                        site,
                                        //order.Header[0].OrderHeader[0].PurchaseOrderNumber[0],
                                        ponum, // site_order_num
                                        ponum, // customer_po
                                        metaData.sbd,
                                        giftmsg.length > 0 ? 1 : 0, // gift
                                        giftmsg,
                                        comments,
                                        package_num/*,
                                         dsinfo.ds_carrier || '',
                                         dsinfo.ds_account || '',
                                         dsinfo.ds_zip || ''*/
                                    ]

                                    insertOrder(orderData, metaData, procStep)
                                })
                            }, (metaData, procStep) => {
                                if(test) console.log('item data:'.yellow, itemData)
                                async.forEachSeries(itemData, (item, nextInsert) => {
                                    if(item.statusCode == 'IA') insertItem(item, metaData, nextInsert)
                                    else {
                                        metaData.rejected.push(item.id)
                                        console.log('skip rejected item'.red)
                                        nextInsert()
                                    }
                                }, procStep)
                            }, (procStep) => acknowledgeXml(orderData, metaData, conn, procStep)
                            //(data, procStep) => orderInvoice(order, data, procStep)
                        ], (err) => {
                            if (err) console.log('error:', err.red)
                            next(err)
                        })
                    }
                })
            }, cb)
        }, (cb) => {
            /**
             * get em outta hea
             */
            if (test) cb()
            else {
                fs.rename(`${ORDERDIR}/${file}`, `${ORDERDIR}/processed/${file}`, () => {
                    console.log(`\tfile moved`)
                    cb()
                })
            }
        }
    ], callback)
}

const acknowledgeX12 = (orderData, metaData, conn, cb) => {
    //console.log(orderData, metaData, conn, cb)
    if(noack) {
        cb()
        return
    }

    process.stdout.write('gen acknowledgement...')

    let orderX12 = metaData.parsed
    let data = []
    let time = moment().format('HHmm')

    /** opening segments */
    data.push(['ISA', '00', '          ', '00', '          ', 'ZZ', 'VICTORY        ', 'ZZ', 'AMAZON         ', moment().format('YYMMDD'), time, 'U', orderX12.header.interchangeControl.controlVersionNumber, orderX12.header.interchangeControl.controlNumber, '0', 'T', '>'])
    data.push(['GS', 'PR', 'VICTORY', 'AMAZON', moment().format('YYYYMMDD'), time, '1', 'X', '004010'])
    data.push(['ST', '855', '0001'])
    data.push(['BAK', '00', 'AC', orderX12.orderDetail.poNumber, moment().format('YYYYMMDD')])

    /** items */
    for(let item of orderX12.items) {
        // qty, unitcode, price, pricecode, idqual, id
        data.push(item.raw.split('*'))
        //data.push(['CTP', '', 'SLP', '9.95', '103', 'EA', 'DIS', '.44'])
        let shipBy = momentBiz.addWeekDays(moment(), 5).format('YYYYMMDD') //moment().add(5, 'days').format('YYYYMMDD')
        // accept/reject code
        let statusCode = metaData.rejected.indexOf(item.productId) > -1 ? 'R2' : 'IA'
        data.push(['ACK', statusCode, item.quantity, item.unitOfMeasure, '068', shipBy])
        //data.push(['DTM', '067', '20141020'])
    }

    /** closing segments */
    let totalQty = orderX12.items.reduce((t, i) => t + parseInt(i.quantity), 0)
    data.push(['CTT', orderX12.items.length, totalQty])
    data.push(['SE', data.length - 1, orderX12.header.transactionSet.controlNumber])
    data.push(['GE', '1', orderX12.header.functionalGroup.controlNumber])
    data.push(['IEA', '1', orderX12.header.interchangeControl.controlNumber])

    //console.log(data)
    let doc = ''
    for(let segment of data) {
        doc += segment.join('*')
        doc += '~'
    }
    //console.log(data)
    console.log('doc:', doc)
    //process.exit()

    let fname = `PR_${orderX12.orderDetail.poNumber}`;
    let localpath = `${PROVDIR}/ack/${fname}`

    conn.privateKey = fs.readFileSync(conn.privateKey)
    //console.log(cb)
    fs.writeFile(localpath, doc, err => {
        if(err) {
            console.log(err);
            cb(err, data);
        } else {
            console.log('xml created'.green)
            if(test) cb(err, data)
            else {
                let sftp = new sftpClient()
                sftp.on('end', () => {
                    cb(err, data)
                })
                sftp.connect(conn).then(() => {
                    sftp.put(localpath, `${conn.dir}/${fname}`).then(() => {
                        console.log(`\tfile uploaded`)
                        sftp.end()
                    })
                }).catch( err => cb(err, data) )
            }
        }
    })
}

const acknowledgeXml = (orderData, metaData, conn, cb) => {
    if(noack) {
        cb()
        return
    }

    process.stdout.write('gen acknowledgement...')

    let order = metaData.parsed,
        data
    //console.log(order)
    let orderdate = order.Header[0].OrderHeader[0].PurchaseOrderDate

    // transform original order object to create ack object
    let copy = JSON.parse(JSON.stringify(order))

    //console.log(order.Header[0].QuantityTotals)

    delete copy.Header[0].OrderHeader[0].PrimaryPOTypeCode
    delete copy.Header[0].OrderHeader[0].Division
    delete copy.Header[0].PaymentTerms
    delete copy.Header[0].Contacts
    for(let i in copy.Header[0].Address) {
        if(copy.Header[0].Address[i].AddressTypeCode[0] != 'ST')
            delete copy.Header[0].Address[i]
    }
    delete copy.Header[0].FOBRelatedInstruction
    delete copy.Header[0].CarrierInformation
    delete copy.Header[0].Notes
    delete copy.Header[0].ChargesAllowances
    delete copy.Header[0].QuantityTotals

    //console.log(order.Header[0].QuantityTotals)

    copy.Header[0].OrderHeader[0].AcknowledgementType = 'AK'
    copy.Header[0].OrderHeader[0].AcknowledgementDate = moment().format("YYYY-MM-DD")

    for(let item of copy.LineItem) {
        delete item.PhysicalDetails
        //delete item.OrderLine[0].ProductID
        delete item.OrderLine[0].NRFStandardColorAndSize


        item.LineItemAcknowledgement = {
            ItemStatusCode: 'IA',
            ItemScheduleQty: item.OrderLine[0].OrderQty,
            ItemScheduleUOM: item.OrderLine[0].ItemScheduleUOM,
            ItemScheduleQualifier: 'EA',
            ItemScheduleDate: orderdate
        }
    }

    let obj = {
        OrderAcks: {
            '$': {xmlns: 'http://www.spscommerce.com/RSX'},
            OrderAck: copy
        }
    }

    let builder = new xml2js.Builder()
    let xml = builder.buildObject(obj)
    let fname = `PR_${copy.Header[0].OrderHeader[0].PurchaseOrderNumber}.xml`;
    let localpath = `${PROVDIR}/ack/${fname}`
    fs.writeFile(localpath, xml, err => {
        if(err) { console.log(err); cb(err, data); }
        else {
            console.log('xml created'.green)
            if(test) cb(err, data)
            else {
                let sftp = new sftpClient()
                sftp.on('end', () => {
                    cb(err, data)
                })
                sftp.connect(conn).then(() => {
                    sftp.put(localpath, `${PUSHDIR}/${fname}`).then(() => {
                        console.log(`\tfile uploaded`)
                        sftp.end()
                    })
                }).catch( err => cb(err, data) )
            }
        }
    })
}

// todo pass in items
const shipX12 = (parsed, conn, cb) => {
    process.stdout.write('generate ASN...')
    //console.log(util.inspect(parsed, false, null, true))

    let senderID = parsed.header.interchangeControl.senderId //; console.log(senderID)
    let site = CONFIGFILE.sites[senderID]
    let x12segments = []
    let time = moment().format('HHmm')
    let date = moment().format('YYYYMMDD')

    /** opening segments */
    x12segments.push(['ISA',
        '00',
        '          ',
        '00',
        '          ',
        'ZZ',
        'VICTORY        ',
        'ZZ',
        pad(senderID, 15),
        moment().format('YYMMDD'),
        time,
        'U',
        parsed.header.interchangeControl.controlVersionNumber,
        parsed.header.interchangeControl.controlNumber,
        '0', 'T', '>'])
    x12segments.push(['GS', 'PR', 'VICTORY', senderID, date, time, '1', 'X', '004010'])
    x12segments.push(['ST', '856', '0001'])

    let doc = ''
    async.waterfall([
        (cb) => {
            /**
             * get shipped items
             */
            process.stdout.write(`get shipped items...`)
            vtdb.getShippedByPO(site, parsed.orderDetail.poNumber, (err, items) => {
                console.log(items)
                if(items.length == 0) cb('no items to ship')
                else cb(err, items)
            })
        }, (items, cb) => {
            let shipId = items[0].order_id
            let totalWeight = items.reduce((c, i) => c + parseInt(i.weight), 0)
            let totalQty = items.reduce((c, i) => c + i.quantity_shipped, 0)

            x12segments.push(['BSN', '00', shipId, date, time, '0001']) // Shipment, Order, Packaging, Item

            let i = 1
            /** shipment */
            x12segments.push(['HL', i, '', 'S'])
            i++
            x12segments.push(['TD1', 'CTN', parsed.orderDetail.totals.totalQuantity, '', '', '', 'G', totalWeight, 'LB'])
            x12segments.push(['TD5', '', '2', 'RPSI'])
            x12segments.push(['REF', 'BM', shipId])
            x12segments.push(['REF', 'CN', items[0].tracking_num])
            x12segments.push(['DTM', '011', moment(items[0].date_added).format('YYYYMMDD'), moment(items[0].date_added).format('HHmm'), 'GM'])
            x12segments.push(['DTM', '017', momentBiz.addWeekDays(moment(items[0].date_added), 3).format('YYYYMMDD')])
            x12segments.push(['FOB', 'PO'])
            x12segments.push(['N1', 'SF', 'VICTORY', '15', '1234']) // todo: real ship from
            x12segments.push(['N4', 'Orlando', 'FL', '32714', 'US'])
            let stLoc = parsed.names.main[0]
            x12segments.push(['N1', 'ST', 'AMAZON', '15', stLoc.idCode]) // todo: real ship to
            //x12segments.push(['N4', 'Orlando', 'FL', '32714', 'US'])

            /** order */
            x12segments.push(['HL', i, '1', 'O'])
            i++
            x12segments.push(['PRF', parsed.orderDetail.poNumber])

            /** packaging */
            x12segments.push(['HL', i, '2', 'P'])
            i++
            x12segments.push(['TD1', 'CTN', totalQty, '', '', '', '', '', '', '', '', ''])
            x12segments.push(['REF', 'CN', items[0].tracking_num])
            /** SSCC barcode (20 digits): [application identifier: 00] [extension digit: 0] [gs1#: 8600440001] [UPC: we create] [check digit] */
            let sscc = checkdigit.mod10.apply(`0008600440001${items[0].tracking_num.toString().substr(-6)}`)
            //x12segments.push(['GM', sscc])

            /** items */
            let j = 1
            let docItems = parsed.items.map(c => parseInt(c.productId))
            //console.log(docItems); //process.exit();
            for (let item of items) {
                //console.log('check item', item.product_id)
                if(docItems.indexOf(item.product_id) == -1) {
                    console.log('item not in original doc:'.blue, item.product_id)
                    continue
                }

                x12segments.push(['HL', i, '3', 'I']);
                i++
                x12segments.push(['LIN', j, 'VN', item.product_id.toString()])
                x12segments.push(['SN1', j, item.quantity_shipped, 'EA'])
                j++
            }

            /** closing segments */
            x12segments.push(['CTT', i, totalQty])
            x12segments.push(['SE', x12segments.length - 1, parsed.header.transactionSet.controlNumber])
            x12segments.push(['GE', '1', parsed.header.functionalGroup.controlNumber])
            x12segments.push(['IEA', '1', parsed.header.interchangeControl.controlNumber])

            for (let segment of x12segments) {
                doc += segment.join('*')
                doc += '~'
            }

            cb()
        }
    ], (err) => {
        cb(err, doc, parsed)
    })
}

const shipSps = (items, parsed, config, cb) => {
    process.stdout.write('generate ASN...')

    async.waterfall([
        (step) => {
            /**
             * build xml shell
             */
            var shipID = items[0].order_id
            var shipdate = moment(items[0].date_added).format('YYYY-MM-DD')
            var shiptime = moment(items[0].date_added).format('HH:mm:ss')
            var weight = items.reduce((total, i) => {
                return total + i.weight
            }, 0)
            var quantity = items.reduce((total, i) => {
                return total + (i.quantity_shipped || i.quantity)
            }, 0)

            var carrier = 'Unknown'
            var CarrierTransMethodCode = 'M'
            var CarrierAlphaCode = 'FDEG'
            var shipment = items[0].shipment_json ? JSON.parse(items[0].shipment_json).selected_rate : false
            if (shipment) {
                carrier = shipment.carrier
                // LT freight, M ground, A air, 7 "mail"
                CarrierTransMethodCode = shipment.service.indexOf('freight') > -1 ? 'LT' : 'M'
                /**
                 FDCC	FedEx Custom Critical
                 FDEG	FEDEX GROUND
                 FDEN	FEDEX (AIR)
                 FXFE	FedEx Freight
                 FLJF	FLT LOGISTICS LLC
                 FTNA	Fortune Transportation
                 FXFE	FedEx LTL Freight East
                 FXFW	FedEx LTL Freight West (formerly VIKN - Viking)
                 FXNL	FedEx Freight National (formerly Watkins)
                 */
                CarrierAlphaCode = shipment.service.indexOf('freight') > -1 ? 'FXFE' : 'FDEG'
            }

            var QuantityAndWeight = [{
                PackingMedium: 'CTN', // Carton
                LadingQuantity: quantity,
                Weight: weight,
                WeightUOM: 'LB'
            }]

            var obj = {
                Shipments: {
                    '$': {xmlns: 'http://www.spscommerce.com/RSX'},
                    Shipment: [{
                        Header: [{
                            ShipmentHeader: [{
                                TradingPartnerId: parsed.header.interchangeControl.senderId,
                                ShipmentIdentification: [shipID],
                                ShipDate: [shipdate],
                                TsetPurposeCode: ['00'], // original
                                ShipNoticeDate: [moment().format("YYYY-MM-DD"),],
                                ShipNoticeTime: [moment().format("HH:mm:ss"),],
                                ASNStructureCode: ['0001'],
                                BillOfLadingNumber: [shipID],
                                CarrierProNumber: [items[0].tracking_num], // tracking
                            }],
                            Dates: [{
                                DateTimeQualifier: ['011'], // actual ship
                                Date: [shipdate],
                                Time: [shiptime],
                            }],
                            Address: [
                                { // ship from
                                    AddressTypeCode: ['SF'],
                                    LocationCodeQualifier: 15,
                                    AddressLocationNumber: '01',
                                    AddressName: ['Landstreet'],
                                    Address1: ['2437 E. Landstreet Road'],
                                    Address2: [''],
                                    City: ['Orlando'],
                                    State: ['FL'],
                                    PostalCode: ['32824'],
                                    Country: ['USA'],
                                }, { // ship to
                                    AddressTypeCode: ['ST'],
                                    LocationCodeQualifier: 15,
                                    AddressLocationNumber: '01',
                                    AddressName: parsed.names.main[0].raw.AddressName[0],
                                    Address1: [items[0].shipping_address1],
                                    Address2: [items[0].shipping_address2],
                                    City: [items[0].shipping_city],
                                    State: [items[0].shipping_state],
                                    PostalCode: [items[0].shipping_zip],
                                    Country: ['USA'],
                                }
                            ],
                            CarrierInformation: [{
                                CarrierTransMethodCode: CarrierTransMethodCode,
                                CarrierAlphaCode: CarrierAlphaCode,
                                CarrierRouting: carrier,
                                ServiceLevelCodes: [{ServiceLevelCode: 'G2'}] // G2 standard service, ND next day air, SC second day air,
                            }],
                            QuantityAndWeight: QuantityAndWeight,
                        }], OrderLevel: [{
                            OrderHeader: [{
                                TradingPartnerId: parsed.header.interchangeControl.senderId,
                                PurchaseOrderNumber: [parsed.orderDetail.poNumber],
                                TsetPurposeCode: ['00'],
                                PrimaryPOTypeCodeDS: [parsed.orderDetail.typeCode],
                                PurchaseOrderDate: [moment(parsed.orderDetail.date).format("YYYY-MM-DD")],
                                Vendor: config.vendorId,
                                CustomerOrderNumber: parsed.customerOrderNumber
                            }],
                            QuantityAndWeight: QuantityAndWeight,
                            PackLevel: [],
                        }]
                    }]
                }
            }

            // for Beall's
            if (items[0].site == 28)
                obj.Shipments.Shipment[0].Header[0].CarrierInformation[0].StatusCode = ['CC']

            // BBB
            if (items[0].site == 33 || items[0].site == 10005) {
                obj.Shipments.Shipment[0].OrderLevel[0].Address = [{
                    AddressTypeCode: 'BY',
                    LocationCodeQualifier: 15,
                    AddressLocationNumber: '01',
                }]
            }

            var tracking = {}
            for (var item of items) {
                if (!(item.tracking_num in tracking))
                    tracking[item.tracking_num] = []
                tracking[item.tracking_num].push(item)
            }
            step(null, obj, parsed, tracking)
        }, (obj, parsed, tracking, step) => {
            /**
             * add items for tracking num
             */
            async.eachOfSeries(tracking, (items, tracking_num, shipStep) => {
                console.log(`\n${items.length} items for ${tracking_num}...`)

                /** SSCC barcode (20 digits): [application identifier: 00] [extension digit: 0] [gs1#: 8600440001] [UPC: we create] [check digit] */
                var sscc = checkdigit.mod10.apply(`0008600440001${tracking_num.toString().substr(-6)}`)
                //if(test) console.log('sscc:', sscc)

                var pack = {
                    Pack: [{
                        PackLevelType: 'P',
                        ShippingSerialID: sscc,
                        CarrierPackageID: [tracking_num],
                    }],
                    References: [{
                        ReferenceQual: ['2I'], // tracking num
                        ReferenceID: [tracking_num],
                        Description: ['Tracking number'],
                    }],
                    ItemLevel: []
                }
                var split = null
                //console.log(parsed.items)
                async.eachSeries(items, (item, nextItem) => {
                    console.log('item:', item.product_id)
                    async.waterfall([
                        (cb) => {
                            /**
                             * convert 4 sets back to 8
                             */
                            if([81, 10092].indexOf(item.product_type) == -1) {
                                cb()
                            } else {
                                process.stdout.write(`\trevert 4 bag set...`)
                                var q = connection.query(`select p.id, p.upc_old from bundled_products b inner join products p on p.id=b.input_product_id where output_product_id = ? limit 1;`, [item.product_id], (err, rows) => {
                                    if (err || rows.length == 0) {
                                        console.log('product not found'.red)
                                        cb()
                                    } else if(split == rows[0].id) {
                                        // second bag of split, reset flag and skip insert
                                        console.log(`second bag, skip:`.white, item.product_id)
                                        split = null
                                        cb('split')
                                    } else {
                                        split = rows[0].id
                                        console.log(`converted 4 bag set:`.white, item.product_id, '=>', rows[0].id)
                                        item.product_id = rows[0].id
                                        item.upc = rows[0].upc_old
                                        obj.Shipments.Shipment[0].Header[0].QuantityAndWeight[0].LadingQuantity -= 1
                                        //console.log(obj.Shipments.Shipment[0].Header[0].QuantityAndWeight[0].LadingQuantity)
                                        cb()
                                    }
                                })
                            }
                        }, (cb) => {
                            /**
                             * get buyer part number and line sequence number from 850 doc
                             */
                            let match = parsed.items.find((e) => {
                                let compare = e.productIdQualifier == 'VN' ?
                                    item.product_id :
                                    e.productIdQualifier == 'UP' ?
                                        item.upc :
                                        null
                                //console.log('check:', e.productId, compare, SKUMAP[item.product_id])
                                return e.productId == compare || e.productId == SKUMAP[item.product_id]
                            })
                            if(match) {
                                //console.log('match:', match.LineSequenceNumber)
                                pack.ItemLevel.push({
                                    ShipmentLine: [{
                                        ConsumerPackageCode: [match.ConsumerPackageCode],
                                        LineSequenceNumber: match.LineSequenceNumber,
                                        BuyerPartNumber: match.BuyerPartNumber,
                                        ShipQty: item.quantity_shipped,
                                        ShipQtyUOM: 'EA',
                                    }], ProductOrItemDescription: [{
                                        ProductCharacteristicCode: '08',
                                        ProductDescription: match.ProductDescription || 'No description',
                                    }]
                                })
                            } else {
                                console.log('item not in orig doc, skip'.yellow)
                            }

                            cb()
                        }
                    ], (err) => {
                        if(err == 'split') nextItem() // not real error, just continue
                        else nextItem(err)
                    })
                }, (err) => {
                    if(pack.ItemLevel.length == 0) {
                            console.log(`no items`)
                            shipStep()
                    } else {
                        obj.Shipments.Shipment[0].OrderLevel[0].PackLevel.push(pack)
                        shipStep(err)
                    }
                })
            }, (err) => {
                step(err, obj, parsed)
            })
        }, (obj, parsed, step) => {
            /**
             * create xml
             */
            var builder = new xml2js.Builder()
            var xml = builder.buildObject(obj)
            var fname = `SH_${parsed.orderDetail.poNumber}.xml`
            let localpath = `${PROVDIR}/shipment/${fname}`
            //if(test) console.log(localpath.cyan)

            fs.writeFile(localpath, xml, err => {
                step(err, xml, localpath)
            })
        }
    ], cb)
}

const invoiceX12 = (parsed, conn, cb) => {
    process.stdout.write('generate ASN...')
    //console.log(util.inspect(parsed, false, null, true))

    let senderID = parsed.header.interchangeControl.senderId //; console.log(senderID)
    let site = CONFIGFILE.sites[senderID]
    let x12segments = []
    let time = moment().format('HHmm')
    let date = moment().format('YYYYMMDD')

    /** opening segments */
    x12segments.push(['ISA',
        '00',
        '          ',
        '00',
        '          ',
        'ZZ',
        'VICTORY        ',
        'ZZ',
        pad(senderID, 15),
        moment().format('YYMMDD'),
        time,
        'U',
        parsed.header.interchangeControl.controlVersionNumber,
        parsed.header.interchangeControl.controlNumber,
        '0', 'T', '>'])
    x12segments.push(['GS', 'PR', 'VICTORY', senderID, date, time, '1', 'X', '004010'])
    x12segments.push(['ST', '856', '0001'])


    let doc = ''
    async.waterfall([
        (cb) => {
            /**
             * get shipped items
             */
            process.stdout.write(`get shipped items...`)
            vtdb.getShippedByPO(site, parsed.orderDetail.poNumber, (err, items) => {
                console.log(items)
                if(items.length == 0) cb('no items to ship')
                else cb(err, items)
            })
        }, (items, cb) => {
            let shipId = items[0].order_id
            let totalWeight = items.reduce((c, i) => c + parseInt(i.weight), 0)
            let totalQty = items.reduce((c, i) => c + i.quantity_shipped, 0)

            x12segments.push(['BSN', '00', shipId, date, time, '0001']) // Shipment, Order, Packaging, Item

            let i = 1
            /** shipment */
            x12segments.push(['HL', i, '', 'S'])
            i++
            x12segments.push(['TD1', 'CTN', parsed.orderDetail.totals.totalQuantity, '', '', '', 'G', totalWeight, 'LB'])
            x12segments.push(['TD5', '', '2', 'RPSI'])
            x12segments.push(['REF', 'BM', shipId])
            x12segments.push(['REF', 'CN', items[0].tracking_num])
            x12segments.push(['DTM', '011', moment(items[0].date_added).format('YYYYMMDD'), moment(items[0].date_added).format('HHmm'), 'GM'])
            x12segments.push(['DTM', '017', momentBiz.addWeekDays(moment(items[0].date_added), 3).format('YYYYMMDD')])
            x12segments.push(['FOB', 'PO'])
            x12segments.push(['N1', 'SF', 'VICTORY', '15', '1234']) // todo: real ship from
            x12segments.push(['N4', 'Orlando', 'FL', '32714', 'US'])
            let stLoc = parsed.names.main[0]
            x12segments.push(['N1', 'ST', 'AMAZON', '15', stLoc.idCode]) // todo: real ship to
            //x12segments.push(['N4', 'Orlando', 'FL', '32714', 'US'])

            /** order */
            x12segments.push(['HL', i, '1', 'O'])
            i++
            x12segments.push(['PRF', parsed.orderDetail.poNumber])

            /** packaging */
            x12segments.push(['HL', i, '2', 'P'])
            i++
            x12segments.push(['TD1', 'CTN', totalQty, '', '', '', '', '', '', '', '', ''])
            x12segments.push(['REF', 'CN', items[0].tracking_num])
            /** SSCC barcode (20 digits): [application identifier: 00] [extension digit: 0] [gs1#: 8600440001] [UPC: we create] [check digit] */
            let sscc = checkdigit.mod10.apply(`0008600440001${items[0].tracking_num.toString().substr(-6)}`)
            //x12segments.push(['GM', sscc])

            /** items */
            let j = 1
            let docItems = parsed.items.map(c => parseInt(c.productId))
            //console.log(docItems); //process.exit();
            for (let item of items) {
                //console.log('check item', item.product_id)
                if(docItems.indexOf(item.product_id) == -1) {
                    console.log('item not in original doc:'.blue, item.product_id)
                    continue
                }

                x12segments.push(['HL', i, '3', 'I']);
                i++
                x12segments.push(['LIN', j, 'VN', item.product_id.toString()])
                x12segments.push(['SN1', j, item.quantity_shipped, 'EA'])
                j++
            }

            /** closing segments */
            x12segments.push(['CTT', i, totalQty])
            x12segments.push(['SE', x12segments.length - 1, parsed.header.transactionSet.controlNumber])
            x12segments.push(['GE', '1', parsed.header.functionalGroup.controlNumber])
            x12segments.push(['IEA', '1', parsed.header.interchangeControl.controlNumber])

            for (let segment of x12segments) {
                //console.log('d', doc)
                //console.log(segment)
                doc += segment.join('*')
                doc += '~'
            }
            console.log(x12segments)
            //console.log(doc)
            cb()
            //process.exit()
        }
    ], (err) => {
        cb(err, doc, parsed)
    })
}

const invoiceSps = (order, parsed, config, cb) => {
    process.stdout.write('gen invoice...')
    //console.log(util.inspect(parsed, false, null, true))

    let ponum = parsed.orderDetail.poNumber
    let copy = parsed.raw.Order

    // remove stuff we dont need from order header
    delete copy.Header[0].Contacts
    //delete copy.Header[0].Address[0]
    delete copy.Header[0].FOBRelatedInstruction
    delete copy.Header[0].Notes
    delete copy.Header[0].ChargesAllowances

    delete copy.Header[0].OrderHeader[0].PrimaryPOTypeCode
    delete copy.Header[0].OrderHeader[0].ShipCompleteCode
    delete copy.Header[0].OrderHeader[0].Division
    delete copy.Header[0].OrderHeader[0].CustomerOrderNumber

    //copy.Header[0].PaymentTerms = [{ TermsDescription: 'Net30' }]

    if(!('QuantityTotals' in copy.Header[0])) {
        copy.Header[0].QuantityTotals = [{
            Quantity: parsed.orderDetail.totals.totalQuantity,
            QuantityUOM: 'EA',
            QuantityTotalsQualifier: 'SQT',
        }]
    }

    // use order header to make invoice header then delete
    copy.Header[0].InvoiceHeader = [Object.assign({
        InvoiceNumber: order.order_id,
        InvoiceDate: moment().format("YYYY-MM-DD"),
        InvoiceTypeCode: 'DR', // debit
        BuyersCurrency: 'USD',
        SellersCurrency: 'USD',
        CustomerOrderNumber: order.order_id,
        //BillOfLadingNumber: [],
        //CarrierProNumber: [],
        ShipDate: moment().format("YYYY-MM-DD"),
    }, copy.Header[0].OrderHeader[0])]
    delete copy.Header[0].OrderHeader
    
    if(!(copy.Header[0].Address.find(e => e.AddressTypeCode[0] == 'VN'))) {
        copy.Header[0].Address.push({
            AddressTypeCode: 'VN',
            AddressName: 'Victory Tailgate LLC',
            Address1: '2437 East Landstreet Rd',
            City: 'Orlando',
            State: 'FL',
            PostalCode: '32824',
            Country: 'USA'
        })
    }

    // rename node orderline to invoiceline
    async.forEachSeries(copy.LineItem, (item, nextItem) => {
        if('ProductID' in item.OrderLine[0] && !('PartNumberQual' in item.OrderLine[0].ProductID[0])) {
            item.OrderLine[0].ProductID[0].PartNumberQual = ['PL']
        }

        item.InvoiceLine = [Object.assign({
            ShipQty: item.OrderLine[0].OrderQty
        }, item.OrderLine[0])]

        delete item.OrderLine

        nextItem(null)
    }, (err) => {
        if(err) console.log(err)

        // Total merchandise amount. Sum of price * quantity
        copy.Summary[0].TotalSalesAmount = parsed.orderDetail.totals.totalSales || order.subtotal

        // Total amount of the transaction. Sum of the item qty * price +/­ the charges, allowances, and taxes, if sent.
        copy.Summary[0].TotalAmount = parsed.orderDetail.totals.grandTotal || order.total

        let obj = {
            Invoices: {
                '$': {xmlns: 'http://www.spscommerce.com/RSX'},
                Invoice: copy
            }
        }

        let builder = new xml2js.Builder()
        let xml = builder.buildObject(obj)
        let fname = `IN_${ponum}.xml`;
        let localpath = `${PROVDIR}/invoice/${fname}`
        fs.writeFile(localpath, xml, err => {
            if(err) console.log(err.message.red)
            else cb(err, xml, localpath)

        })
    })
}

const insertOrder = (order, metaData, callback) => {
    process.stdout.write('insert order...')
    //console.log('metadata:', metaData)
    //process.exit()

    let vto // "total sales amount" for invoice

    async.waterfall([
        (insertStep) => {
            /**
             * TradingPartnerId:
             *  fanatics: CDKALLVICTORYTA
             *  bealls: DY7ALLVICTORYTA
             */
            vtdb.query('select ds_carrier, ds_account, ds_zip from marketplaces where site_id = ?', [metaData.site], (err, results) => {
                insertStep(err, results ? results[0] : {})
            })
        }, (dsinfo, insertStep) => {
            order.push(dsinfo.ds_carrier || '')
            order.push(dsinfo.ds_account || '')
            order.push(dsinfo.ds_zip || '')
            if(test) {
                let result = {}
                result.insertId = 987654;
                console.log(`test insert: ${result.insertId}`.green)
                insertStep(null, result)
            } else vtdb.insertOrder(order, insertStep)
        }, (result, insertStep) => {
            metaData.vto = result.insertId
            console.log(`vto: ${metaData.vto}`.green)
            insertStep()
        }
    ], (err) => {
        if(err) {
            console.log('insert error'.red, order)
            if ('message' in err) console.log(err.message.red)
        }
        callback(err, metaData)
    })
}

const insertItem = (item, metaData, callback) => {
    async.waterfall([
        (itemstep) => {
            /**
             * SPLIT 8 BAGS INTO 4
             */
            console.log('search sku', item.sku)
            vtdb.query(`select id, product_type from products where id = ? or upc = ? limit 1;`, [item.sku, item.upc], (err, result) => {
                if(err || result.length == 0) {
                    let msg = `item not found: ${item.sku} / ${item.upc}`
                    sendAlert(msg, () => {
                        itemstep(msg)
                    })
                } else {
                    console.log(result)
                    let skus = [result[0].id]
                    if (result.length > 0 && [78, 79, 83].indexOf(result[0].product_type) > -1) {
                        // find 4 bags
                        vtdb.query(`select b.output_product_id from bundled_products b inner join products p on p.id=b.output_product_id where input_product_id = ? and p.active='y'`, [result[0].id], (err, rows) => {
                            if (err || rows.length < 2) itemstep(err, skus)
                            else {
                                skus = rows.map(x => x.output_product_id)
                                console.log('splitting 8 bag set:'.white, sku, '=>', JSON.stringify(skus))
                                itemstep(err, skus)
                            }
                        })
                    } else itemstep(err, skus)
                }
            })
        }, (skus, itemstep) => {
            /**
             * get slots and ptype
             */
             vtdb.getItemInfo(skus, itemstep)
        }, (prods, itemstep) => {
            /**
             * build query
             */
            let bagsInOrder = false
            async.eachSeries(prods, (prod, nextProd) => {
                console.log('insert item:'.yellow, prod.name)
                let pid = prod.id || 0
                let pname = prod.name
                let ptype = prod.product_type
                if(!bagsInOrder && bagTypes.indexOf(ptype) > -1) {
                    console.log('bags found'.blue)
                    bagsInOrder = true
                }
                let slot_data = (prod.slots && !test) ? prod.slots.split(',') : []

                let info = ''

                /**
                 * SPLIT QUANTITY
                 */
                let loop = [...Array(item.qty).keys()]
                async.eachSeries(loop, (value, nextqty) => {
                    process.stdout.write(`\tinserting item...`)
                    let slot = slot_data.shift() || []
                    let slot_num = slot[1] || ''
                    //let item_query = `insert into ordered_items (order_id, product_id, name, style, price, date_added, updated_date, quantity, slot_nums, category, refund_amt, item_status, cwg_customer_price, updated_by, product_type, info, tax, discount) values ?`
                    let item_vals = [[
                        metaData.vto,
                        pid,
                        pname, //item.ProductOrItemDescription[0].ProductDescription[0],
                        '', // style
                        item.price / prods.length,
                        moment().format('YYYY-MM-DD HH:mm:ss'),
                        moment().format('YYYY-MM-DD HH:mm:ss'),
                        1, // qty
                        slot_num,
                        0,
                        0, //item.amount_refunded / qty, // refund
                        0, // status
                        0, // cwg
                        0, // updated by
                        ptype, // ptype
                        info,
                        item.tax,
                        item.discount
                    ]]

                    async.waterfall([
                        (step) => {
                            /**
                             * ADDONS
                             */
                            let addons = []

                            /**
                             * TOWER CC
                             */
                            if([10091, 266].indexOf(ptype) > -1) {
                                if (prod.team_id > 0) {
                                    // check for team bag
                                    vtdb.query(`select id, name, product_type from products where team_id = ? and product_type = 269 and active = 'y'`, [prod.team_id], (err, result) => {
                                        if(err)
                                            console.log(err)
                                        if(result)
                                            addons.push(result[0])
                                        step(null, addons)
                                    })
                                } else {
                                    addons.push({id:224353, name:'Team Tumble Tower Carrying Case', product_type: 269})
                                    step(null, addons)
                                }

                                /**
                                 * GAMEDAY TOWER CC
                                 */
                            } else if ([10014].indexOf(ptype) > -1) {
                                addons.push({id: 1222675, name: 'Black Gameday Tower Carrying Case', product_type: 10055})
                                step(null, addons)

                                /**
                                 * BAGS FOR BOARD
                                 */
                            } else if (!bagsInOrder && boardTypes.indexOf(ptype) > -1) {
                                vtdb.query(`select p.id, p.name, p.product_type from selected_products_addons a inner join products p on p.id=a.selected_addon_id where a.marketplace_id = 38 and p.product_type = 82 and a.product_id = ?`, [pid], (err, result) => {
                                    if(err)
                                        console.log(err)
                                    if(result.length > 0) {
                                        console.log("BMBs".blue)
                                        for(prod of result) {
                                            addons.push(prod)
                                        }
                                    } else {
                                        info += 'Bags: 8 corn-filled best matching colors<br>Plus any additional bags listed with this order.'
                                    }

                                    step(null, addons)
                                })
                            } else step(null, addons)
                        }, (addons, step) => {
                            for(addon of addons) {
                                console.log(`addon: ${addon.name}`.blue)
                                item_vals.push([
                                    metaData.vto,
                                    addon.id,
                                    addon.name,
                                    '', // style
                                    0,
                                    moment().format('YYYY-MM-DD HH:mm:ss'),
                                    moment().format('YYYY-MM-DD HH:mm:ss'),
                                    1, // qty
                                    '',
                                    0,
                                    0, //item.amount_refunded / qty, // refund
                                    0, // status
                                    0, // cwg
                                    0, // updated by
                                    addon.product_type, // ptype
                                    '',
                                    0,
                                    0
                                ])
                            }

                            if(test) {
                                let result = {}
                                result.insertId = 1234;
                                console.log(`test insert: ${result.insertId}`.green)
                                step()
                            } else {
                                vtdb.insertItems([item_vals], (err, result) => {
                                    /*if (err) console.log(`tried ${q.sql}`)
                                    else*/ if (result.insertId > 0) {
                                        console.log(`${result.insertId}`.green)
                                    }
                                    step(err)
                                })
                            }
                        }
                    ], nextqty)
                }, nextProd)
            }, itemstep)
        }
    ], (err) => {
        if(err) console.log(err.red)
        //console.log('item ins done')
        callback()
    })
}

const shipAction = (config, callback) => {
    //console.log(config.format); process.exit();
    let readDir =  `${PROVDIR}/orders/processed` // todo: make param in func
    let formatCamel = config['format'].charAt(0).toUpperCase() + config['format'].slice(1);
    async.waterfall([
        //(cb) => vtdb.getShippedByPO(site, ponum, (err, rows) => {
        (cb) => {
            console.log('get recently shipped...');
            vtdb.getRecentlyShipped(config.siteId, (err, rows) => {
                /** group orders */
                let orderGroup = {}
                for (let item of rows) {
                    if (!(item.customer_po in orderGroup))
                        orderGroup[item.customer_po] = []
                    orderGroup[item.customer_po].push(item)
                }
                cb(null, orderGroup)
            })
        }, (orders, cb) => {
            console.log(`${Object.keys(orders).length} orders found`)
            async.forEachOfSeries(orders, (order, ponum, next) => {
                console.log('checking', ponum, '...')
                let origDoc
                async.waterfall([
                    (cb) => fs.readdir(readDir, {}, cb),
                    (files, cb) => {
                        let toProcess = files.filter(file => file.indexOf(config.prefix) > -1)
                        async.eachSeries(toProcess, (file, nextFile) => {
                            async.waterfall([
                                (cb) => FORMATS[config['format']].parse(`${ORDERDIR}/processed/${file}`, cb),
                                (parsed, cb) => {
                                    if(parsed && parsed.orderDetail.poNumber == ponum) {
                                        console.log(`match found: ${file}`.blue)
                                        origDoc = file
                                        cb(parsed)
                                    } else cb()
                                }
                            ], nextFile)
                        }, (err) => {
                            if(!err || 'errno' in err) {
                                console.log(`\nno match found`.red)
                                cb('01')
                            } else {
                                cb(null, err)
                            }
                        })
                    }, (parsed, cb) => {
                        ACTIONS['ship'+formatCamel](order, parsed, config, (err, doc, localpath) => {
                            cb(err, doc, localpath)
                        })
                    }, (doc, localpath, cb) => {
                        let fname = localpath.split('/')[localpath.split('/').length - 1];
                        if(config.put.privateKey)
                            config.put.privateKey = fs.readFileSync(config.put.privateKey)
                        fs.writeFile(localpath, doc, err => {
                            console.log('xml created'.green, localpath)
                            if (test) cb(err)
                            else {
                                let sftp = new sftpClient()
                                sftp.on('end', () => {
                                    cb(err)
                                })
                                sftp.connect(config.put).then(() => {
                                    sftp.put(localpath, `${config.put.dir}/${fname}`).then(() => {
                                        console.log(`\tfile uploaded`)
                                        sftp.end()
                                    })
                                }).catch(err => cb(err))
                            }
                        })
                    }, (err) => {
                        /** get em outta hea */
                        fs.rename(`${ORDERDIR}/processed/${origDoc}`, `${ORDERDIR}/shipped/${origDoc}`, () => {
                            console.log(`\tfile moved`)
                            cb(err)
                        })
                    }
                ], next)
            }, cb)
        }
    ], callback)
}

const invoiceAction = (config, callback) => {
    let readDir = `${PROVDIR}/orders/shipped`
    let formatCamel = config['format'].charAt(0).toUpperCase() + config['format'].slice(1);
    async.waterfall([
        (cb) => {
            console.log('get recently invoiced...');
            vtdb.getRecentlyInvoiced(config.siteId, (err, rows) => {
                if(err) console.log(err.sqlMessage.red)
                cb(err, rows)
            })
        }, (orders, cb) => {
            console.log(`${Object.keys(orders).length} orders found`)
            async.forEachSeries(orders, (order, next) => {
                let ponum = order.customer_po
                console.log('checking', ponum, '...')
                let origDoc
                async.waterfall([
                    (cb) => fs.readdir(readDir, {}, cb),
                    (files, cb) => {
                        let toProcess = files.filter(file => file.indexOf(config.prefix) > -1)
                        async.eachSeries(toProcess, (file, nextFile) => {
                            async.waterfall([
                                (cb) => FORMATS[config['format']].parse(`${readDir}/${file}`, cb),
                                (parsed, cb) => {
                                    if(parsed && parsed.orderDetail.poNumber == ponum) {
                                        console.log(`match found: ${file}`.blue)
                                        origDoc = file
                                        cb(parsed)
                                    } else cb()
                                }
                            ], nextFile)
                        }, (parsed) => {
                            if(!parsed || 'errno' in parsed) {
                                console.log(`\nno match found`.red)
                                cb('01')
                            } else {
                                cb(null, parsed)
                            }
                        })
                    }, (parsed, cb) => {
                        ACTIONS['invoice'+formatCamel](order, parsed, config, cb)
                    }, (doc, localpath, cb) => {
                        let fname = localpath.split('/')[localpath.split('/').length - 1];
                        if(config.put.privateKey)
                            config.put.privateKey = fs.readFileSync(config.put.privateKey)
                        fs.writeFile(localpath, doc, err => {
                            console.log('xml created'.green, localpath)
                            if (test) cb(err)
                            else {
                                let sftp = new sftpClient()
                                sftp.on('end', () => {
                                    cb(err)
                                })
                                sftp.connect(config.put).then(() => {
                                    sftp.put(localpath, `${config.put.dir}/${fname}`).then(() => {
                                        console.log(`\tfile uploaded`)
                                        sftp.end()
                                    })
                                }).catch(err => cb(err))
                            }
                        })
                    }, (err) => {
                        /** get em outta hea */
                        fs.rename(`${ORDERDIR}/shipped/${origDoc}`, `${ORDERDIR}/invoiced/${origDoc}`, () => {
                            console.log(`\tfile moved`)
                            cb(err)
                        })
                    }
                ], next)
            }, cb)
        }
    ], callback)
}

const sendAlert = (msg, callback) => {
    let body = {
        "message": 'SPS error',
        "description": msg,
        "teams": [{"name": "Web"}],
        "tags": ["SPS"],
        "priority": "P3"
    }
    if(!test) {
        request({
            method: 'POST',
            url: 'https://api.opsgenie.com/v2/alerts',
            headers: {
                'content-type': 'application/json',
                'authorization': 'GenieKey ffcb3c34-e6d0-47c3-b1a6-a78659e530bf'
            },
            body: JSON.stringify(body)
        }, (err) => {
            if (err) console.log(err)
            else console.log('alert sent'.blue)
            callback()
        })
    } else callback()
}

/**
 * MAIN
 */
const FORMATS = {
    'sps': sps,
    'x12': x12
}
const ACTIONS = {
    'shipX12': shipX12,
    'shipSps': shipSps,
    'invoiceSps': invoiceSps,
}

/** run through each account in config file */
console.log(moment().format().blue)
async.forEachOfSeries(CONFIGFILE.accounts, (config, providerName, mainStep) => {
    console.log(` --- ${action.toUpperCase()} ${providerName} ---`.yellow)
    PROVDIR = `${__dirname}/${providerName}`
    ORDERDIR = `${PROVDIR}/orders`
    try { SKUMAP = require(`${PROVDIR}/skumap.json`) } catch(e) { console.log('no sku map'.red) }
    //let site = CONFIGFILE.connections[providerName].siteId

    if(action == 'ship') {
        shipAction(config, mainStep)
    } else if(action == 'invoice') {
        invoiceAction(config, mainStep)
    } else {
        async.waterfall([
            (cb) => downloadOrders(config, cb),
            (cb) => fs.readdir(ORDERDIR, {}, cb),
            (files, cb) => {
                let toProcess = files.filter(file => file.indexOf(config.prefix) > -1)
                console.log(`${toProcess.length} to process`.yellow)
                async.forEachSeries(toProcess, (file, next) => {
                    processOrder(file, config, (err) => {
                        if (err) console.error(err.red)
                        next(one ? 'one only' : false)
                    })
                }, cb)
            }
        ], mainStep)
    }
}, (err) => {
    if(err) console.error(err.red)
    console.log(`done\n`.green)
    process.exit()
})