require('colors').enabled = true

const _ = require('lodash')

const args = require('yargs').argv
const ENV = args.prod ? 'prod' : 'dev'

const config = require(`${__dirname}/../jsConfig.${ENV}.json`)
const DB = new (require(`./classes/db.js`))(config)
const Products = new (require('./classes/products.js'))(config, DB, args)

/** force products (superbowl, etc) **/
let force = [/*9511361,9511348,9511372,9511357,9511365,9511350,9511366,9511351,9511363,9511359,9511371,9511356*/]

console.log(`${new Date().toLocaleString()}`.yellow, `\nRunning inventory (${ENV.toUpperCase().yellow})...`)
DB.executeGql(`{ marketplace_list(active: true) { id name } }`)
    .then(async results => {
        let promises = []
        for(let marketplace of results.marketplace_list) {
            promises.push(await Products.syncInventory(marketplace, force))
        }

        return Promise.all(promises)
    })
    .then(results =>
        results.forEach(message => {
            if(message) console.log(`-> ${message}`)
        })
    )
    .catch(err => console.log('err:', err.red))

