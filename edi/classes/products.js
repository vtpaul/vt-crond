const _ = require('lodash')

module.exports = class Products {
    constructor (config, DB, args = []) {
        this.config = config
        this.DB = DB
        this.args = args
        this.marketplace = {}
    }

    syncInventory (marketplace, force = []) {
        this.marketplace = marketplace

        /** search config for proper channel to push to for this marketplace */
        let connection = _.find(this.config.connections, e => _.includes(e.sites, parseInt(this.marketplace.id)))
        if (!connection) {
            //`no connection set up for site ${this.marketplace.id}`.blue
            return false
        }

        return new Promise(async (resolve, reject) => {
            await this.getSyncableInventory(this.args.limit || connection.productLimit, force)
                .then(async products => {
                    /** the class to do the work is defined in the config */
                    let classname = connection.class
                    let instance = new (require(`./${classname}.js`))(connection, this.marketplace.id, this.args)

                    let sendReport = []
                    sendReport.push(`${products.length} products for ${this.marketplace.name} through ${classname.toUpperCase()}`)

                    await instance.pushInventory(products)
                        .then(result => {
                            sendReport.push(result)
                            return this.args.sync === false ? 'synctime not updated'.blue : this.markInventorySynced(products)
                        })
                        .then(result => {
                            sendReport.push(result)
                            resolve(sendReport.join(' / '))
                        })
                        .catch(reject)
                })
                .catch(resolve)
        })
    }

    async getSyncableInventory (limit = 10, force = [], gql = true) {
        let query, products
        if(gql) {
            query = `{
              product_list(
                marketplace_id_out_of_sync: ${this.marketplace.id},
                sort_ascending: true,
                first: ${limit}
              ) {
                products {
                  id
                  name
                  upc
                  upc_old
                  active
                  product_type {
                    id
                    active
                  }
                  listings {
                    marketplace_sku
                    marketplace {
                      id
                    }
                    active
                  }
                }
              }
            }`;
            products = await this.DB.executeGql(query)
                .then(results => results.product_list.products.map(e => {
                    let listing = e.listings.find(e => e.marketplace.id === this.marketplace.id)
                    /** determine active or not (exception for 8 bag sets) */
                    let active = [79,83,139,78].indexOf(e.product_type.id) > -1 ? !!(listing.active) : !!(listing.active && e.active && e.product_type.active)
                    /** force active **/
                    if (force.indexOf(e.id) > -1) {
                        console.log(`forcing ${e.id} (${e.name})`.magenta)
                        active = true;
                    }

                    return {
                        id: e.id,
                        marketplace_id: listing.marketplace.id,
                        marketplace_sku: listing.marketplace_sku,
                        upc: e.upc || e.upc_old,
                        active: active
                    }
                }))
                .catch(err => err)

        } else {
            query = `
                select
                  pm.product_id as id,
                  pm.marketplace_id,
                  pm.marketplace_sku,
                  if(p.upc is null or p.upc = '', p.upc_old, p.upc) as upc,
                  if(p.active == 'y', true, false) as active,
                  p.product_type
                from products_marketplaces pm
                  inner join products p on p.id = pm.product_id
                where
                  pm.marketplace_id = ${this.marketplace.id}
                  pm.sync_time is null or pm.sync_time < p.updated_date
                limit 0, 10;
            `
            products = await this.DB.executeSql(query).then(results => results)
        }

        /** filters */
        /*let filters = this.args.filters || []
        for(let filter of filters) {
            products = products.filter(e => e[filter.name] === filter.value)
        }*/

        //console.log(products); process.exit();

        return new Promise((resolve, reject) => {
            Array.isArray(products) ?
                resolve(products) :
                reject(`none syncable for ${this.marketplace.name}`)
        })
    }

    async markInventorySynced(products, gql = true) {
        //process.stdout.write(`mark em synced...`)

        let query, success
        if(gql) {
            query = `
                mutation($ids: [Int]!) {
                  products_mark_synced(
                    marketplace_id: ${this.marketplace.id},
                    product_id: $ids
                  )
                }
            `
            let variables = { ids: products.map(e => e.id) }
            success = await this.DB.executeGql(query, variables).then(results => results)
        } else {
            // todo: make sql query
            query = ` `
            success = await this.DB.executeSql(query).then(results => results)
        }

        return new Promise((resolve, reject) => {
            success ? resolve('synctime updated'.green) : reject('error updating products'.red)
        })
    }
}