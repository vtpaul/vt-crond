//const async = require('async')
const mysql = require('mysql')
const conn = mysql.createConnection({
    host     : 'victory-prod-temp2-cluster.cluster-cbtmqjwywgwy.us-east-1.rds.amazonaws.com',
    user     : 'vt-backend',
    password : "gwuERd3C7wLrHCNKthpzcYme3tF9jtLfKbyVmjkU",
    database : "tgsgames_corn2"
})

let funcs = {

    insertOrder: function(order, cb) {
        var sql = `
            insert into orders (
                name,
                email,
                phone,
                subtotal,
                tax,
                discount,
                shipping,
                total,
                date_added,
                paid_date,
                status,
                shipping_full_name,
                shipping_first_name,
                shipping_last_name,
                shipping_address1,
                shipping_address2,
                shipping_city,
                shipping_state,
                shipping_zip,
                site,
                site_order_num,
                customer_po,
                ship_by,
                gift,
                giftmsg,
                comments,
                pack_slip_note,
                dropship_carrier,
                dropship_account,
                dropship_zip
            ) values (?)`
        let q = conn.query(sql, [order], (err, result) => {
            //console.log(q.sql)
            cb(err, result)
        })
    },

    insertItems: function(items, cb) {
        var sql = `
            insert into ordered_items (
                order_id, 
                product_id, 
                name, 
                style, 
                price, 
                date_added, 
                updated_date, 
                quantity, 
                slot_nums, 
                category, 
                refund_amt, 
                item_status, 
                cwg_customer_price, 
                updated_by, 
                product_type, 
                info, 
                tax, 
                discount
            ) values ?`
        let q = conn.query(sql, [items], (err, result) => {
            //console.log(q.sql)
            cb(err, result)
        })
    },

    getItemInfo: function(skus, cb) {
        var sql = `
            select
                p.id,
                p.name,
                p.product_type,
                p.team_id,
                group_concat(concat_ws('/', s.slot_id, s.slot_num), ',') as slots
            from products p
                left join products_slots s on s.product_id = p.id and s.quantity > 0
            where p.id IN (?)
            group by p.id`
        let q = conn.query(sql, [skus], (err, rows) => {
            //console.log(q.sql)
            cb(err, rows)
        })
    },

    getShippedByPO: function(site, ponum, cb) {
        var sql = `
            select
                o.order_id,
                it.ordered_item_id,
                it.quantity_shipped,
                i.quantity,
                i.product_id,
                i.product_type,
                o.site,
                o.shipping_first_name,
                o.shipping_last_name,
                o.shipping_address1,
                o.shipping_address2,
                o.shipping_city,
                o.shipping_state,
                o.shipping_zip,
                o.site_order_num,
                o.customer_po,
                ot.tracking_num,
                ot.date_added,
                lower(ot.carrier) as carrier,
                pt.title as description,
                p.weight,
                p.upc_old,
                s.shipment_json
            from order_tracking ot 
                left join ordered_item_tracking it on it.track_id=ot.track_id
                inner join ordered_items i on i.item_id=it.ordered_item_id
                inner join orders o on o.order_id=ot.order_id
                left join shipments s on s.shp=ot.shp
                left join products p on p.id=i.product_id
                left join product_types pt on pt.product_type_id = i.product_type
            where 
                o.status = 3
                and o.site = ?
                and o.customer_po = ?
            order by o.order_id asc;`
        let q = conn.query(sql, [site, ponum], (err, rows) => {
            //console.log(q.sql)
            cb(err, rows)
        })
    },

    getRecentlyShipped: function(site, cb) {
        var sql = `
            select
                o.order_id,
                it.ordered_item_id,
                it.quantity_shipped,
                i.quantity,
                i.product_id,
                i.product_type,
                o.site,
                o.shipping_first_name,
                o.shipping_last_name,
                o.shipping_address1,
                o.shipping_address2,
                o.shipping_city,
                o.shipping_state,
                o.shipping_zip,
                o.site_order_num,
                o.customer_po,
                ot.tracking_num,
                ot.date_added,
                lower(ot.carrier) as carrier,
                pt.title as description,
                p.weight,
                p.upc_old,
                s.shipment_json
            from order_tracking ot 
                left join ordered_item_tracking it on it.track_id=ot.track_id
                inner join ordered_items i on i.item_id=it.ordered_item_id
                inner join orders o on o.order_id=ot.order_id
                left join shipments s on s.shp=ot.shp
                left join products p on p.id=i.product_id
                left join product_types pt on pt.product_type_id = i.product_type
            where 
                o.status = 3
                and o.site = ?
                and ot.date_added > date_sub(now(), interval 1 hour)
            order by o.order_id asc;`
        let q = conn.query(sql, [site], (err, rows) => {
            //console.log(rows)
            //console.log(q.sql)
            cb(err, rows)
        })
    },

    getRecentlyInvoiced: function(site, cb) {
        var sql = `
            select
                o.order_id,
                o.customer_po,
                o.subtotal,
                o.total
            from orders o 
            where o.customer_po = '22222222'
               /* o.status = 3
                and o.invoice_number is not null
                and o.invoice_number != ''
                and o.site = ?
                and o.date_added > date_sub(now(), interval 1 month)*/
            order by o.order_id asc;`
        let q = conn.query(sql, [site], (err, rows) => {
            //console.log(rows)
            //console.log(q.sql)
            cb(err, rows)
        })
    },

    query: function query(query, params, cb) {
        conn.query(query, params, cb)
    }
}

module.exports = funcs