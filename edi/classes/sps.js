const fs = require('fs')
const async = require('async')
const { Builder } = require('xml2js')
const { DateTime } = require("luxon")
const sftpClient = require('ssh2-sftp-client')
const _ = require('lodash')

module.exports = class Sps {
    constructor(config, marketplaceId, args = []) {
        this.config = config
        this.partnerId = _.findKey(config.sites, e => e == marketplaceId)
        this.args = args
    }

    parseDoc(file, callback) {
        if (file.includes('/')) {
            fs.readFile(file, 'utf8', (err, contents) => {
                XMLToJSON(contents, callback)
            });
        } else {
            return XMLToJSON(file, callback);
        }
    }

    XMLToJSON(data, callback) {
        let parsed

        async.waterfall([
            (cb) => xml2js.parseString(data.toString(), cb),
            (xml, cb) => {
                //console.log('xml:', util.inspect(xml, false, null, true))

                let shipTo = xml.Order.Header[0].Address.find((e) => e.AddressTypeCode[0] == 'ST')
                let totalItemQty = xml.Order.LineItem.reduce((c, i) => { return c + parseInt(i.OrderLine[0].OrderQty[0]) }, 0)
                parsed = {
                    raw: xml,
                    filename: '',
                    customerOrderNumber: xml.Order.Header[0].OrderHeader[0].CustomerOrderNumber[0],
                    header: {
                        interchangeControl: {
                            raw: '',
                            authInfoQual: '00',
                            authInfo: '',
                            securityInfoQual: '00',
                            securityInfo: '',
                            senderIdQual: 'ZZ',
                            senderId: xml.Order.Header[0].OrderHeader[0].TradingPartnerId,
                            receiverIdQual: 'ZZ',
                            receiverId: 'VICTORY',
                            date: xml.Order.Header[0].OrderHeader[0].PurchaseOrderDate[0],
                            time: '',
                            repetitionSeparator: '',
                            controlVersionNumber: '',
                            controlNumber: '',
                            acknowledgmentRequested: '',
                            usageIndicator: ''
                        },
                        functionalGroup: {
                            raw: '',
                            functionalIdentifierCode: 'PO',
                            senderCode: xml.Order.Header[0].OrderHeader[0].TradingPartnerId,
                            receiverCode: 'VICTORY',
                            date: xml.Order.Header[0].OrderHeader[0].PurchaseOrderDate[0],
                            time: '',
                            controlNumber: '',
                            responsibleAgencyCode: '',
                            versionCode: ''
                        },
                        transactionSet: {
                            raw: '',
                            code: '850',
                            controlNumber: ''
                        }
                    },
                    orderDetail: {
                        raw: '',
                        purposeCode: xml.Order.Header[0].OrderHeader[0].TsetPurposeCode[0],
                        typeCode: xml.Order.Header[0].OrderHeader[0].PrimaryPOTypeCode[0],
                        poNumber: xml.Order.Header[0].OrderHeader[0].PurchaseOrderNumber[0],
                        releaseNumber: '',
                        date: xml.Order.Header[0].OrderHeader[0].PurchaseOrderDate[0],
                        referenceIdentification: {
                            raw: '',
                            refIdQual: '',
                            refId: ''
                        },
                        salesRequirements: {
                            raw: '',
                            backOrder: ''
                        },
                        dateTimeReference: [ {
                            raw: '',
                            dateQual: xml.Order.Header[0].Dates[0].DateTimeQualifier[0],
                            date: xml.Order.Header[0].Dates[0].Date[0]
                        } ],
                        totals: {
                            raw: '',
                            totalItems: xml.Order.Summary[0].TotalLineItemNumber[0],
                            totalQuantity: totalItemQty,
                            totalSales: xml.Order.Summary[0].TotalSalesAmount != undefined ? xml.Order.Summary[0].TotalSalesAmount[0] : null,
                            grandTotal: xml.Order.Summary[0].TotalAmount != undefined ? xml.Order.Summary[0].TotalAmount[0] : null
                        }
                    },
                    names: {
                        main: xml.Order.Header[0].Address.map((e) => {
                            return {
                                raw: e,
                                entityIdCode: e.AddressTypeCode[0],
                            }
                        })

                        /*[ {
                            raw: shipTo,
                            entityIdCode: 'ST',
                            node: '',
                            idCodeQual: '', // todo find for full addr
                            idCode: ''
                        } ]*/
                    },
                    items: xml.Order.LineItem.map((e, i) => {
                        return {
                            raw: '',
                            LineSequenceNumber: e.OrderLine[0].LineSequenceNumber[0],
                            ConsumerPackageCode: e.OrderLine[0].ConsumerPackageCode[0],
                            BuyerPartNumber: e.OrderLine[0].BuyerPartNumber[0],
                            ProductSizeDescription: e.OrderLine[0].ProductSizeDescription[0],
                            id: ++i,
                            quantity: e.OrderLine[0].OrderQty[0],
                            unitOfMeasure: e.OrderLine[0].OrderQtyUOM[0],
                            price: e.OrderLine[0].PurchasePrice[0],
                            unitPriceCode: 'PE',
                            productIdQualifier: 'VN',
                            //productId: e.OrderLine[0].ProductID[0].PartNumber[0]
                            productId: e.OrderLine[0].BuyerPartNumber[0]
                        }}
                    ),
                    file: ''
                }

                cb()
            }
        ], (err) => {
            callback(err, parsed)
        })
    }

    buildInventoryDoc(products) {
        //process.stdout.write('build xml...')

        let docId = Date.now()
        let obj = {
            ItemRegistries: {
                '$': { xmlns: 'http://www.spscommerce.com/RSX' },
                ItemRegistry: {
                    Header: {
                        HeaderReport: {
                            TradingPartnerId: this.partnerId,
                            DocumentId: docId,
                            TsetPurposeCode: '01', // Manufacturer/Distributor Inventory Report
                            ReportTypeCode: 'MB',
                            InventoryDate: DateTime.local().toFormat('yyyy-LL-dd'),
                            InventoryTime: DateTime.local().toFormat('HH:mm:ssZZ')
                            //Vendor: // Defaulted by SPS
                        },
                        Dates: {
                            DateTimeQualifier: '097', // Transaction Date
                            Date: DateTime.local().toFormat('yyyy-LL-dd'),
                            Time: DateTime.local().toFormat('HH:mm:ssZZ')
                        },
                        //Address: // Defaulted by SPS
                    },
                    Structure: {
                        LineItem: []
                    },
                    Summary: {
                        TotalLineItemNumber: 0
                    }
                }
            }
        }

        let i = 0
        for(let product of products) {
            obj.ItemRegistries.ItemRegistry.Structure.LineItem.push({
                InventoryLine: {
                    LineSequenceNumber: ++i,
                    BuyerPartNumber: product.marketplace_sku.toString().padStart(10, '0'),
                    VendorPartNumber: product.id,
                    ConsumerPackageCode: product.upc,
                    //PurchasePrice:
                },
                /*PriceDetails {
                    PriceTypeIDCode: TIP
                    UnitPrice: 5.48
                    SellersCurrency: testSellersCurrency
                },
                ProductOrItemDescription: {
                    ProductCharacteristicCode: 74
                    ProductDescription: Super comfortable
                },*/
                QuantitiesSchedulesLocations: {
                    QuantityQualifier: '33', // Sellable Qty
                    TotalQty: product.active ? 9999 : 0,
                    TotalQtyUOM: 'EA',
                    Dates: {
                        DateTimeQualifier: '018',
                        Date: DateTime.local().toFormat('yyyy-LL-dd'),
                        Time: DateTime.local().toFormat('HH:mm:ssZZ')
                    }
                }
            })
        }

        obj.ItemRegistries.ItemRegistry.Summary.TotalLineItemNumber = i

        let xml = new Builder().buildObject(obj)
        let localPath = `${__dirname}/../docs/IB_${docId}.xml`

        return new Promise((resolve, reject) => {
            fs.writeFile(localPath, xml, err => {
                err ? reject(err.message) : resolve(localPath)
            })
        })
    }

    sendDoc(localPath, remotePath) {
        //process.stdout.write('push file...')

        let sftp = new sftpClient()
        return new Promise((resolve, reject) => {
            sftp.connect(this.config.put)
                .catch(e => reject(`ftp connect error: ${e.message}`))
                .then(() => sftp.put(localPath, remotePath))
                .catch(e => reject(`upload error: ${e.message}`))
                .then(() => {
                    sftp.end()
                    resolve(`sent: `.green + localPath.replace(/^.*[\\\/]/, ''))
                })
        })
    }

    pushInventory(products = []) {
        //console.log('sps inventory push')

        return new Promise((resolve, reject) => {
            this.buildInventoryDoc(products)
                .then(localPath => {
                    let filename = localPath.split('/')[localPath.split('/').length - 1]
                    let remotePath = `${this.config.put.dir}/${filename}`
                    return this.args.push === false ?
                        `not sent: `.blue + filename :
                        this.sendDoc(localPath, remotePath)
                })
                .then(resolve)
                .catch(reject)
        })
    }

}